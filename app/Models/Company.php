<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';

    public function addCompany($request) {

        if ($request->hasFile('logo')) {
            $request->logo = $request->file('logo')->getClientOriginalName();
        }

        /* Implode 3 comany numbers to 1 string */
        $number_arr = [$request->number0, $request->number1, $request->number2];
        $ar = array_filter(
            $number_arr,
            function($el){ return !empty($el);}
        );
        $number = implode(',', $ar);

        if ($request->has('company_id')) {
            $company = Company::find($request->company_id);
            /****************************/
            $company->user_id = $request->user_id;
            $company->name = $request->name;
            if (isset($request->logo)) {
                $company->logo = $request->logo;
            }
            $company->email = $request->email;
            $company->site = $request->website;
            $company->number = $number;
            $company->description = $request->description;
            $company->update();
            $id = $company->id;
        } else {
            $company = new Company();
            $company->user_id = $request->user_id;
            $company->name = $request->name;
            if (isset($request->logo)) {
                $company->logo = $request->logo;
            }
            $company->email = $request->email;
            $company->site = $request->website;
            $company->number = $number;
            $company->description = $request->description;
            $company->save();
            $id = $company->id;
        }
            return $id;
    }

    public function company() {
        return $this->belongsTo('App\Models\User');
    }

    public function vacancy() {
        return $this->hasMany('App\Models\Vacancy');
    }
}
