<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TopBlogger extends Model
{
    protected $table = 'top_bloggers';

    protected $fillable = array('id', 'user_id');
}
