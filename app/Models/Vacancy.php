<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vacancy extends Model
{
    protected $table = 'vacancies';

    public function company() {
        return $this->belongsTo('App\Models\Company', 'company_id');
    }

    public function category() {
        return $this->belongsTo('App\Models\VacancyCategory', 'category_id');
    }

    public function industry() {
        return $this->belongsTo('App\Models\Industry', 'industry_id');
    }
}
