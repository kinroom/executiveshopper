<?php

namespace App\Models;

use Baum\Node;

class ProductCategory extends Node
{
    protected $table = 'shop_categories';
    
    protected $fillable = ['id', 'name','slug', 'parent_id', 'lft', 'rgt', 'depth', 'description', 'keywords'];

    public function children() {
        return $this->hasMany('App\Models\ProductCategory', 'parent_id');
    }

    public function products() {
        return $this->belongsToMany('App\Models\Product', 'products_categories', 'category_id', 'product_id');
    }
}
