<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model  {

	/**
	 * @var string
	 */
	protected $table = 'roles';

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function users() 
	{
	  return $this->hasMany('App\Models\User');
	}

}
