<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VacancyCategory extends Model
{
    protected $table = 'vacancy_categories';

    public function vacancy() {
        return $this->hasOne('App\Models\Vacancy');
    }
}
