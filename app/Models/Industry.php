<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Industry extends Model
{
    protected $table = 'industries';

    public function vacancy() {
        return $this->hasOne('App\Models\Vacancy');
    }
}
