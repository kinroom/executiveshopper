<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Amsgames\LaravelShop\Traits\ShopItemTrait;

class Product extends Model
{
    use ShopItemTrait;

    protected $table = 'products';

    protected $fillable = ['name', 'sku', 'price', 'video', 'quantity', 'colours', 'thumb', 'sizes', 'size_fit', 'material_cafe', 'delivery', 'details', 'description', 'keywords'];

    public function categories() {
        return $this->belongsToMany('App\Models\ProductCategory', 'products_categories', 'product_id', 'category_id');
    }

    /**
     * @param $product
     * @return mixed
     */
    public function treeLink($product)
    {
        $url = implode('/', $product->categories()->first()->getAncestorsAndSelf()->lists('slug')->toArray());

        return $url;
    }

    public function images() {
        return $this->hasMany('App\Models\ProductImage', 'product_id');
    }

    public function scopeCategorized($query, ProductCategory $category = null) {

        if (!$category) return $query->with('categories');

        $categoryIds = $category->getDescendantsAndSelf()->lists('id');

        return $query->with('categories')
            ->join('products_categories', 'products_categories.product_id', '=', 'products.id')
            ->whereIn('products_categories.category_id', $categoryIds)->orderBy('products.created_at', 'desc')->groupBy('product_id');
    }

    public function scopeCounter($query, ProductCategory $category = null) {

        if (!$category) return $query->with('categories');

        $categoryIds = $category->getDescendantsAndSelf()->lists('id');

        $counter = $query->with('categories')
            ->join('products_categories', 'products_categories.product_id', '=', 'products.id')
            ->whereIn('products_categories.category_id', $categoryIds)->count();

        return $counter;
    }

    // this is a recommended way to declare event handlers
    protected static function boot() {
        parent::boot();

        static::deleting(function($product) { // before delete() method call this
            $product->images()->delete();
            $product->categories()->delete();
        });
    }
}