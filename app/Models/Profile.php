<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Profile extends Model
{

    public function saveProfile($request) {

        $hide_email = 0;
        $hide_number = 0;
        $hide_birthday = 0;

        if (isset($request['hide_email'])) $hide_email = 1;
        if (isset($request['hide_number'])) $hide_number = 1;
        if (isset($request['hide_birthday'])) $hide_birthday = 1;

        if (isset($request['user-male'])) { $gender = 0; } else { $gender = 1; }

        $profile = User::find($request->id);

        $profile->firstname = $request->name;
        $profile->lastname = $request->surname;

        if ($request->hasFile('photo')) {
            $profile->avatar =  $request->file('photo')->getClientOriginalName();
            $profile->service =  'site';
        }
        $profile->gender = $gender;
        $profile->contact_email = $request->email;
        $profile->hide_email = $hide_email;
        $profile->number = $request->number;
        $profile->hide_number = $hide_number;
        $profile->birthday = $request->birthday;
        $profile->hide_birthday = $hide_birthday;
        $profile->site = $request->site;
        $profile->save();
    }
}
