<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class stickyPost extends Model
{
    protected $table = 'sticky_post';

    protected $fillable = array('id', 'post_id');

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo('App\Models\Post');
    }
}
