<?php

namespace App\Http\Middleware;

use App\Models\Role;

use Closure;
use Illuminate\Http\RedirectResponse;

class IsRedactor {

	/**
	 * @param $request
	 * @param Closure $next
	 * @return RedirectResponse
	 */
	public function handle($request, Closure $next)
	{
		$user = $request->user();

		if ($user) {
			$role = Role::where('id', $user->role_id)->pluck('slug');

			if ($user && ($role == 'redac' || $role == 'admin')) {
				return $next($request);
			}
		}

		return redirect()->back();
	}

}