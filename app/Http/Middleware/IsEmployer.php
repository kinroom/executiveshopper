<?php

namespace App\Http\Middleware;

use App\Models\Role;
use Illuminate\Http\RedirectResponse;

use Closure;

class IsEmployer
{
    /**
     * @param $request
     * @param Closure $next
     * @return RedirectResponse
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        if ($user) {
            $role = Role::where('id', $user->role_id)->pluck('slug');

            if ($user && ($role == 'employer' || $role == 'admin' || $role == 'redac')) {
                return $next($request);
            }
        }
        return redirect()->back();
    }

}