<?php

namespace App\Http\Middleware;

use App\Models\Role;
use Illuminate\Http\RedirectResponse;

use Closure;

class IsBlogger
{
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        if ($user) {
            $role = Role::where('id', $user->role_id)->pluck('slug');

            if ($role == 'blogger' || $role == 'admin' || $role == 'redac') {
                return $next($request);
            }
        }

        return redirect()->back();
    }
}