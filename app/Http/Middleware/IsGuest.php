<?php

namespace App\Http\Middleware;

use Illuminate\Http\RedirectResponse;

use Closure;

class IsGuest
{
    /**
     * @param $request
     * @param Closure $next
     * @return RedirectResponse
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        if ($user) {
            return redirect()->back();
        }

        return $next($request);
    }

}