<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\ThrottlesLogins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Repositories\UserRepository;
use App\Jobs\SendMail;

class AuthController extends Controller
{

	use AuthenticatesAndRegistersUsers, ThrottlesLogins;

	/**
	 * AuthController constructor.
	 */
	public function __construct()
	{
        parent::__construct();
		$this->middleware('guest', ['except' => 'getLogout']);
	}

	/**
	 * @param LoginRequest $request
	 * @param Guard $auth
	 * @return mixed
	 */
	public function postLogin(
		LoginRequest $request,
		Guard $auth)
	{
		$logValue = $request->input('login');

		$logAccess = filter_var($logValue, FILTER_VALIDATE_EMAIL) ? 'email' : 'email';

        $throttles = in_array(
            ThrottlesLogins::class, class_uses_recursive(get_class($this))
        );

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
			return redirect(\App::getLocale() . '/auth/login')
				->with('error', trans('front/login.maxattempt'))
				->withInput($request->only('log'));
        }

		$credentials = [
			$logAccess  => $logValue,
			'password'  => $request->input('password')
		];


		if(!$auth->validate($credentials)) {
			if ($throttles) {
	            $this->incrementLoginAttempts($request);
	        }
            return redirect(\App::getLocale() . '/auth/register');
		}

		$user = $auth->getLastAttempted();

        $request->session()->put('user_id', $user->id);

		if($user->confirmed) {
			if ($throttles) {
                $this->clearLoginAttempts($request);
            }

			$auth->login($user, $request->has('memory'));

			if($request->session()->has('user_id'))	{
				$request->session()->forget('user_id');
			}

			return redirect(\App::getLocale() . '/profile/');
		} else {
            return view('auth.not-confirmed');
        }
	}


	/**
	 * @param RegisterRequest $request
	 * @param UserRepository $user_gestion
	 * @return mixed
	 */
	public function postRegister(RegisterRequest $request, UserRepository $user_gestion)
	{
		$user = $user_gestion->store(
			$request->all(),
			$confirmation_code = str_random(30)
		);

		$this->dispatch(new SendMail($user));

        return redirect(\App::getLocale() . '/email-confirmation');
	}

	/**
	 * @param UserRepository $user_gestion
	 * @param $confirmation_code
	 * @return mixed
	 */
	public function getConfirm(UserRepository $user_gestion, $confirmation_code)
	{
		$user = $user_gestion->confirm($confirmation_code);

        $firstname = $user->firstname;
        $lastname = $user->lastname;
		
        return view('auth.confirmed', compact('firstname', 'lastname'));
	}


	/**
	 * @param UserRepository $user_gestion
	 * @param Request $request
	 * @return mixed
	 */
	public function getResend(UserRepository $user_gestion, Request $request)
	{
		if ($request->session()->has('user_id'))	{
			$user = $user_gestion->getById($request->session()->get('user_id'));

			$this->dispatch(new SendMail($user));


            return redirect(\App::getLocale() . '/email-confirmation');
		}

        return redirect('/' . \App::getLocale() . '/');
	}

    public function confirmation()
    {
        return view('auth.confirmation');
    }

}
