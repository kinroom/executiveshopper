<?php
namespace App\Http\Controllers;

use App\Jobs\ChangeLocale;
use App\Models\Language;
use App\Models\Post;
use App\Models\stickyPost;
use App\Models\TopBlogger;
use App\Models\Slider;
use App\Models\User;
use App\Models\Advertisement;
use Illuminate\Support\Facades\App;

class HomeController extends Controller
{
    private $conditions;

    public function __construct()
    {
        parent::__construct();

        $locale = App::getLocale() ? App::getLocale() : 'en';
        $language = Language::where('name', $locale)->first();

        $this->conditions = ['active' => 1, 'language_id' => $language->id];
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $latest_posts = Post::where($this->conditions)->orderBy('id', 'DESC')->take(9)->get();
      
        $latest_from_cat_1 = Post::where('category_id', 1)->where($this->conditions)->orderBy('id', 'DESC')->take(3)->get();
        $latest_from_cat_2 = Post::where('category_id', 2)->where($this->conditions)->orderBy('id', 'DESC')->take(3)->get();
        $latest_from_cat_3 = Post::where('category_id', 3)->where($this->conditions)->orderBy('id', 'DESC')->take(3)->get();

        $most_viewed = Post::where($this->conditions)->orderBy('views', 'DESC')->take(3)->get();

        $latest_videos = Post::select('title', 'video', 'slug', 'created_at')->where('video', '<>', '')->where($this->conditions)->orderBy('id', 'desc')->take(3)->get();

        $special_id = stickyPost::first();
        
        if ($special_id !== null) {
            $special_news = Post::where('language_id', $this->conditions['language_id'])->where('original_id', $special_id->post_id)->first();
            if (!$special_news) {
                $special_news = Post::where('id', $special_id->post_id)->first();
            }
            $special_news->content = $this->getSummary($special_news->content, $counttext = 50, $sep = ' ') . ' ...';
        }

        $slider = Slider::with('post')->get();

		if (count($slider) > 0)
		{
			 foreach ($slider as $slide) {
				$translatedPost = Post::where('language_id', $this->conditions['language_id'])->where('original_id', $slide->post->id)->first();
				if ($translatedPost) {
					$slide->post =  $translatedPost;
				}
			}
		}
       
        $top_blogger = TopBlogger::first();
        if ($top_blogger != null) {
            $week_blogger = User::find($top_blogger->user_id);
            $week_blogger->description = $top_blogger->description;
        }

        $advertisement = Advertisement::where('place', 'main')->first();

        return view('front.index', compact('latest_posts', 'latest_from_cat_1', 'latest_from_cat_2', 'latest_from_cat_3',
            'most_viewed', 'special_news', 'slider', 'week_blogger', 'advertisement', 'latest_videos'));
    }

    /**
     * @param ChangeLocale $changeLocale
     * @param $language
     * @return mixed
     */
    public function language(ChangeLocale $changeLocale, $language)
    {
        $changeLocale->changeLocale($language);
        return redirect()->back();
    }
}