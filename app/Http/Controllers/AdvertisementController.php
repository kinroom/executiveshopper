<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Advertisement;
use App\Http\Requests\AdvertisementRequest;


class AdvertisementController extends Controller
{
    /**
     * AdvertisementController constructor.
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $advertisements = Advertisement::get();
        return view('back.advertisement.index', compact('advertisements'));
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return view('back.advertisement.create', compact('advertisement'));
    }

    /**
     * @param AdvertisementRequest $request
     * @return mixed
     */
    public function store(AdvertisementRequest $request)
    {

        if ($request->place == 'main') {
            $advirtisement = Advertisement::firstOrNew(['place' => 'main']);
            $advirtisement->place = $request->place;
            $advirtisement->code = $request->code;
            $advirtisement->save();
        } else {
                Advertisement::create(array('place' => $request->place, 'code' => $request->code));
        }

        return redirect()->route('admin.advertisement.index');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $advertisement = Advertisement::find($id);

        return view('back.advertisement.show', compact('advertisement'));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $advertisement = Advertisement::find($id);

        return view('back.advertisement.edit', compact('advertisement'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $advertisement = Advertisement::find($id);
        $advertisement->place = $request->place;
        $advertisement->code = $request->code;
        $advertisement->update();

        return redirect()->route('admin.advertisement.index');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        Advertisement::destroy($id);
        return redirect()->back();
    }
}
