<?php

namespace App\Http\Controllers;

use App\Repositories\VacancyRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;

use App\Models\Industry;
use App\Models\VacancyCategory;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Vacancy;
use App\Jobs\SendMail;
use App\Models\Post;
use Illuminate\Contracts\Auth\Guard;
use App\Models\User;
use App\Http\Requests\VacancyRequest;
use App\Models\Country;
use App\Models\City;
use App\Models\Role;


class VacancyController extends Controller
{
    protected $availability = [];

    protected $industries = [];

    protected $categories = [];

    protected $availabilities = [0, 1];

    protected $vacancy_gestion;

    protected $user_gestion;

    protected $nbrPages;

    protected $locale;

    /**
     * VacancyController constructor.
     */
    public function __construct(Request $request, VacancyRepository $vacancy_gestion, UserRepository $user_gestion)
    {
        parent::__construct();

        $this->user_gestion = $user_gestion;
        $this->vacancy_gestion = $vacancy_gestion;
        $this->nbrPages = 12;
        $this->locale = substr($request->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);;

        $this->industries = Industry::where('id', '>', 0)->lists('id')->toArray();
        $this->categories = VacancyCategory::where('id', '>', 0)->lists('id')->toArray();
        $this->availability = [
            '0' => 'Freelance',
            '1' => 'Internship'
        ];

        $this->middleware('employer', ['only' => ['create', 'store', 'destroy']]);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function indexOrder(Request $request)
    {
        $vacancies = $this->vacancy_gestion->index($this->nbrPages);

        return view('back.vacancy.index', compact('vacancies'));
    }

    /**
     * @return mixed
     */
    public function index(Request $request)
    {
        $vacancies = Vacancy::where('is_active', 1)->orderBy('id', 'desc')->take(10)->get();
        $count_vacancy = Vacancy::where('is_active', 1)->count();

        $companies = $this->getCompanies($vacancies);
        $countries = Vacancy::select('country')->where('country', '<>', 'All')->where('is_active', 1)->groupBy('country')->orderBy('country', 'ASC')->get();
        $industries = $this->getIndustries();
        $categories = $this->getCategories();
        $freelance_count = Vacancy::where('availability_id', 0)->where('is_active', 1)->count();
        $internship_count = Vacancy::where('availability_id', 1)->where('is_active', 1)->count();

        if (count($vacancies) > 0) {
            $more_vacancies = Vacancy::where('id', '<', $vacancies[count($vacancies) - 1]['id'])->where('is_active', 1)->orderBy('id', 'desc')->take(1)->get();
        }

        $user = $request->user();

        if ($user) {
            $role = Role::where('id', $user->role_id)->pluck('slug');
        } else {
            $role = '';
        }

        return view('front.vacancy.index', compact('vacancies', 'more_vacancies', 'companies', 'categories',
                                            'industries', 'countries', 'count_vacancy', 'freelance_count', 'internship_count', 'role'));
    }


    /**
     * @param Request $request
     * @return string
     */
    public function loadVacancies(Request $request)
    {
        $page = 10 * $request->page;

        $vacancies = Vacancy::where('is_active', 1)->orderBy('id', 'desc')->take(10)->offset($page)->get();

        if (count($vacancies) > 0) {
            $more_vacancies = Vacancy::where('is_active', 1)->orderBy('id', 'desc')->where('id', '<', $vacancies[count($vacancies) - 1]['id'])->take(1)->get();
        } else { $more_vacancies = []; }

        $string = $this->getHtmlTemplate($vacancies, $more_vacancies, $request->page, false, false);

        return $string;
    }


    /**
     * @param Request $request
     */
    public function filterVacancies(Request $request)
    {
        $page = 10 * $request->ajax_page;

        if ($request->industries == NULL) $request->industries = $this->industries;
        if ($request->categories == NULL) $request->categories = $this->categories;
        if ($request->availabilities == NULL) $request->availabilities = $this->availabilities;
        if ($request->country == NULL || $request->country == 'All') { $request->country = '%'; }
        if ($request->city == NULL || $request->city == 'All') { $request->city = '%'; }

        $vacancies = Vacancy::whereIn('industry_id', $request->industries)
                            ->whereIn('category_id', $request->categories)
                            ->whereIn('availability_id', $request->availabilities)
                            ->where('country', 'LIKE', $request->country)
                            ->where('city', 'LIKE', $request->city)
                            ->where('is_active', 1)
                            ->orderBy('id', 'desk')
                            ->take(10)
                            ->offset($page)
                            ->get();

        $more_vacancies = Vacancy::whereIn('industry_id', $request->industries)
                                ->whereIn('category_id', $request->categories)
                                ->whereIn('availability_id', $request->availabilities)
                                ->where('id', '<', $vacancies[count($vacancies) - 1]['id'])
                                ->where('country', 'LIKE', "$request->country%")
                                ->where('city', 'LIKE', "$request->city")
                                ->where('is_active', 1)
                                ->orderBy('id', 'desk')
                                ->take(1)
                                ->get();



        $count_vacancies = Vacancy::whereIn('industry_id', $request->industries)
                                ->whereIn('category_id', $request->categories)
                                ->whereIn('availability_id', $request->availabilities)
                                ->where('is_active', 1)
                                ->where('country', 'LIKE', $request->country)
                                ->where('city', 'LIKE', $request->city)
                                ->count();

        $string = $this->getHtmlTemplate($vacancies, $more_vacancies, $request->ajax_page, true, false);

        echo json_encode(Array('vacancies' => $string, 'count' => $count_vacancies));

    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request, Guard $auth)
    {
        $search = $request->search;

        $vacancies = Vacancy::where('title', 'LIKE', "%$search%")->where('is_active', 1)->orderBy('id', 'desk')->take(10)->get();
        $count_vacancy = Vacancy::where('title', 'LIKE', "%$search%")->where('is_active', 1)->count();
        $companies = $this->getCompanies($vacancies);
        $countries = Country::select('id_country', 'country_name_en')->get();
        $industries = $this->getIndustries();
        $categories = $this->getCategories();
        $freelance_count = Vacancy::where('availability_id', 0)->where('is_active', 1)->count();
        $internship_count = Vacancy::where('availability_id', 1)->where('is_active', 1)->count();

        if (count($vacancies) > 0) {
            $more_search = Vacancy::where('title', 'LIKE', "%$request->search%")->where('is_active', 1)->where('id', '<', $vacancies[count($vacancies) - 1]['id'])->orderBy('id', 'desc')->take(1)->get();
        }

        if (Auth::check()) {
            $role = Role::where('id', $auth->user()->role_id)->pluck('slug');
        } else {
            $role = 'guest';
        }



        return view('front.vacancy.index', compact('vacancies', 'more_search', 'companies', 'categories',
            'industries', 'countries', 'count_vacancy', 'freelance_count', 'internship_count','search', 'role'));

    }

    /**
     * @param Request $request
     * @return string
     */
    public function searchVacancies(Request $request)
    {
        $page = 10 * $request->page;

        $search = $request->search;

        $vacancies = Vacancy::where('title', 'LIKE', "%$search%")->where('is_active', 1)->orderBy('id', 'desk')->take(10)->offset($page)->get();

        if (count($vacancies) > 0) {
            $more_search = Vacancy::where('title', 'LIKE', "%$request->search%")->where('is_active', 1)->where('id', '<', $vacancies[count($vacancies) - 1]['id'])->orderBy('id', 'desc')->take(1)->get();
        }

        $string = $this->getHtmlTemplate($vacancies, $more_search, $request->page, false, true, $search);

        return $string;
    }

    /**
     * @param $array
     * @param $more_vacancies
     * @param $page
     * @param $is_filter
     * @param $is_search
     * @param null $search
     * @return string
     */
    public function getHtmlTemplate($array, $more_vacancies, $page, $is_filter, $is_search, $search = null)
    {
        $string = '';

        foreach ($array as $vacancy) {
            $vacancy->description = $this->getSummary($vacancy->description, $counttext = 40, $sep = ' ');
            $vacancy->availability_id = $this->availability[$vacancy->availability_id];

            $string .= "
                <li class='clearfix'>
                    <div class='clearfix'>
                        <div class='vacancies-left'>";
            if ($vacancy->company->logo != '' && file_exists(base_path() .'/public/img/uploads/companies/' . $vacancy->company->user_id . '/' . $vacancy->company->id . '/' . $vacancy->company->logo)) {
                $string .=  "<img src='/img/uploads/companies/" . $vacancy->company->user_id . "/" . $vacancy->company->id . "/" . $vacancy->company->logo . "' alt='" . $vacancy->company->name ."'>";
            }  else {
                $string .=  "<img src='/img/company_default.png' alt='" . $vacancy->company->name ."'>";
            }
            $string .= "
                        </div>
                        <div class='vacancies-right clearfix'>
                            <div class='job-heading'>
                                <h2><a href='/$this->locale/fashion-job/$vacancy->id/'>$vacancy->title</a></h2>
                                <span class='job-company-name'>" . $vacancy->company->name ."</span>
                                <span class='job-date'>".date('d.m.Y', strtotime($vacancy->created_at))."</span>
                            </div>
                            <div class='job-text'>
                                <p>$vacancy->description</p>
                            </div>
                            <div class='vacancy-bottom clearfix'>
                                <div class='clearfix'>
                                    <div class='vacancy-bottom-left'>
                                                <span>".trans('back/my-vacancies.salary').":</span>
                                    </div>
                                    <div class='vacancy-bottom-right'>
                                        <span>$vacancy->salary</span>
                                    </div>
                                </div>
                                <div class='clearfix'>
                                    <div class='vacancy-bottom-left'>
                                        <span>".trans('back/my-vacancies.availability').":</span>
                                    </div>
                                    <div class='vacancy-bottom-right'>
                                        <span>$vacancy->availability_id</span>
                                    </div>
                                </div>
                                <div class='clearfix'>
                                    <div class='vacancy-bottom-left'>
                                        <span>".trans('back/my-vacancies.location').":</span>
                                    </div>
                                    <div class='vacancy-bottom-right'>
                                        <span>$vacancy->country</span>
                                        <span class='vacancy-bottom-city'>$vacancy->city</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>";
        }

        // if it is not last post in database, show more button
        if (isset($more_vacancies) && count($more_vacancies) > 0) {
            $page = $page + 1;
            if ($is_filter) {
                $string .= "<button class='more-posts more-vacancies filter-more' href='#' id='$page'>";
            } elseif ($is_search) {
                $string .= "<button class='more-posts more-vacancies' href='#' id='$page+$search' onclick='searchVacancies(this.id)'>";

            } else {
                $string .= "<button class='more-posts more-vacancies' href='#' id='$page' onclick='loadVacancies(this.id)'>";
            }
            $string .= "
                <span>".trans('back/my-vacancies.show-more')."</span>
                <i class='fa fa-angle-down show-more-icon'></i>
            </button>
        ";
        }

        return $string;
    }


    /**
     * @return mixed
     */
    public function getIndustries()
    {
        $industries = Industry::select('id','name')->get();

        foreach ($industries as $industry) {
            $industry->count = Vacancy::where('industry_id', $industry->id)->where('is_active', 1)->count();
        }

        return $industries;
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        $categories = VacancyCategory::select('id','name')->get();

        foreach ($categories as $category) {
            $category->count = Vacancy::where('category_id', $category->id)->where('is_active', 1)->count();
        }

        return $categories;
    }

    /**
     * @param $vacancies
     * @return array
     */
    public function getCompanies($vacancies)
    {
        $companies = [];

        foreach ($vacancies as $vacancy) {
            $vacancy->description = $this->getSummary($vacancy->description, $counttext = 40, $sep = ' ');
            $vacancy->availability_id = $this->availability[$vacancy->availability_id];
            $new = Vacancy::find($vacancy->id)->company;
            array_push($companies, $new);
        }

        return $companies;
    }

    /**
     * @param Request $request
     * @return string
     */
    public function getCity(Request $request)
    {
        $options = '';

        if (strlen($request->country_id) > 3) {

            $cities = Vacancy::select('city')->where('country', $request->country_id)->where('is_active', 1)->groupBy('city')->orderBy('city', 'ASC')->get();
            foreach ($cities as $city) {
                $options .= "<option value='$city->city'>$city->city</option>";
            }

        } else {

            $cities = City::where('id_country', $request->country_id)->orderBy('city_name_en', 'ASC')->get();
            foreach ($cities as $city) {
                $options .= "<option value='$city->city_name_en'>$city->city_name_en</option>";
            }
        }

        return $options;
    }

    /**
     * @param Guard $auth
     * @return mixed
     */
    public function create(Guard $auth)
    {
        $count = Post::where('user_id', \Auth::user()->id)->where('active', 1)->where('active_seen', 0)->count();
        $url = config('medias.url');

        $companies = User::find($auth->user()->id)->companies()->select('id','name')->get();
        $categories = VacancyCategory::select('id','name')->get();
        $industries = Industry::select('id','name')->get();
        $countries = Country::select('id_country', 'country_name_en')->orderBy('country_name_en', 'ASC')->get();

        $role = Role::where('id', $auth->user()->role_id)->pluck('slug');

        return view('back.vacancy.create', compact('count', 'companies', 'categories', 'industries', 'url', 'countries', 'role'));
    }

    /**
     * @param VacancyRequest $request
     * @param Guard $auth
     * @return mixed
     */
    public function store(VacancyRequest $request, Guard $auth)
    {
        if (isset($request['filter-country']) && $request['filter-country'] !== 'All') {
            $country = Country::where('id_country',$request['filter-country'])->pluck('country_name_en');
        } else {
            $country = 'All';
        }

        $vacancy = new Vacancy();
        $vacancy->user_id = $auth->user()->id;
        $vacancy->company_id = $request->company_id;
        $vacancy->title = $request->title;
        $vacancy->description = $request->description;
        $vacancy->salary = $request->salary;
        $vacancy->country = $country;
        $vacancy->city = $request['filter-city'];
        $vacancy->availability_id = $request->availability_id;
        $vacancy->category_id = $request->category_id;
        $vacancy->industry_id = $request->industry_id;
        $vacancy->save();
        
        return redirect(\App::getLocale() . '/fashion-job/confirmation');
    }

    public function confirmation(Guard $auth)
    {
        $role = Role::where('id', $auth->user()->role_id)->pluck('slug');
        $count = Post::where('user_id', \Auth::user()->id)->where('active', 1)->where('active_seen', 0)->count();

        return view('back.vacancy.confirmation', compact('role', 'count'));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $vacancy = Vacancy::find($id);
        $role = Role::where('id', \Auth::user()->role_id)->pluck('slug');
        $vacancy->availability_id = $this->availability[$vacancy->availability_id];

        return view('front.vacancy.show', compact('vacancy', 'role'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function sendCV(Request $request)
    {
        SendMail::sendCV($request);
        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function destroy(Request $request, $id)
    {
        dd(11);
        if ($id == 0) $id = $request->id;
        Vacancy::destroy($id);
        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function updateSeen(Request $request, $id)
    {
        $this->vacancy_gestion->updateSeen($request->all(), $id);

        return response()->json();
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function updateActive(Request $request, $id)
    {
        $this->vacancy_gestion->updateActive($request->all(), $id);

        return response()->json();
    }
}
