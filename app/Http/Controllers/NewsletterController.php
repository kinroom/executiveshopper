<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Subscribers;

use Newsletter;

class NewsletterController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function subscribe(Request $request)
    {
        $email = $request->get('email');

        if(!$email) return redirect()->back();

        if (!Newsletter::hasMember($email)) {
            Newsletter::subscribe($email);

            $subscriber = new Subscribers();
            $subscriber->email = $email;
            $subscriber->save();
        }

        return view('emails.subscribed');
    }
}
