<?php

namespace App\Http\Controllers;

use App\Jobs\SendMail;
use App\Models\ProductImage;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Support\Facades\Input;

class ProductController extends Controller
{
    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        parent::__construct();
        
        $this->productRepository = $productRepository;

        //$this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('created_at', 'DESC')->paginate(10);

        return view('back.shop.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roots = ProductCategory::roots()->get();
        $url = config('medias.url');

        return view('back.shop.product.create', compact('roots', 'url'));
    }

    /**
     * @param Requests\StoreProductRequest $request
     * @return mixed
     */
    public function store(Requests\StoreProductRequest $request)
    {
        $product = $this->productRepository->createProduct($request);

        if ($request->hasFile('thumb') || $request->hasFile('images')) {
            $imageController = new ImageController();

            if ($request->hasFile('thumb')) $imageController ->saveProductThumb($request->file('thumb'), $product->id);

            if ($request->hasFile('images')) {
                foreach ($request->file('images') as $image) {
                    $imageController ->saveProductImage($image, $product->id);
                    $this->productRepository->createProductImages($product->id, $image->getClientOriginalName());
                }
            }
        }
        
        return redirect()->route('product.list');
    }

    /**
     * @param $categories
     * @param $slug
     * @return mixed
     */
    public function show($categories, $slug)
    {
        $product = Product::where(['slug' => $slug])->first();
        $roots = ProductCategory::roots()->get();
        
        return view('front.shop.product', compact('roots', 'product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $product = Product::find($id);

        $categoriesIds = $product->categories()->lists('id')->toArray();

        if ($product->thumb) {
            $thumb['path'] = 'http://' . $request->server("HTTP_HOST") . '/img/uploads/shop/products/' . $product->id . '/thumbs/' .  $product->thumb;
            $thumb['deletePath'] = route('product.thumb.delete', ['id' => $id]);
        } else {
            $thumb = null;
        }

        $productImages = $product->images()->lists('name', 'id')->toArray();

        $images = [];
        $imagesConfig = [];

        $i = 0;

        foreach ($productImages as $key => $image) {
            $images[$i] = 'http://' . $request->server("HTTP_HOST") . '/img/uploads/shop/products/' . $product->id . '/images/' . $image;
            $imagesConfig[$i] = ['url' => route('product.image.delete', ['id' => $key]), 'caption' => $image];
            $i++;
        }

        $roots = ProductCategory::roots()->get();
        $url = config('medias.url');

        return view('back.shop.product.edit', compact('roots', 'product', 'categoriesIds', 'thumb', 'images', 'imagesConfig', 'url'));
    }

    /**
     * @param Requests\StoreProductRequest $request
     * @param $id
     * @return mixed
     */
    public function update(Requests\StoreProductRequest $request, $id)
    {
        $product = $this->productRepository->updateProduct($request, $id);
        
        if ($request->hasFile('thumb') || $request->hasFile('images')) {
            $imageController = new ImageController();

            if ($request->hasFile('thumb')) $imageController->saveProductThumb($request->file('thumb'), $product->id);

            if ($request->hasFile('images')) {
                foreach ($request->file('images') as $image) {
                    $imageController->saveProductImage($image, $product->id);
                    $this->productRepository->createProductImages($product->id, $image->getClientOriginalName());
                }
            };
        }


        return redirect()->back();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteImage($id)
    {
        $image = ProductImage::find($id);

        $thumb = new ImageController();
        $thumb->deleteProductImage($image->product_id, $image->name);

        $image->delete();

        return json_encode(array('uploaded' => 'OK' ));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteThumb($id)
    {
        $product = Product::find($id);

        $thumb = new ImageController();
        $thumb->deleteProductThumb($id, $product->thumb);

        $product->thumb = null;
        $product->save();

        return json_encode(array('uploaded' => 'OK' ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $thumb = new ImageController();
        $thumb->deleteProduct($id);

        $product = Product::find($id);
        $product->delete();

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return string
     */
    public function buyProduct(Request $request)
    {
        $data = $request->all();
        $product = Product::find($data['product_id']);

        $product = [
            'name' => $product->name,
            'price' => $product->price,
            'color' => isset($data['color']) ? '#' . trim($data['color']) : 'None',
            'size' => isset($data['size']) ? trim($data['size']) : 'None'
        ];

        $user = [
            'name' => $data['name'],
            'email' => $data['email'],
            'phone' => $data['phone']
        ];
        
        SendMail::buyProduct($product, $user);
        
        return 'ok';
    }
}
