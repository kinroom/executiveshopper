<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Http\Requests\TranslationRequest;
use App\Repositories\BlogRepository;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mockery\CountValidator\Exception;
use App\Models\Tag;
use App\Http\Requests\PostRequest;

class TranslationController extends Controller
{
    protected $blog_gestion;

    protected $tag;

    protected $model;


    public function __construct(BlogRepository $blog_gestion, Tag $tag, Post $post)
    {
        parent::__construct();
        $this->blog_gestion = $blog_gestion;
        $this->tag = $tag;
        $this->model = $post;

        $this->middleware('redac');
    }
    /**
     * @param $id
     * @return mixed
     */
    public function translate($id)
    {
        $post = Post::find($id);

        $languages = Language::all();

        return view('back.translate.translate', compact('post', 'languages'));
    }

    /**
     * @param TranslationRequest $request
     * @param $id
     * @return mixed
     */
    public function store(TranslationRequest $request, $id)
    {
        try{
            $post = Post::find($id);

            $translate = new Post();

            $translate->title =  $request->get('title');
            $translate->slug = str_slug($request->get('title'), "-");
            $translate->category_id = $post->category_id;
            $translate->subcategory = $post->subcategory;
            $translate->thumb = $post->thumb;
            $translate->video = $post->video;
            $translate->content = $request->get('content');
            $translate->photographer = $post->photographer;
            $translate->styling = $post->styling;
            $translate->outfit = $post->outfit;
            $translate->location = $post->location;
            $translate->description = $request->get('description');
            $translate->keywords = $request->get('keywords');
            $translate->website = $post->website;
            $translate->active = 1;
            $translate->seen = 1;
            $translate->active_seen = 1;
            $translate->original_id = $post->id;
            $translate->is_commercial = $post->is_commercial;
            $translate->language_id = $request->get('language');
            $translate->translator_id = Auth::user()->id;
            $translate->user_id = $post->user_id;
            $translate->save();


            if($request->get('tags') != '') {

                $tags = explode(',', $request->get('tags'));

                foreach ($tags as $tag) {
                    $tag_ref = $this->tag->whereTag($tag)->first();
                    if(is_null($tag_ref)) {
                        $tag_ref = new $this->tag();
                        $tag_ref->tag = $tag;
                        $translate->tags()->save($tag_ref);
                    } else {
                        $translate->tags()->attach($tag_ref->id);
                    }
                }
            }

            $language = Language::find($request->get('language'));

            return redirect()->route('admin.trans.index', $language->name);
        } catch (Exception $e) {
            dd($e);
            return redirect()->back();
        }

    }

    public function translations(Request $request, $language)
    {
        $language_id = Language::where('name', $language)->pluck('id');

        $query = $this->model
            ->select('posts.id', 'title', 'user_id', 'slug', 'firstname', 'lastname', 'name')
            ->join('users', 'users.id', '=', 'posts.translator_id')
            ->join('categories', 'categories.id', '=', 'posts.category_id')
            ->orderBy('id', 'desc')
            ->where('original_id', '<>', 0)
            ->where('language_id', $language_id);

        $posts = $query->paginate(10);

        return view('back.translate.index', compact('posts', 'language'));
    }

    public function edit($id)
    {
        $post = Post::find($id);
        $url = config('medias.url');
        $tags = '';

        foreach($post->tags as $tag) {
            $tags .= $tag->tag.',';
        }
        $post->tags = substr($tags, 0, -1);

        return view('back.translate.edit', compact('post', 'url'));
    }


    public function update(PostRequest $request, $id)
    {
        $post = Post::find($id);

        $post->title = $request->title;

        if ($request->hasFile('thumb')) {
            $image = new ImageController();
            // Delete previous thumbs
            $image->deleteThumbs($post->user_id, $post->thumb);
            $thumb = $image->saveThumbs($request->file('thumb'), $request->user_id);
            $post->thumb = $thumb;
        }

        $post->category_id = $request->category_id;
        $post->subcategory = ltrim($request['subcategory']);
        $post->content = $request['content'];
        $post->photographer = $request['photographer'];
        $post->styling = $request['styling'];
        $post->description = $request['description'];
        $post->keywords = $request['keywords'];
        $post->outfit = $request['outfit'];
        $post->location = $request['location'];
        $post->website = $request['website'];
        $post->save();

        $language = Language::find($post->language_id);

        return redirect()->route('admin.trans.index', $language->name);
    }

}