<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Slider;
use App\Models\Post;
use App\Repositories\BlogRepository;


class SliderController extends Controller
{

    protected $blog_gestion;

    /**
     * SliderController constructor.
     * @param BlogRepository $blog_gestion
     */
    public function __construct(BlogRepository $blog_gestion)
    {
        $this->blog_gestion = $blog_gestion;
        $this->middleware('admin');
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $slider = Slider::with('post')->get();

        return view('back.slider.index', compact('slider'));
    }

    /**
     * @return mixed
     */
    public function create()
    {
        $url = config('medias.url');

        $posts = Post::where('original_id', 0)->orderBy('title', 'ASC')->where('active', 1)->lists('title', 'id');

        return view('back.slider.create', compact('url', 'posts'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $slider = new Slider(); 
        $slider->post_id = $request->post;
        $slider->save();

        return redirect()->route('admin.slider.index');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return redirect(\App::getLocale() . '/');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $slide = Slider::find($id);

        $posts = Post::where('original_id', 0)->orderBy('title', 'ASC')->lists('title', 'id');

        return view('back.slider.edit', compact('slide', 'posts'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $slide = Slider::find($id);
        $slide->post_id = $request->post;
        $slide->update();

        return redirect()->route('admin.slider.index');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $slide = Slider::find($id);
        $slide->delete();

        return redirect()->back();
    }
}
