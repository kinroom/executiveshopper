<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Http\Request;

use App\Http\Requests;

class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roots = ProductCategory::roots()->get();

        return view('back.shop.category.index', compact('roots'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roots = ProductCategory::roots()->get();
        
        return view('back.shop.category.create', compact('roots'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $parentId = $request->request->get('parent_id') == 0 ? null : $request->request->get('parent_id');

        $name = $request->request->get('name');

        if ($parentId) {
            $root = ProductCategory::find($parentId);

            $root->children()->create([
                'slug' => str_slug($name, '-'),
                'name' => $name,
                'keywords' => $request->request->get('keywords'),
                'description' => $request->request->get('description')
            ]);
        } else {
            $category = new ProductCategory();
            $category->slug = str_slug($name, '-');
            $category->fill($request->all());
            $category->parent_id = $parentId;
            $category->save();
        }

        return redirect()->route('product_category.index');
    }

    /**
     * @param Request $request
     * @param $hierarchy
     * @return mixed
     */
    public function show(Request $request, $hierarchy = null)
    {
        if ($hierarchy !== 'all') {
            $categories = explode('/', $hierarchy);
            $category = ProductCategory::where('slug', $categories[0])->where('parent_id', null)->first();

            if (count($categories) > 1) {
                foreach ($category->getDescendants() as $child) {
                    if ($child->slug == end($categories)) {
                        $category = $child;
                        break;
                    }
                }
            }

         
            $products = Product::categorized($category)->paginate(6);
            $counter = Product::counter($category);
        } else {
            $products = Product::orderBy('created_at', 'DESC')->paginate(6);
            $counter = Product::count();
        }


        if ($request->ajax()) {
            return view('front.shop.list', compact('products'))->render();
        }

        $roots = ProductCategory::roots()->get();

        if (!is_numeric($counter)) $counter = 0;

        $categoryName = $hierarchy == null || $hierarchy == 'all' ? 'All' : $category->name;
     
        return view('front.shop.category', compact('products', 'roots', 'category', 'categoryName', 'counter'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = ProductCategory::find($id);
        
        $roots = ProductCategory::roots()->get();

        return view('back.shop.category.edit', compact('category', 'roots'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $parentId = $request->request->get('parent_id');
        $parentId = $parentId == 0 ? null : $parentId;

        $category = ProductCategory::find($id);
        $category->fill($request->all());
        $category->parent_id = $parentId;
        $category->update();

        return redirect()->route('product_category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = ProductCategory::find($id);
        $category->delete();

        return redirect()->back();
    }
}
