<?php namespace App\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\App;

use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;

use App\Repositories\BlogRepository;
use App\Repositories\UserRepository;

use App\Models\Category;
use App\Models\Post;
use App\Jobs\SendMail;
use App\Models\User;
use App\Models\Language;
use App\Models\Tag;
use App\Models\Role;


class BlogController extends Controller {

    protected $blog_gestion;

    protected $user_gestion;

    protected $nbrPages;

    protected $tag;

    protected $categories;

    private $conditions;

    /**
     * BlogController constructor.S
     * @param BlogRepository $blog_gestion
     * @param UserRepository $user_gestion
     * @param Tag $tag
     */
    public function __construct(BlogRepository $blog_gestion, UserRepository $user_gestion, Tag $tag)
    {
        parent::__construct();
        $this->tag = $tag;
        $this->user_gestion = $user_gestion;
        $this->blog_gestion = $blog_gestion;
        $this->nbrPages = 12;

        $locale = App::getLocale() ? App::getLocale() : 'en';
        $language = Language::where('name', $locale)->first();

        $this->conditions = ['active' => 1, 'language_id' => $language->id];

        $this->middleware('ajax', ['only' => ['updateSeen', 'updateActive', 'updateLanguage', 'buyService', 'upLike']]);
        $this->middleware('redac', ['only' => ['index', 'indexOrder', 'see', 'edit', 'update', 'destroy', 'updateSeen', 'updateActive', 'updateLanguage', 'buyService']]);
        $this->middleware('blogger', ['only' => ['create', 'store', '']]);
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return redirect(route('blog.order', [
            'name' => 'posts.created_at',
            'sens' => 'asc'
        ]));
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function indexOrder(Request $request)
    {
        $statut = $this->user_gestion->getStatut();

        $posts = $this->blog_gestion->index(
            10,
            ($statut == 'admin' || $statut == 'redac') ? null : $request->user()->id,
           'id',
            $request->sens
        );


        $links = $posts->appends([
            'name' => $request->name,
            'sens' => $request->sens
        ]);

        if($request->ajax()) {
            return response()->json([
                'view' => view('back.blog.table', compact('statut', 'posts'))->render(),
                'links' => $links->setPath('order')->render()
            ]);
        }

        $links->setPath('')->render();

        $order = (object)[
            'name' => $request->name,
            'sens' => 'sort-' . $request->sens
        ];


        $languages = Language::lists('name', 'id');
        $languages->prepend('Choose language:');

        return view('back.blog.index', compact('posts', 'links', 'order', 'languages'));
    }

    /**
     * @param Guard $auth
     * @return mixed
     */
    public function create(Guard $auth)
    {
        $profile = $auth->user();

        $role = Role::where('id', $profile->role_id)->pluck('slug');

        $user_id = $profile->id;

        $url = config('medias.url');
        $categories = Category::get();

        $count = Post::where('user_id', $user_id)->where($this->conditions)->where('active_seen', 0)->count();

        return view('back.blog.create')->with(compact('url', 'categories','count', 'user_id', 'role'));
    }


    /**
     * @param PostRequest $request
     * @return mixed
     */
    public function store(PostRequest $request)
    {
        $input = array_map('trim', $request->all());
        $input['category_id'] = $request->category_id;
        $input['subcategory'] = ltrim($request->subcategory);

        if ($request->hasFile('thumb')) {
            $thumb = new ImageController();
            $thumb = $thumb->saveThumbs($request->file('thumb'), $request->user_id);
            $input['thumb_img'] = $thumb;
        }

        $request->replace($input);

        $this->blog_gestion->store($request->all(), $request->user()->id);

        return redirect(\App::getLocale() . '/profile/approving-posts/');
    }

    /**
     * @param Guard $auth
     * @param $category
     * @param $id
     * @return mixed
     */
    public function see(Guard $auth, $category, $id)
    {
        if (\Auth::check()) {
            $user = User::find($auth->user()->id);
        }

        $post = Post::find($id);

        $author = User::find($post->user_id);

        return view('back.blog.show', compact('post', 'category', 'user', 'author'));
    }

    /**
     * @param Guard $auth
     * @param $category
     * @param $slug
     * @return mixed
     */
    public function show($category, $slug)
    {
        $post_id = Post::where('slug', $slug)->pluck('id');

        $latest = Post::where($this->conditions)->where('id', '<>', $post_id)->orderBy('created_at', 'DESC')->take(3)->get();
        if (count($latest) > 0) $latest[0]['content'] = $this->getSummary($latest[0]['content'], $counttext = 40, $sep = ' ');

        $most_viewed = Post::where($this->conditions)->orderBy('views', 'DESC')->take(3)->get();
        if (count($most_viewed) > 0) $most_viewed[0]['content'] = $this->getSummary($most_viewed[0]['content'], $counttext = 40, $sep = ' ');

        $category_id = Category::where('name', $category)->pluck('id');

        $more_articles = Post::where($this->conditions)->where('category_id', $category_id)->where('id', '<>', $post_id)->orderBy('id', 'DESC')->take(9)->get();

        if (($this->blog_gestion->show($category, $slug)) == 'fail') {
            return redirect('/');
        }

        return view('front.blog.show',  array_merge($this->blog_gestion->show($category, $slug)), compact('latest', 'most_viewed', 'more_articles'));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $post = Post::find($id);
        $url = config('medias.url');
        $categories = Category::get();
        $tags = '';

        foreach($post->tags as $tag) {
            $tags .= $tag->tag.',';
        }
        $post->tags = substr($tags, 0, -1);

        return view('back.blog.edit', compact('post', 'url', 'categories'));
    }

    /**
     * @param PostRequest $request
     * @param $id
     * @return mixed
     */
    public function update(PostRequest $request, $id)
    {
        $post = Post::find($id);

        $post->title = $request->title;

        if ($request->hasFile('thumb')) {
            $image = new ImageController();
            // Delete previous thumbs
            $image->deleteThumbs($post->user_id, $post->thumb);
            $thumb = $image->saveThumbs($request->file('thumb'), $request->user_id);
            $post->thumb = $thumb;
        }

        $post->category_id = $request->category_id;
        $post->subcategory = ltrim($request['subcategory']);
        $post->content = $request['content'];
        $post->description = $request['description'];
        $post->keywords = $request['keywords'];
        $post->photographer = $request['photographer'];
        $post->styling = $request['styling'];
        $post->outfit = $request['outfit'];
        $post->location = $request['location'];
        $post->website = $request['website'];
        $post->save();

        return redirect()->route('admin.blog.order');
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function updateLanguage(Request $request, $id)
    {
        $this->blog_gestion->updateLanguage($request->all(), $id);

        return response()->json();
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function updateSeen(Request $request, $id)
    {
        $this->blog_gestion->updateSeen($request->all(), $id);

        return response()->json();
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function updateActive(Request $request, $id)
    {
        $this->blog_gestion->updateActive($request->all(), $id);

        return response()->json();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $post = $this->blog_gestion->getById($id);

        $image = new ImageController();
        // Delete previous thumbs
        $image->deleteThumbs($post->user_id, $post->thumb);

        $this->authorize('change', $post);
        $post->sliders()->delete();
        $post->stickyPost()->delete();
        $this->blog_gestion->destroy($post);

        return redirect()->route('admin.blog.order')->with('ok', trans('back/blog.destroyed'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function tag(Request $request)
    {
        $tag = $request->input('tag');
        $articles = $this->blog_gestion->indexTag($this->nbrPages, $tag);
        $count = $this->blog_gestion->countPostsWithTag($tag);
        $categoryName = 'tag';
        return view('front.category.index', compact('articles', 'count', 'tag', 'categoryName'));
    }

    /**
     * @param Guard $auth
     * @param Request $request
     */
    public function upLike(Guard $auth, Request $request)
    {
        $user = $auth->user();
        $user_id = $user->id;
        $post_id = $request->post_id;
        $this->user_gestion->upLike($user_id, $post_id);

        $post = Post::find($request->post_id);
        $post->likes += 1;
        $post->save();
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function search(Request $request)
    {
        $search = $request->input('search');
        $articles = $this->blog_gestion->search($this->nbrPages, $search);
        $count = $this->blog_gestion->countPostsBySearch($search);
        $categoryName = 'search';

        $name = $request->input('search');
        
        return view('front.category.index', compact('articles', 'count', 'search', 'categoryName'));
    }

    /**
     * @param Request $request
     * @return string
     */
    public function buyService(Request $request)
    {
        SendMail::buyService($request);
        return 'ok';
    }
}
