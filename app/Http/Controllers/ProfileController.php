<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Vacancy;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Contracts\Auth\Guard;
use App\Models\Profile;
use App\Models\Company;
use App\Models\Post;
use App\Models\Role;

class ProfileController extends Controller
{
    public $user;

    public $count;

    public $availability;

    /**
     * ProfileController constructor.
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        parent::__construct();
        $this->middleware('auth');
        $this->middleware('blogger', ['only' => ['show', 'loadPosts', 'activatePosts', 'deletePost']]);
        $this->middleware('employer', ['only' => ['myVacancies', 'loadVacancies', 'deleteVacancy']]);

        $this->user = $auth->user();
        $count = Post::where('user_id', $this->user['id'])->where('active', 1)->where('active_seen', 0)->count();
        $this->count = $count;

        $this->availability = [
            '0' => 'Freelance',
            '1' => 'Internship'
        ];

        $roleId = $this->user ? $this->user->role_id : 2;

        $role = Role::where('id', $roleId)->pluck('slug');

        view()->share('count', $count);
        view()->share('role', $role);
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $profile = $this->user;
        $companies = Company::where('user_id', $profile->id)->get();

        return view('back.profile.index', compact('profile', 'companies'));
    }

    /**
     * @param Request $request
     */
    public function store(Request $request)
    {
        if ($request->hasFile('photo')) {
            $image = new ImageController();
            $prev_photo = \App\Models\User::where('id', $request->id)->pluck('avatar');

            if ($prev_photo !== '') {
                $image->deletePhoto($request->id, $prev_photo);
            }
            $image->savePhoto($request->file('photo'), $request->id, 340, 340);
        }

        $profile = new Profile();
        $profile->saveProfile($request);
    }

    

    /**
     * @param $posts
     * @return mixed
     */
    public function show($posts)
    {
        if ($posts == 'posts') {
            $posts = Post::where('user_id', $this->user['id'])->where('original_id', 0)->orderBy('id', 'DESC')->take(10)->get();
            if (count($posts) > 0) {
                $more_posts = Post::where('user_id', $this->user['id'])->where('original_id', 0)->where('id', '<', $posts[count($posts) - 1]['id'])->orderBy('id', 'DESC')->take(1)->get();
            }
            return view('back.profile.posts', compact('posts', 'more_posts'));
        } elseif ($posts == 'active-posts') {

            if ($this->count < 10 && $this->count > 0) {
                $active_seen = Post::where('user_id', $this->user['id'])->where('active', 1)->where('original_id', 0)->where('active_seen', 0)->orderBy('id', 'DESC')->take($this->count)->get();
                $not_seen = Post::where('user_id', $this->user['id'])->where('active', 1)->where('original_id', 0)->where('active_seen', 1)->orderBy('id', 'DESC')->take(10 - $this->count)->get();
                $active_posts = $active_seen->merge($not_seen);
                if (count($active_posts) > 0) {
                    $more_posts = Post::where('user_id', $this->user['id'])->where('active', 1)->where('original_id', 0)->where('active_seen', 1)->where('id', '<', $active_posts[count($active_posts) - 1]['id'])->orderBy('id', 'DESC')->take(1)->get();
                }
            } elseif ($this->count > 10) {
                $active_posts = Post::where('user_id', $this->user['id'])->where('active', 1)->where('original_id', 0)->where('active_seen', 0)->where('active_seen', 0)->orderBy('id', 'DESC')->take(10)->get();
                if (count($active_posts) > 0) {
                    $more_posts = Post::where('user_id', $this->user['id'])->where('active', 1)->where('original_id', 0)->where('active_seen', 0)->where('id', '<', $active_posts[count($active_posts) - 1]['id'])->orderBy('id', 'DESC')->take(1)->get();
                }
            } else {
                $active_posts = Post::where('user_id', $this->user['id'])->where('active', 1)->where('original_id', 0)->where('active_seen', 1)->where('active_seen', 0)->orderBy('id', 'DESC')->take(10)->get();
                if (count($active_posts) > 0) {
                    $more_posts = Post::where('user_id', $this->user['id'])->where('active', 1)->where('original_id', 0)->where('active_seen', 1)->where('id', '<', $active_posts[count($active_posts) - 1]['id'])->orderBy('id', 'DESC')->take(1)->get();
                }
            }

            return view('back.profile.active-posts', compact('active_posts', 'more_posts'));
        } else {
            $approving_posts = Post::where('user_id', $this->user['id'])->where('active', 0)->where('original_id', 0)->orderBy('id', 'DESC')->take(10)->get();
            if (count($approving_posts) > 0) {
                $more_posts = Post::where('user_id', $this->user['id'])->where('active', 0)->where('original_id', 0)->where('id', '<', $approving_posts[count($approving_posts) - 1]['id'])->orderBy('id', 'DESC')->take(1)->get();
            }
            return view('back.profile.approving-posts', compact('approving_posts', 'more_posts'));
        }
    }

    public function loadPosts(Request $request)
    {
        $page = 10 * $request->page;

        if ($request->category == 'posts') {
            $posts = Post::where('user_id', $request->user_id)->where('original_id', 0)->orderBy('id', 'DESC')->take(10)->offset($page)->get();
            $more_posts = Post::where('user_id', $request->user_id)->where('original_id', 0)->where('id', '<', $posts[count($posts) - 1]['id'])->orderBy('id', 'DESC')->take(1)->get();
        } elseif ($request->category == 'approving_posts') {
            $posts = Post::where('user_id', $this->user['id'])->where('active', 0)->where('original_id', 0)->orderBy('id', 'DESC')->take(10)->offset($page)->get();
            $more_posts = Post::where('user_id', $request->user_id)->where('active', 0)->where('original_id', 0)->where('id', '<', $posts[count($posts) - 1]['id'])->orderBy('id', 'DESC')->take(1)->get();
        } else {
            $qty = $request->qty - $page;
            if ($qty > 10) {
                $posts = Post::where('user_id', $this->user['id'])->where('active', 1)->where('original_id', 0)->where('active_seen', 0)->orderBy('id', 'DESC')->take(10)->offset($page)->get();
                $more_posts = Post::where('user_id', $request->user_id)->where('active', 1)->where('original_id', 0)->where('active_seen', 0)->where('id', '<', $posts[count($posts) - 1]['id'])->orderBy('id', 'DESC')->take(1)->get();
            } elseif ($qty > 0) {
                $active_seen = Post::where('user_id', $this->user['id'])->where('active', 1)->where('original_id', 0)->where('active_seen', 0)->orderBy('id', 'DESC')->take($qty)->offset($page)->get();
                $last_qty = 10 - $qty;
                $not_seen = Post::where('user_id', $this->user['id'])->where('active', 1)->where('original_id', 0)->where('active_seen', 1)->orderBy('id', 'DESC')->take($last_qty)->get();
                $posts = $active_seen->merge($not_seen);
                $more_posts = Post::where('user_id', $request->user_id)->where('active', 1)->where('original_id', 0)->where('active_seen', 1)->where('id', '<', $posts[count($posts) - 1]['id'])->orderBy('id', 'DESC')->take(1)->get();
            } else {
                if ($page > $this->count + 10) {
                    $page = $page - $this->count;
                    $posts = Post::where('user_id', $this->user['id'])->where('active', 1)->where('original_id', 0)->where('active_seen', 1)->orderBy('id', 'DESC')->take(10)->offset($page)->get();
                } else {
                    $qty = (ceil($this->count/10) * 10) - $this->count;
                    $posts = Post::where('user_id', $this->user['id'])->where('active', 1)->where('original_id', 0)->where('active_seen', 1)->orderBy('id', 'DESC')->take(10)->offset($qty)->get();
                }
            }
            $more_posts = Post::where('user_id', $request->user_id)->where('active', 1)->where('original_id', 0)->where('active_seen', 1)->where('id', '<', $posts[count($posts) - 1]['id'])->orderBy('id', 'DESC')->take(1)->get();
        }


        $string = '';
        foreach ($posts as $post) {
            if ($post->active == 1 && $post->active_seen == 0) {
                $string .= "<li class='clearfix myposts-new-item'>";
            } else {
                $string .= "<li class='clearfix'>";
            }

            $string .= "
                <div class='myposts-item-preview-left'>";
                    if ($post->thumb != ''  && file_exists(base_path() .'/public/img/uploads/thumbs/first/' . $post->user_id . '/' . $post->thumb)) {
                        $string .= "<img src = '/img/uploads/thumbs/first/$post->user_id/$post->thumb' alt = '$post->title'>";
                    } else {
                        $string .= "<img src = '/img/post-default-mini.jpg'>";
                    }
            $string .= "
                </div>
                <div class='myposts-item-right'>";
            if ($post->active == 0) {
                $string .= "<h2><a href='#'>$post->title</a></h2>";
            } else {
                $string .= "<h2><a href='/activate-post/$post->id/'>$post->title</a></h2>";
            }
                $string .= "
                    <span class='myposts-date'>".date('d.m.Y', strtotime($post->created_at))."</span>
                    <span class='delete-post-button' data-toggle='modal' data-target='#modal-delete-post' id='$post->id'></span>
                            </div>
                        </li>";
        }

        // if it is not last post in database, show more button
        if (isset($more_posts) && count($more_posts) > 0){
            $page = $request->page + 1;
            $string .= "
            <button class='more-posts' id='$page+$request->user_id+$request->category+$request->qty' onclick='profilePosts(this.id)'>
                <span>".trans('back/my-posts.show-more')."</span>
                <i class='fa fa-angle-down show-more-icon'></i>
            </button>
        ";
        }
        return $string;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function activatePost($id)
    {
        $post = Post::find($id);
        $category = strtolower(Category::where('id', $post->category_id)->pluck('name'));

        if ($post->active_seen == 0) {
            $post->active_seen = 1;
            $post->save();
        }

        return redirect()->route('articles.post.show', [$category, $post->slug]);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function deletePost(Request $request)
    {
        $post = Post::find($request->id);

        $image = new ImageController();
        // Delete thumbs
        $image->deleteThumbs($post->user_id, $post->thumb);

        $post->delete();

        return redirect()->back();
    }

    /**
     * @return mixed
     */
    public function myVacancies()
    {
        $vacancies = Vacancy::where('user_id', $this->user['id'])->orderBy('id', 'desc')->take(10)->get();

        $companies =[];
        foreach ($vacancies as $vacancy) {
            $vacancy->description = $this->getSummary ($vacancy->description, $counttext = 40, $sep = ' ');
            $vacancy->availability_id = $this->availability[$vacancy->availability_id];
            $new = Vacancy::find($vacancy->id)->company;

            array_push($companies, $new);
        }

        if (count($vacancies) > 0) {
            $more_vacancies = Vacancy::where('user_id', $this->user['id'])->where('id', '<', $vacancies[count($vacancies) - 1]['id'])->orderBy('id', 'desc')->take(1)->get();
        }

        $company_exist = Company::where('user_id', $this->user['id'])->take(1)->get();

        return view('back.profile.vacancies', compact('vacancies', 'more_vacancies', 'company_exist', 'companies'));
    }

    /**
     * @param Request $request
     * @return string
     */
    public function loadVacancies(Request $request)
    {
        $locale = substr($request->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);

        $page = 10 * $request->page;
        $vacancies = Vacancy::where('user_id', $this->user['id'])->orderBy('id', 'desc')->take(10)->offset($page)->get();

        if (count($vacancies) > 0) {
            $more_vacancies = Vacancy::where('user_id', $this->user['id'])->orderBy('id', 'desc')->where('id', '<', $vacancies[count($vacancies) - 1]['id'])->orderBy('id', 'desc')->take(1)->get();
        }

        $string = '';

        foreach ($vacancies as $vacancy) {
            $vacancy->description = $this->getSummary ($vacancy->description, $counttext = 40, $sep = ' ');
            $vacancy->availability_id = $this->availability[$vacancy->availability_id];
            $company = Vacancy::find($vacancy->id)->company;

            $string .= "
                <li class='clearfix'>
                    <div class='clearfix'>
                        <div class='vacancies-left'>";
                            if ($vacancy->logo != '' && file_exists(base_path() .'/public/img/uploads/companies/' . $company->user_id . '/' . $company->id . '/' . $company->logo)) {
                                $string .=  "<img src='/img/uploads/companies/$company->user_id/$company->id/$company->logo/' alt='Company name'>";
                            }  else {
                                $string .=  "<img src='/img/company_default.png' alt='Company name'>";
                            }
            $string .= "
                        </div>
                        <div class='vacancies-right clearfix'>
                            <div class='job-heading'>
                                <h2><a href='/$locale/fashion-job/$vacancy->id/'>$vacancy->title</a></h2>
                                <span class='job-company-name'>$company->name</span>
                                <span class='job-date'>".date('d.m.Y', strtotime($vacancy->created_at))."</span>
                                <span class='delete-post-button' data-toggle='modal' data-target=''#modal-delete-post' id='$vacancy->id'></span>
                            </div>
                            <div class='job-text'>
                                <p>$vacancy->description</p>
                            </div>
                            <div class='vacancy-bottom clearfix'>
                                <div class='clearfix'>
                                    <div class='vacancy-bottom-left'>
                                                <span>".trans('back/my-vacancies.salary').":</span>
                                    </div>
                                    <div class='vacancy-bottom-right'>
                                        <span>$vacancy->salary</span>
                                    </div>
                                </div>
                                <div class='clearfix'>
                                    <div class='vacancy-bottom-left'>
                                        <span>".trans('back/my-vacancies.availability').":</span>
                                    </div>
                                    <div class='vacancy-bottom-right'>
                                        <span>$vacancy->availability_id</span>
                                    </div>
                                </div>
                                <div class='clearfix'>
                                    <div class='vacancy-bottom-left'>
                                        <span>".trans('back/my-vacancies.location').":</span>
                                    </div>
                                    <div class='vacancy-bottom-right'>
                                        <span>$vacancy->country</span>
                                        <span class='vacancy-bottom-city'>$vacancy->city</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>";
        }

        // if it is not last post in database, show more button
        if (isset($more_vacancies) && count($more_vacancies) > 0) {
            $page = $request->page + 1;
            $string .= "
            <button class='more-posts' id='$page+$request->user_id' onclick='profileVacancies(this.id)'>
                <span>".trans('back/my-posts.show-more')."</span>
                <i class='fa fa-angle-down show-more-icon'></i>
            </button>
        ";
        }

        return $string;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function deleteVacancy(Request $request)
    {
        $vacancy = Vacancy::find($request->id);
        $vacancy->delete();
        
        return redirect()->back();
    }
}
