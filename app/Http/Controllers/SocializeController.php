<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Role;
use Illuminate\Support\Facades\Route;

class SocializeController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');

        $role = Route::current() ? Route::current()->getParameter('role') : 1;

        Session::put('locale', \App::getLocale());

        if ($role !== null) {
            Session::put('role', $role);
        }
    }
    /**
     * @param Request $request
     * @return mixed
     */
    public function loginWithFacebook(Request $request)
    {
        session_save_path("/tmp");
        $code = $request->get('code');
        $fb = \OAuth::consumer('Facebook', url().'/facebook');

        if (!is_null($code)) {
            $token = $fb->requestAccessToken($code);
            $profile = json_decode($fb->request('/me?fields=first_name,last_name,email,age_range,gender'), true);
            $avatar = 'http://graph.facebook.com/'. $profile['id'].'/picture?type=large';
            $profile['service'] = 'facebook';
            $profile['avatar'] = $avatar;

            if ($this->createOrLogin($profile)) {
                $url = Session::get('locale') . '/profile';
            } else {
                $url = Session::get('locale') . '/auth/register';
            }

            return redirect((string)$url);
        } else {

                $url = $fb->getAuthorizationUri();
                return redirect((string)$url);
            }
        }

    /**
     * @param Request $request
     * @return mixed
     */
    public function loginWithVkontakte(Request $request)
    {
        session_save_path("/tmp");
        $code = $request->get('code');
        $vk = \OAuth::consumer('Vkontakte', url(). '/' . \App::getLocale() . '/vkontakte');

        if (!is_null($code)) {
            $token = $vk->requestAccessToken($code);

            $profile = json_decode($vk->request('https://api.vk.com/method/users.get?fields=photo_big,email'), true)["response"][0];
            $profile['email'] = $token->getExtraParams()['email'];
            $profile['service'] = 'vkontakte';
            $profile['avatar'] = $profile['photo_big'];
            $profile['id'] = $profile['uid'];
            unset($profile['photo_big'], $profile['uid']);

            if ($this->createOrLogin($profile)) {
                $url = Session::get('locale') . '/profile';
            } else {
                $url = Session::get('locale') . '/auth/register';
            }

            return redirect((string)$url);
            } else {
                $url = $vk->getAuthorizationUri();

                return redirect((string)$url);
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function loginWithGoogle(Request $request)
    {
        $code = $request->get('code');
        $googleService = \OAuth::consumer('Google', url().'/google');

        if (!is_null($code)) {
            $token = $googleService->requestAccessToken($code);
            $profile = json_decode($googleService->request('https://www.googleapis.com/oauth2/v1/userinfo'), true);
            $profile['service'] = 'google';
            $profile['avatar'] = $profile['picture'];
            $profile['first_name'] = $profile['given_name'];
            $profile['last_name'] = $profile['family_name'];
            unset($profile['picture'], $profile['given_name'], $profile['family_name']);

            if ($this->createOrLogin($profile)) {
                $url = Session::get('locale') . '/profile';
            } else {
                $url = Session::get('locale') . '/auth/register';
            }

            return redirect((string)$url);
        } else {
            $url = $googleService->getAuthorizationUri();

            return redirect((string)$url);
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function loginWithTwitter(Request $request)
    {
        $token  = $request->get('oauth_token');
        $verify = $request->get('oauth_verifier');
        $tw = \OAuth::consumer('Twitter', url().'/twitter');

        if (!is_null($token) && ! is_null($verify)) {
            $token = $tw->requestAccessToken($token, $verify);
            $profile = json_decode($tw->request('account/verify_credentials.json'), true);
            $profile['service'] = 'twitter';
            $profile['first_name'] = explode(' ',$profile['name'])[0];
            $profile['last_name'] = explode(' ',$profile['name'])[1];
            $profile['avatar'] = str_replace('_normal', '', $profile['profile_image_url']);
            $profile['email'] = 'none';

            if ($this->createOrLogin($profile)) {
                $url = Session::get('locale') . '/profile';
            } else {
                $url = Session::get('locale') . '/auth/register';
            }

            return redirect((string)$url);
        } else {
            $reqToken = $tw->requestRequestToken();
            $url = $tw->getAuthorizationUri(['oauth_token' => $reqToken->getRequestToken()]);

            return redirect((string)$url);
        }
    }


    /**
     * @param $profile
     * @return bool
     */
    public function createOrLogin($profile)
    {
        $user = User::where('email', $profile['email'])->first();

        if ($user !== null) {
            \Auth::loginUsingId($user->id);

            return true;
        } else {
            if (Session::has('role'))
            {
                $role_id = Role::where('slug', Session::get('role'))->pluck('id');

                Session::forget('role');

                $user = new User();
                $user->service = $profile['service'];
                $user->firstname = $profile['first_name'];
                $user->lastname = $profile['last_name'];
                $user->avatar = $profile['avatar'];
                $user->email = $profile['email'];
                $user->contact_email = $profile['email'];
                $user->password = bcrypt('Burbelo'.$profile['id'].'Blog');
                $user->role_id = $role_id;
                $user->seen = 0;
                $user->valid = 1;
                $user->confirmed = 1;
                $user->confirmation_code = NULL;
                $user->save();

                \Auth::loginUsingId($user->id);
                return true;
            } else {
                return false;
            }

        }

    }

}