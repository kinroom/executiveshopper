<?php

namespace App\Http\Controllers;

use App\Http\Requests;

class ImageController extends Controller
{
    /**
     * ImageController constructor.
     */
    public function __construct()
    {
        require public_path().'/filemanager/connectors/php/inc/wideimage/lib/WideImage.php';
    }

    /**
     * @param $file
     * @param $user_id
     * @param $width
     * @param $height
     * @return mixed
     */
    public function savePhoto($file, $user_id, $width, $height)
    {
        $file->move('img/uploads/profiles/'.$user_id, $file->getClientOriginalName());
        $photo = $file->getClientOriginalName();
        $path = 'img/uploads/profiles/'.$user_id.'/'.$photo;

        $image = \WideImage::load($path);
        $resized = $image->resize($width, $height, 'outside')->crop(0, 0, $width, $height);
        $resized->saveToFile($path, 80);

        return $photo;
    }

    /**
     * @param $user_id
     * @param $photo
     */
    public function deletePhoto($user_id, $photo)
    {
        $prev_photo = 'img/uploads/profiles/'.$user_id.'/'.$photo;
        if (file_exists($prev_photo)) {
            unlink($prev_photo);
        }
    }

    /**
     * @param $file
     * @param $user_id
     * @param $company_id
     * @param $width
     * @param $height
     * @return mixed
     */
    public function saveLogo($file, $user_id, $company_id, $width, $height)
    {

        $logo_path = $this->createLogoFolder($user_id, $company_id);

        $file->move($logo_path, $file->getClientOriginalName());
        $logo = $file->getClientOriginalName();
        $path = $logo_path.'/'.$logo;

        $image = \WideImage::load($path);
        $resized = $image->resize($width, $height, 'outside')->crop(0, 0, $width, $height);
        $resized->saveToFile($path, 80);

        return $logo;
    }

    /**
     * @param $user_id
     * @param $company_id
     * @return string
     */
    public function createLogoFolder($user_id, $company_id)
    {
        $path = 'img/uploads/companies/'.$user_id.'/'.$company_id;

        if(!is_dir($path)) {
            mkdir($path, 0755, true);
        }

        return $path;
    }

    /**
     * @param $user_id
     * @param $company_id
     */
    public function deleteCompanyFolder($user_id, $company_id)
    {
        $path = 'img/uploads/companies/'.$user_id.'/'.$company_id;
        if ($objs = glob($path."/*")) {
            foreach($objs as $obj) {
                is_dir($obj) ? deleteCompanyFolder($obj) : unlink($obj);
            }
        }
        rmdir($path);

    }

    /**
     * @param $user_id
     * @param $company_id
     * @param $logo
     */
    public function deleteLogo($user_id, $company_id, $logo)
    {
        $logo = 'img/uploads/companies/'.$user_id.'/'.$company_id.'/'.$logo;

        if (file_exists($logo)) {
            unlink($logo);
        }
    }


    /**
     * @param $picture
     * @param $product_id
     */
    public function saveProductThumb($picture, $product_id)
    {
        $dir = 'img/uploads/shop/products/'. $product_id. '/thumbs/';

        if (!is_dir($dir)) {
            mkdir($dir, 0755, true);
        }

        $name = $picture->getClientOriginalName();
        $path = $dir . $name;
        $picture->move($dir, $name);
        
        \WideImage::load($path)->resize(320, 440, 'outside')->crop('center', 'center', 320, 440)->saveToFile($path, 80);
    }

    /**
     * @param $product_id
     * @param $name
     */
    public function deleteProductImage($product_id, $name)
    {
        $path = 'img/uploads/shop/products/'. $product_id. '/images/' . $name;

        if (file_exists($path)) unlink($path);
    }

    /**
     * @param $product_id
     * @param $name
     */
    public function deleteProductThumb($product_id, $name)
    {
        $path = 'img/uploads/shop/products/'. $product_id. '/thumbs/' . $name;

        if (file_exists($path)) unlink($path);
    }

    /**
     * @param $product_id
     */
    public function deleteProduct($product_id)
    {
        $dir = 'img/uploads/shop/products/'. $product_id;

        $this->rrmdir($dir);
    }

    /**
     * @param $dir
     */
    function rrmdir($dir)
    {
        if (is_dir($dir))
        {
            $objects = scandir($dir);

            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir."/".$object) == "dir"){
                        $this->rrmdir($dir."/".$object);
                    } else {
                        unlink($dir."/".$object);
                    }
                }
            }

            reset($objects);
            rmdir($dir);
        }
    }


    /**
     * @param $picture
     * @param $product_id
     */
    public function saveProductImage($picture, $product_id)
    {
        $dir = 'img/uploads/shop/products/'. $product_id. '/images/';

        if (!is_dir($dir)) {
            mkdir($dir, 0755, true);
        }

        $name = $picture->getClientOriginalName();
        $path = $dir . $name;
        $picture->move($dir, $name);


        \WideImage::load($path)->resize(320, 440, 'outside')->crop('center', 'center', 320, 440)->saveToFile($path, 80);
    }


    /**
     * @param $file
     * @param $user_id
     * @return mixed
     */
    public function saveThumbs($file, $user_id)
    {
        $paths = $this->createFolders($user_id);

        $file->move('img/uploads/thumbs/originals/'.$user_id, $file->getClientOriginalName());
        $thumb = $file->getClientOriginalName();

        $original = 'img/uploads/thumbs/originals/'.$user_id.'/'.$thumb;

        $image = \WideImage::load($original);

        $resized = $image->resize(640, 640, 'outside')->crop('center', 'center', 640, 640);
        $resized->saveToFile($paths[0].'/'.$thumb, 80);

        $resized = $image->resize(1140, 489, 'outside')->crop('center', 'center', 1140, 489);
        $resized->saveToFile($paths[1].'/'.$thumb, 80);

        $resized = $image->resize(570, 390, 'outside')->crop('center', 'center', 570, 390);
        $resized->saveToFile($paths[2].'/'.$thumb, 80);

        $resized = $image->resize(1663, 737, 'outside')->crop('center', 'center', 1663, 737);
        $resized->saveToFile($paths[3].'/'.$thumb, 80);

        unlink($original);

        return $thumb;
    }

    /**
     * @param $user_id
     * @return array
     */
    public function createFolders($user_id)
    {
        $base_path = 'img/uploads/thumbs/first/';
        $path = $base_path.$user_id.'/';

        $paths = [
            'img/uploads/thumbs/first/'.$user_id,
            'img/uploads/thumbs/second/'.$user_id,
            'img/uploads/thumbs/third/'.$user_id,
            'img/uploads/thumbs/fourth/'.$user_id
        ];

        if(!is_dir($path)) {
            mkdir($paths[0], 0755, true);
            mkdir($paths[1], 0755, true);
            mkdir($paths[2], 0755, true);
            mkdir($paths[3], 0755, true);
        }

        return $paths;
    }

    /**
     * @param $user_id
     * @param $thumb
     */
    public function deleteThumbs($user_id, $thumb)
    {
        if ($thumb !== '') {
            $image1 = 'img/uploads/thumbs/first/' . $user_id . '/' . $thumb;
            $image2 = 'img/uploads/thumbs/second/' . $user_id . '/' . $thumb;
            $image3 = 'img/uploads/thumbs/third/' . $user_id . '/' . $thumb;
            $image4 = 'img/uploads/thumbs/fourth/' . $user_id . '/' . $thumb;

            if (file_exists($image1)) {
                unlink($image1);
                unlink($image2);
                unlink($image3);
                unlink($image4);
            }
        }
    }

    /**
     * @param $file
     * @param $width
     * @param $height
     * @return mixed
     */
    public function saveSlide($file, $width, $height)
    {
        $file->move('img/uploads/slider/', $file->getClientOriginalName());
        $slide = $file->getClientOriginalName();

        $original = 'img/uploads/slider/'.$slide;
        $image = \WideImage::load($original);
        $resized = $image->resize($width, $height, 'outside')->crop('center', 'center', $width, $height);
        $resized->saveToFile($original, 80);

        return $slide;
    }

    /**
     * @param $slide
     */
    public function deleteSlide($slide)
    {
        $slide = 'img/uploads/slider/'.$slide;

        if (file_exists($slide)) {
            unlink($slide);
        }
    }

}
