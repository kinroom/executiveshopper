<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\Category;

abstract class Controller extends BaseController {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct() {
        // Pass blog navigation to the template
        $category = Category::get();
        view()->share('blog_nav', $category);
    }

    public function getImageName($string) {
        preg_match('%<img.*?src=["\'](.*?)["\'].*?/>%i', $string, $image);
        if (count($image) > 0) {
            $img_path = explode('/', $image[1]);
            $img_name = end($img_path);
        } else { $img_name = ''; }

        return $img_name;
    }

    public function getImageAlt($string, $title = null) {
        preg_match('%<img.*?alt=["\'](.*?)["\'].*?/>%i', $string, $alt);
        if (count($alt) > 0) {
            $img_alt = $alt[1];
            if (strlen($img_alt) <= 1) {
                $img_alt = $title;
            }
        } else { $img_alt = '';}

        return $img_alt;
    }

    function getSummary($text, $counttext, $sep = ' ') {
        $text = strip_tags($text);
        $words = explode($sep, $text);
        if ( count($words) > $counttext )
            $text = join($sep, array_slice($words, 0, $counttext));
        return $text;
    }

}
