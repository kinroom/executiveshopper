<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Company;
use Illuminate\Contracts\Auth\Guard;

class CompanyController extends Controller
{
    private $user_id;

    /**
     * CompanyController constructor.
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->middleware('employer');

        parent::__construct();

        $user = $auth->user();
        if ($user) {
            $this->user_id = $user->id;
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $logo = new ImageController();

        if (($request->company_id) !== '') {

            $prev_logo = Company::where('id', $request->company_id)->pluck('logo');

            if ($request->hasFile('logo')) {
                if ($prev_logo !== '' && $prev_logo !== $request->file('logo')->getClientOriginalName()) {
                    $logo->deleteLogo($request->user_id, $request->company_id, $prev_logo);
                }
            }
        }

        $company = new Company();
        $company_id = $company->addCompany($request);

        if ($request->hasFile('logo')) {
            $logo->saveLogo($request->file('logo'), $request->user_id, $company_id, 150, 150);
        }

        return $company_id;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function destroy(Request $request)
    {

        if(\Request::ajax()) {
            $logo = new ImageController();
            $company = Company::find($request->id);
            $company->vacancy()->delete();

            if ($company->logo !== '') {
                $logo->deleteLogo($company->user_id, $company->id, $company->logo);
                $logo->deleteCompanyFolder($company->user_id, $company->id);
            }
            $company->delete();
        }

        return $request->id;
    }
}
