<?php namespace App\Http\Controllers;

use App\Repositories\VacancyRepository;
use App\Repositories\UserRepository;
use App\Repositories\BlogRepository;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Models\stickyPost;

class AdminController extends Controller {

    protected $user_gestion;

    public function __construct(UserRepository $user_gestion)
    {
        $this->middleware('redac');
		$this->user_gestion = $user_gestion;
    }


	public function admin(BlogRepository $blog_gestion, VacancyRepository $vacancy_gestion)
    {
		$nbrUsers = $this->user_gestion->getNumber();
		$nbrPosts = $blog_gestion->getNumber();
        $nbrVacancies = $vacancy_gestion->getNumber();

		return view('back.index', compact('nbrUsers', 'nbrPosts', 'nbrVacancies', 'title'));
	}

    /**
     * @return mixed
     */
    public function stickyPost()
    {
        $stickedPost = stickyPost::first();

        $post = '';

        if ($stickedPost) $post = $stickedPost->id;

        $posts = Post::where('original_id', 0)->orderBy('title', 'ASC')->where('active', 1)->lists('title', 'id');

        return view('back.users.sticky_post', compact('posts', 'post'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function newStickyPost(Request $request)
    {
        $post_id = $request->all()['post'];

        $sticky_post = stickyPost::firstOrNew(array('id' => '1'));
        $sticky_post->post_id = $post_id;
        $sticky_post->save();

        return redirect()->back();
    }
}
