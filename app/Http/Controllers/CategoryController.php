<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;

use App\Models\Advertisement;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Category;
use App\Models\PostTag;
use App\Models\Language;

class CategoryController extends Controller
{
    private $conditions;

    public function __construct()
    {
        parent::__construct();
        $locale = App::getLocale() ? App::getLocale() : 'en';
        $language = Language::where('name', $locale)->first();

        $this->conditions = ['active' => 1, 'language_id' => $language->id];
    }

    /**
     * @param $categoryName
     * @return mixed
     */
    public function show($categoryName)
    {
        $advertisement = Advertisement::where('place', 'category')->take(1)->pluck('code');
        list($category, $subcategories, $articles, $count) = $this->getArticles($categoryName);

        return view('front.category.index', compact('category', 'subcategories', 'articles', 'count', 'advertisement', 'categoryName'));
    }

    /**
     * @param $category
     * @param $slug
     * @return mixed
     */
    public function post($category, $slug)
    {
        $post_id = Post::where('slug', $slug)->pluck('id');

        $latest = Post::where($this->conditions)->where('id', '<>', $post_id)->orderBy('created_at', 'DESC')->take(3)->get();
        if (count($latest) > 0) $latest[0]['content'] = $this->getSummary($latest[0]['content'], $counttext = 40, $sep = ' ');

        $most_viewed = Post::where($this->conditions)->orderBy('views', 'DESC')->take(3)->get();
        if (count($most_viewed) > 0) $most_viewed[0]['content'] = $this->getSummary($most_viewed[0]['content'], $counttext = 40, $sep = ' ');

        $category_id = Category::where('name', $category)->pluck('id');

        $more_articles = Post::where($this->conditions)->where('category_id', $category_id)->where('id', '<>', $post_id)->orderBy('id', 'DESC')->take(9)->get();

        if (($this->blog_gestion->show($category, $slug)) == 'fail') {
            return redirect(\App::getLocale() . '/');
        }

        return view('front.blog.show',  array_merge($this->blog_gestion->show($category, $slug)), compact('latest', 'most_viewed', 'more_articles'));

    }

    /**
     * @param $categoryName
     * @param $subCategory
     * @return mixed
     */
    public function showSubcategory($categoryName, $subCategory)
    {
        $advertisement = Advertisement::where('place', 'category')->take(1)->pluck('code');
        list($category, $subcategories, $articles, $count) = $this->getArticles($categoryName, $subCategory);
        
        return view('front.category.index', compact('category', 'subcategories', 'articles', 'count', 'advertisement', 'subCategory', 'categoryName'));
    }

    /**
     * @param $categoryName
     * @param null $subcategory
     * @return array
     */
    public function getArticles($categoryName, $subcategory = null)
    {
        $category_id = Category::where('name', $categoryName)->pluck('id');
        $category = Category::find($category_id);

        $subcategories = [];

        if ($subcategory != null) {
            $subcat = explode(',', $category[0]['subcategories']);

            for ($i = 0; $i < count($subcat); $i++) {
                $subcategories[] = ltrim($subcat[$i]);
            }
            $articles = Post::where('category_id', $category_id)->where('subcategory',$subcategory)->where($this->conditions)->orderBy('id', 'DESC')->take(12)->get();
            $count = Post::where('category_id', $category_id)->where('subcategory',$subcategory)->where($this->conditions)->count();
        } elseif ($categoryName == 'all') {
            $articles = Post::where($this->conditions)->orderBy('id', 'DESC')->take(12)->get();
            $count = Post::where($this->conditions)->count();
        } else {
            $articles = Post::where('category_id', $category_id)->where($this->conditions)->orderBy('id', 'DESC')->take(12)->get();
            $count = Post::where('category_id', $category_id)->where($this->conditions)->count();
        }

        return (array($category, $subcategories, $articles, $count));
    }

    /**
     * @param Request $request
     * @return string
     */
    public function loadPosts(Request $request)
    {
        $locale = $request->lang;

        App::setLocale($locale);

        $this->conditions['language_id'] = $language_id = Language::where('name', $locale)->first()->id;
        $category = explode('.', $request->category);

        $page = 6 * $request->page;

        $categories = array_filter($category,
            function($el){ return !empty($el);}
        );

        array_shift($categories);

        $category_id = Category::where('name', $categories[0])->pluck('id');

        if ($request->search !== 'notsearch') {
            $articles = Post::where('title', 'LIKE', "%$request->search%")->where('active', 1)->orderBy('id', 'DESC')->take(6)->offset($page)->get();
            // its last post or not
            $last_post = Post::where('title', 'LIKE', "%$request->search%")->where('active', 1)->where('id', '<', $articles[count($articles) - 1]['id'])->orderBy('id', 'DESC')->take(1)->get();
        } elseif ($request->tag !== 'nottag') {

            $ids = PostTag::where('tag_id', $request->tag)->get(array('post_id'));
            $posts_id = [];

            foreach ($ids as $id){
                $posts_id[] .= $id->post_id;
            }

            $articles = Post::whereIn('id', $posts_id)->where('active', 1)->orderBy('id', 'DESC')->take(6)->offset($page)->get();
            // its last post or not
            $last_post = Post::whereIn('id', $posts_id)->where('active', 1)->orderBy('id', 'DESC')->where('id', '<', $articles[count($articles) - 1]['id'])->take(1)->get();
        } else {

            if (count($categories) > 1) {

                $articles = Post::where('category_id', $category_id)->where('subcategory', $categories[1])->where($this->conditions)->orderBy('id', 'DESC')->take(6)->offset($page)->get();
                // its last post or not
                $last_post = Post::where('category_id', $category_id)->where('subcategory', $categories[1])->where($this->conditions)->where('id', '<', $articles[count($articles) - 1]['id'])->orderBy('id', 'DESC')->take(1)->get();
            } else {
                if ($categories[0] == 'all') {
                    $articles = Post::where($this->conditions)->orderBy('id', 'DESC')->take(6)->offset($page)->get();

                    // its last post or not
                    $last_post = Post::where('id', '<', $articles[count($articles) - 1]['id'])->where($this->conditions)->orderBy('id', 'DESC')->take(1)->get();
                } else {
                    $articles = Post::where('category_id', $category_id)->where($this->conditions)->orderBy('id', 'DESC')->take(6)->offset($page)->get();
                    // its last post or not
                    $last_post = Post::where('category_id', $category_id)->where($this->conditions)->where('id', '<', $articles[count($articles) - 1]['id'])->orderBy('id', 'DESC')->take(1)->get();
                }
            }
        }

        $advertisement = Advertisement::where('place', 'category')->take(1)->offset($request->page - 1)->pluck('code');

        $string = '';

        foreach ($articles as $key => $article) {

            if ($key == 0 && isset($advertisement) && $advertisement !== '') {
                $string .=
                    "<div class='adv-blog-block clearfix'>
                                <div class='adv-blog'>
                                    <span>$advertisement</span>
                                </div>
                            </div>";
            }

            $string .= "
                <li class='latest-news-item'>
                    <div class='latest-news-preview-img-block-outer'>
                        <a href='/$locale/blog/" . $article->category->name . "/" .$article->slug. "/' class='latest-news-preview-img-block-inner'>";
            if ($article->thumb != '' && file_exists(base_path() .'/public/img/uploads/thumbs/first/' . $article->user_id . '/' . $article->thumb)) {
                $string .= "<img class='latest-news-preview-img' src = '/img/uploads/thumbs/first/". $article->user_id ."/". $article->thumb . "' alt = '".$article->title."'>";
            } else {
                $string .= "<img class='latest-news-preview-img' src = '/img/post-default-feautered.jpg' alt = '".$article->title."'>";
            }
            $string .= "
                            <div class='latest-news-preview-img-overlay'>
                                <span>". trans('front/blog.read-article') ."</span>
                                <div class='read-article-divider'>
                                </div>
                            </div>
                        </a>
                    </div>
                    <a class='latest-news-category-link' href='/$locale/category/". $article->category->name ."/'>". trans('front/category.' . $article->category->name) ."</a>
                    <h3>
                        <a class='latest-news-item-heading' href='/$locale/blog/" . $article->category->name ."/". $article->slug ."/'>".$article->title.
                "<div class='news-underline'></div>
                        </a>
                    </h3>
                    <span class='latest-news-date'>".  date('d/m/y', strtotime($article->created_at)) ."</span>
                    <span class='latest-news-author'>". trans('front/home.by') ." <a>" . $article->user->firstname . ' ' . $article->user->lastname."</a></span>
                    <div class='likes-views-buttons'>
                        <button onclick='Like($article->id)'>
                            <i class='fa fa-heart'></i>
                            <span id='post_". $article->id ."'>".$article->likes."</span>
                        </button>
                        <span class='views-number'>
                            <i class='fa fa-eye'></i>
                                <span>".$article->views."</span>
                            </span>
                    </div>
                </li>";
        }

        // if it is not last post in database, show more button
        if (isset($last_post) && count($last_post) > 0) {
            if ($request->tag !== 'nottag') {
                $string .= "<button class='more-posts' id='".($request->page)."_".$request->category."_notsearch_".$request->tag."' onclick='loadPosts(this.id)'>";
            } elseif ($request->search !== 'notsearch') {
                $string .= "<button class='more-posts' id='".$request->page."_".$request->category."_".$request->search."_nottag' onclick='loadPosts(this.id)'>";
            } else {
                $string .= "<button class='more-posts' id='".($request->page)."_".$request->category."_notsearch_nottag' onclick='loadPosts(this.id)'>";
            }

            $string .= " <span>". trans('front/blog.show-more') ."</span>
                <i class='fa fa-angle-down show-more-icon'></i>
            </button>
        ";
        }

        return $string;
    }

    /**
     * @param Request $request
     * @return string
     */
    public function loadSubcategories(Request $request)
    {
        $category = $request->category;
        $subcategories = Category::where('name', $category)->pluck('subcategories');

        if ($subcategories != '') {
            $sub_arr = explode(',', $subcategories);
        }

        $options = '';

        if (!empty($sub_arr)) {
            for ($i = 0; $i < count($sub_arr); $i++) {
                if ($i==0) {
                    $options .= "<option selected disabled>Choose a sub-category:</option>";
                }
                $options .= "<option>".trim($sub_arr[$i])."</option>";
            }
        }
        return $options;
    }
}
