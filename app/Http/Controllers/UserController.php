<?php namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use App\Repositories\RoleRepository;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Requests\RoleRequest;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\TopBlogger;
use App\Models\Post;

class UserController extends Controller {

    protected $user_gestion;

    protected $role_gestion;

    /**
     * UserController constructor.
     * @param UserRepository $user_gestion
     * @param RoleRepository $role_gestion
     */
    public function __construct(UserRepository $user_gestion, RoleRepository $role_gestion)
    {
        $this->user_gestion = $user_gestion;
        $this->role_gestion = $role_gestion;

        $this->middleware('admin', ['only' => ['index', 'indexSort', 'indexGo', 'create', 'show', 'updateSeen',
                                    'destroy', 'update', 'weekBlogger', 'liveSearch', 'newTopBlogger']]);
        $this->middleware('ajax', ['only' => 'updateSeen','buyService', 'liveSearch']);
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->indexGo('total');
    }

    /**
     * @param $role
     * @return mixed
     */
    public function indexSort($role)
    {
        return $this->indexGo($role, true);
    }

    /**
     * @param $role
     * @param bool $ajax
     * @return mixed
     */
    private function indexGo($role, $ajax = false)
    {
        $counts = $this->user_gestion->counts();
        $users = $this->user_gestion->index(20, $role);
        $links = $users->setPath('')->render();
        $roles = $this->role_gestion->all();

        if($ajax) {
            return response()->json([
                'view' => view('back.users.table', compact('users', 'links', 'counts', 'roles'))->render(),
                'links' => str_replace('/sort/total', '', $links)
            ]);
        }

        return view('back.users.index', compact('users', 'links', 'counts', 'roles'));
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return view('back.users.create', $this->role_gestion->getAllSelect());
    }

    /**
     * @param UserCreateRequest $request
     * @return mixed
     */
    public function store(UserCreateRequest $request)
    {
        $this->user_gestion->store($request->all());
        return redirect()->route('admin.user.index')->with('ok', 'Created');
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function show(User $user)
    {
        return view('back.users.show',  compact('user'));
    }


    /**
     * @param User $user
     * @return mixed
     */
    public function edit(User $user)
    {
        $roles = $this->role_gestion->getAllSelect()['select'];

        return view('back.users.edit', array_merge(compact('user', 'roles') ));
    }

    /**
     * @param UserUpdateRequest $request
     * @param $user
     * @return mixed
     */
    public function update(UserUpdateRequest $request, $user)
    {
        $this->user_gestion->update($request->all(), $user);

        return redirect()->route('admin.user.index')->with('ok', 'Updated');
    }

    /**
     * @param Request $request
     * @param $user
     * @return mixed
     */
    public function updateSeen(Request $request, $user)
    {
        $this->user_gestion->updateSeen($request->all(), $user->id);

        return response()->json();
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function destroy(User $user)
    {
        User::destroy($user->id);

        return redirect()->route('admin.user.index')->with('ok', 'Destroyed');
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        $roles = $this->role_gestion->all();

        return view('back.users.roles', compact('roles'));
    }

    /**
     * @param RoleRequest $request
     * @return mixed
     */
    public function postRoles(RoleRequest $request)
    {
        $this->role_gestion->update($request->except('_token'));

        return redirect('user/roles')->with('ok', 'Ok');
    }

    /**
     * @return mixed
     */
    public function weekBlogger()
    {
        $blogger = TopBlogger::first();

        if ($blogger != null)  {
            $blogger_id = $blogger->user_id;
            $top_blogger = User::find($blogger_id);
            $firstname = $top_blogger->firstname;
            $lastname = $top_blogger->lastname;
            $description = $blogger->description;
        }

        return view('back.users.week-blogger', compact('firstname', 'lastname', 'description'));
    }

    /**
     * @param Request $request
     */
    public function liveSearch(Request $request)
    {
        $search = $request->search;
        $search = addslashes($search);
        $search = htmlspecialchars($search);
        $search = stripslashes($search);
        if($search == ''){
            exit("Начните вводить запрос");
        }

        $result = [];

        if ($request->what == 'week-blogger')
        {
            $users = User::where('firstname', 'LIKE', "%$search%")->get();

            foreach ($users as $user)
            {
                $user = $user->firstname . ' ' . $user->lastname;
                array_push($result, $user);
            }
        } else {
            $posts = Post::where('title', 'LIKE', "%$search%")->get();
            foreach ($posts as $post)
            {
                $post = $post->title;
                array_push($result, $post);
            }

        }

        echo json_encode($result);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function newTopBlogger(Request $request)
    {
        $blogger = explode(' ', $request->search);
        $firstname = $blogger[0];
        $lastname = $blogger[1];
        $blogger_id = User::where('firstname', $firstname)->where('lastname', $lastname)->pluck('id');

        $top_blogger = TopBlogger::firstOrNew(array('id' => '1'));
        $top_blogger->user_id = $blogger_id;
        $top_blogger->description = $request->description;
        $top_blogger->save();

        return redirect()->back();
    }

}