<?php

/** Admin **/
Route::group([
    'prefix' => 'admin',
], function () {
    Route::get('dashboard', 'AdminController@admin')->name('admin.dashboard');

    Route::resource('advertisement', 'AdvertisementController');
    Route::resource('slider', 'SliderController');

    Route::resource('user', 'UserController');
    Route::put('admin/userseen/{user}', 'UserController@updateSeen');
    Route::get('week-blogger', 'UserController@weekBlogger')->name('admin.week_blogger');
    Route::post('/new-top-blogger', 'UserController@newTopBlogger')->name('admin.new_top_blogger');
    Route::post('/live-search', 'UserController@liveSearch')->name('admin.user_search');
    Route::get('/sticky-post', 'AdminController@stickyPost')->name('admin.sticky_post');
    Route::post('/new-sticky-post', 'AdminController@newStickyPost')->name('admin.new_sticky_post');

    Route::resource('product', 'ProductController', ['names' => [
        'index' => 'product.list',
        'create' => 'product.create',
        'store' => 'product.store',
        'edit' => 'product.edit',
        'update' => 'product.update',
        'destroy' => 'product.destroy'
    ]]);

    /** Shop (boutique) */

    Route::resource('shop/category', 'ProductCategoryController', ['names' => [
        'index' => 'product_category.index',
        'create' => 'product_category.create',
        'store' => 'product_category.store',
        'edit' => 'product_category.edit',
        'update' => 'product_category.update',
        'destroy' => 'product_category.destroy',
        'show' => 'product_category.show'
    ]]);

    Route::post('/vacancy-delete', [
        'uses' => 'ProfileController@deleteVacancy',
        'middleware' => 'employer'
    ]);

    Route::get('vacancy', [
        'uses' => 'VacancyController@indexOrder',
        'as' => 'admin.vacancy'
    ]);

    Route::put('vacancyactive/{id}', [
        'uses' => 'VacancyController@updateActive',
    ])->name('admin.vacancyactive');

    Route::put('vacancyseen/{id}', [
        'uses' => 'VacancyController@updateSeen'
    ])->name('admin.vacancyseen');

    Route::put('/translate/{id}', 'TranslationController@update')->name('admin.trans.update');
    Route::get('/translate/{id}', 'TranslationController@translate')->name('admin.trans.translate');
    Route::post('/translate/{id}', 'TranslationController@store')->name('admin.trans.store');
    Route::get('/translations/{id}', 'TranslationController@translations')->name('admin.trans.index');
    Route::get('/translate/{id}/edit', 'TranslationController@edit')->name('admin.trans.edit');

    Route::get('blog/order', 'BlogController@indexOrder')->name('admin.blog.order');
    Route::put('postseen/{id}', 'BlogController@updateSeen')->name('admin.postseen');
    Route::put('postactive/{id}', 'BlogController@updateActive')->name('admin.postactive');
    Route::put('postlanguage/{id}', 'BlogController@updateLanguage')->name('admin.postlanguage');
    Route::get('post/see/{category}/{id}', 'BlogController@see')->name('admin.post.see');
    Route::resource('blog', 'BlogController', ['except' => ['create', 'store']]);
});

Route::group(['prefix' => LaravelLocalization::setLocale(),'middleware' => [ 'localeSessionRedirect', 'localizationRedirect']], function() {
    // Profile

    Route::resource('profile', 'ProfileController');

    Route::group([
        'prefix' => 'profile',
    ], function () {
        Route::get('post/create', 'BlogController@create')->name('profile.post.create');
        Route::post('post/store', 'BlogController@store')->name('profile.post.store');
        Route::get('profile/vacancies/', 'ProfileController@myVacancies');
        Route::post('/post-delete', ['uses' => 'ProfileController@deletePost', 'middleware' => 'blogger'])->name('profile.post.delete');
        Route::get('/activate-post/{id}', ['uses' => 'ProfileController@activatePost', 'middleware' => 'blogger'])->name('profile.post.activate');
    });

// Home
    Route::get('/', [
        'uses' => 'HomeController@index',
        'as' => 'home'
    ]);


// Business
    Route::get('business/{param}/', 'PageController@show')->name('business.page');

// Blog
    Route::get('blog/tag', 'BlogController@tag');
    Route::get('blog/search', 'BlogController@search');
    Route::get('/articles/{category}/{title}', 'BlogController@show')->name('articles.post.show');


    Route::resource('/articles', 'CategoryController', ['prefix' => 'articles', 'names' => [
        'index' => 'category.index',
        'show' => 'category.show',
        'create' => 'category.create',
        'store' => 'category.store',
        'edit' => 'category.edit',
        'update' => 'category.update',
        'destroy' => 'category.destroy'
    ]]);

    Route::get('/articles/{name}/category/{subcategory}', 'CategoryController@showSubcategory')->name('subcategory.show');


// Fashion-job

    Route::post('fashion-job/search', 'VacancyController@search');
    Route::get('/fashion-job/confirmation', 'VacancyController@confirmation');
    Route::resource('company', 'CompanyController');
    Route::resource('fashion-job', 'VacancyController');

// Boutique

    Route::get('boutique/category/{hierarchy}', 'ProductCategoryController@show')->where('hierarchy', '(.*)?')->name('boutique');
    Route::get('boutique/{hierarchy}/{slug}', 'ProductController@show')->where('hierarchy', '(.*)?')->name('product.show');
    Route::post('/boutique/product/buy', ['uses' => 'ProductController@buyProduct', 'as' => 'product.buy']);
    Route::post('admin/product/images/{id}/delete', ['uses' => 'ProductController@deleteImage', 'as' => 'product.image.delete']);
    Route::post('admin/product/thumb/{id}/delete', ['uses' => 'ProductController@deleteThumb', 'as' => 'product.thumb.delete']);


// User
    Route::get('user/sort/{role}', 'UserController@indexSort');
    Route::get('user/roles', 'UserController@getRoles');
    Route::post('user/roles', 'UserController@postRoles');


// Auth
    Route::controllers([
        'auth' => 'Auth\AuthController',
        'password' => 'Auth\PasswordController'
    ]);

    Route::get('/email-confirmation', 'Auth\AuthController@confirmation');
    Route::get('/resend-email', 'Auth\AuthController@getResend');


// Social networks auth

    Route::get('/facebook/{role?}', ['uses' => 'SocializeController@loginWithFacebook']);
    Route::get('/vkontakte/{role?}', ['uses' => 'SocializeController@loginWithVkontakte']);
    Route::get('/google/{role?}', ['uses' => 'SocializeController@loginWithGoogle']);
    Route::get('/twitter/{role?}', ['uses' => 'SocializeController@loginWithTwitter']);


// Social networks share
    Route::get('/share-twitter/{category}/{slug}/{title}/', function ($category, $slug, $title) {
        $link = URL::to('/') . '/blog/' . $category . '/' . $slug . '/';
        $share = Share::load($link, $title)->twitter();
        return redirect($share);
    });

    Route::get('/share-facebook/{category}/{slug}/{title}/', function ($category, $slug, $title) {
        $link = URL::to('/') . '/blog/' . $category . '/' . $slug . '/';
        $share = Share::load($link, $title)->facebook();
        return redirect($share);
    });

    Route::get('/share-pinterest/{category}/{slug}/{title}/', function ($category, $slug, $title) {
        $link = URL::to('/') . '/blog/' . $category . '/' . $slug . '/';
        $share = Share::load($link, $title)->pinterest();
        return redirect($share);
    });

//Subscribe
    Route::post('subscribe', 'NewsletterController@subscribe');

});

// Ajax actions
Route::post('/like', [
    'uses' => 'BlogController@upLike',
    'middleware' => 'auth'
]);

Route::post('/load-posts', [
    'uses' => 'CategoryController@loadPosts'
]);

Route::post('/load-subcat', [
    'uses' => 'CategoryController@loadSubcategories'
]);

Route::post('/buy-service', [
    'uses' => 'BlogController@buyService'
]);

Route::post('/save-profile', [
    'uses' => 'ProfileController@store',
    'middleware' => 'auth'
]);

Route::post('/save-company', [
    'uses' => 'CompanyController@store',
    'middleware' => 'employer'
]);

Route::post('/delete-company', [
    'uses' => 'CompanyController@destroy',
    'middleware' => 'employer'
]);

Route::post('/profile-posts', [
    'uses' => 'ProfileController@loadPosts',
    'middleware' => 'blogger'
]);

Route::post('/profile-vacancies', [
    'uses' => 'ProfileController@loadVacancies',
    'middleware' => 'employer'
]);

Route::post('/load-vacancies', [
    'uses' => 'VacancyController@loadVacancies'
]);

Route::post('/filter-vacancies', [
    'uses' => 'VacancyController@filterVacancies'
]);

Route::post('/search-vacancies', [
    'uses' => 'VacancyController@searchVacancies'
]);

Route::post('/get-city', [
    'uses' => 'VacancyController@getCity'
]);

Route::post('send-cv', 'VacancyController@sendCV');

/*
|--------------------------------------------------------------------------
| Max Upload File Size filter
|--------------------------------------------------------------------------
|
| Check if a user uploaded a file larger than the max size limit.
| This filter is used when we also use a CSRF filter and don't want
| to get a TokenMismatchException due to $_POST and $_GET being cleared.
|
*/
Route::filter('maxUploadFileSize', function()
{
    // Check if upload has exceeded max size limit
    if (! (Request::isMethod('POST') or Request::isMethod('PUT'))) { return; }
    // Get the max upload size (in Mb, so convert it to bytes)
    $maxUploadSize = 1024 * 1024 * ini_get('post_max_size');
    $contentSize = 0;
    if (isset($_SERVER['HTTP_CONTENT_LENGTH']))
    {
        $contentSize = $_SERVER['HTTP_CONTENT_LENGTH'];
    }
    elseif (isset($_SERVER['CONTENT_LENGTH']))
    {
        $contentSize = $_SERVER['CONTENT_LENGTH'];
    }
    // If content exceeds max size, throw an exception
    if ($contentSize > $maxUploadSize)
    {
        throw new GSVnet\Core\Exceptions\MaxUploadSizeException;
    }
});