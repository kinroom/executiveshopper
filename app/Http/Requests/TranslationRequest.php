<?php namespace App\Http\Requests;

class TranslationRequest extends Request {


    public function rules() {
        return [
            'title' => 'required|max:255',
            'content' => 'required|max:65000',
            'language' => 'required'
        ];
    }

}