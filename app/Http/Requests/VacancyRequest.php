<?php namespace App\Http\Requests;


class VacancyRequest extends Request {


    public function rules() {
        return [
            'title' => 'required',
            'company_id' => 'required',
            'category_id' => 'required',
            'industry_id' => 'required',
            'availability_id' => 'required',
            'description' => 'required'
        ];
    }

}