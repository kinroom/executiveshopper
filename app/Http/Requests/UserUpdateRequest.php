<?php namespace App\Http\Requests;

class UserUpdateRequest extends Request {

	public function rules() {
		$id = $this->user->id;
		return $rules = [
			'firstname' => 'required|max:30|alpha',
            'lastname' => 'required|max:30|alpha',
            'email' => 'required|email|unique:users,email,' . $id
		];
	}

}
