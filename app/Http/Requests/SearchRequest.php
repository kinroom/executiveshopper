<?php namespace App\Http\Requests;

class SearchRequest extends Request {


	public function rules() {
		return [
			'search' => 'required|max:100',
		];
	}

}
