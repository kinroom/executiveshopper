<?php namespace App\Http\Requests;

class UserCreateRequest extends Request {

	public function rules() {
		return [
			'firstname' => 'required|max:30|alpha|unique:users',
            'lastname' => 'required|max:30|alpha|unique:users',
			'email' => 'required|email|unique:users',
			'password' => 'required|min:8'
		];
	}

}