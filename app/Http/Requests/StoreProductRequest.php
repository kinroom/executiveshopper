<?php namespace App\Http\Requests;

class StoreProductRequest extends Request {
    
    public function rules() {
        $rules = [
            'name' => 'required|max:255|unique:products,name, ' . $this->route()->parameter('product'),
            'category' => 'required',
            'price' => 'required|numeric|between:0,1000000',
            'sizes' => 'required'
        ];
        
        return $rules;
    }

    public function messages()
    {
        return [
            'name.unique' => 'This value already exists and must be unique: name!',
            'name.required' => 'This is required field: name.',
            'name.max' => 'This field can contain maximum 255 characters: name.',
            'category.required' => 'This is required field: category',
            'price.between' => 'This number must have value from 0 to 1 000 000: price.',
            'price.numeric' => 'This field must be a number type: price.',
            'price.required' => 'This is required field: price.',
            'sizes.required' => 'This is required field: sizes.',
        ];
    }

}