<?php namespace App\Http\Requests;

class AdvertisementRequest extends Request {


    public function rules() {

        return [
            'place' => 'required',
            'code' => 'required'
        ];
    }

}