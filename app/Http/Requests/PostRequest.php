<?php namespace App\Http\Requests;

class PostRequest extends Request {


	public function rules() {
		$id = $this->blog ? ',' . $this->blog : '';

		return [
			'title' => 'required|max:255',
			'content' => 'required|max:65000',
            'category_id' => 'required',
		];
	}

}