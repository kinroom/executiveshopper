<?php namespace App\Http\Requests;

class RoleRequest extends Request {


	public function rules() {
		return [
			'admin' => 'required|alpha|max:50',
			'redac' => 'required|alpha|max:50',
			'user'  => 'required|alpha|max:50'
		];
	}

}