<?php namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class RegisterRequest extends Request {

	public function rules()
	{
		return [
			'firstname' => 'required|max:30|alpha',
            'lastname' => 'required|max:30|alpha',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|min:8',
			'role' => 'required'
		];
	}

}
