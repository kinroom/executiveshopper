<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use Request;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Mail\Mailer;

class SendMail extends Job implements SelfHandling
{

    protected $user;

    /**
     * SendMail constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        
        $data = [
            'title'  => trans('front/verify/email.email-title'),
            'intro'  => trans('front/verify/email.email-intro'),
            'link'   => trans('front/verify/email.email-link'),
            'email' => $user->email,
            'confirmation_code' => $this->user->confirmation_code
        ];

        Mail::send('emails.auth.verify', $data, function($message) {
            $message->to($this->user->email, $this->user->firstname . ' ' . $this->user->lastname)
                ->subject(trans('front/verify/email.email-title'));
        });

    }

    /**
     * @param $request
     */
    public static function buyService($request)
    {

        if ($request->phone == '') {
            $request->phone = 'none';
        }

        $data = [
            'title' => $request->name . ' want to buy service from your site!',
            'post' => $request->post,
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone
        ];

        Mail::send('emails.service', $data, function ($message) use ($request) {
            $message->to('whereismy@mail.ru', 'Olga Burbelo')->subject($request->name . ' bought some service!');
        });
    }

    /**
     * @param $product
     * @param $user
     */
    public static function buyProduct($product, $user)
    {
        $data = [
            'product' => $product,
            'user' => $user
        ];

       Mail::send('emails.shop.product_buy', $data, function ($message) use ($user) {
            $message->to('whereismy@mail.ru', 'Burbell - shop')->subject($user['name'] . ' bought product in burbell shop!');
       });

    }

    /**
     * @param $request
     */
    public static function sendCV($request)
    {
        $data = [
            'title'  => 'Resume from your site!',
            'vacancy' => $request->vacancy,
            'company' => $request->company
            ];

        Mail::send('emails.resume', $data, function($message) use ($request) {
            $message->to('whereismy@mail.ru', $request->company)->subject('Resume from your site!');
            $message->attach($request->file('loadCV')->getRealPath(), array(
                    'as' => 'resume.' . $request->file('loadCV')->getClientOriginalExtension(),
                    'mime' => $request->file('loadCV')->getMimeType())
            );
        });

    }

    /**
     * @param Mailer $mailer
     */
    public function handle(Mailer $mailer) {
        $data = [
            'title'  => trans('front/verify.email-title'),
            'intro'  => trans('front/verify.email-intro'),
            'link'   => trans('front/verify.email-link'),
            'confirmation_code' => $this->user->confirmation_code
        ];

       /* $mailer->send('emails.auth.verify', $data, function($message) {
            $message->to($this->user->email, $this->user->username)
                    ->subject(trans('front/verify.email-title'));
        });*/
    }
}
