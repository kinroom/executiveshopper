<?php namespace App\Repositories;

use App\Models\Language;
use App\Models\Post, App\Models\Tag, App\Models\Like, App\Models\Category, App\Models\User, App\Models\PostTag;

class BlogRepository extends BaseRepository{

    /**
     * @var Tag
     */
    protected $tag;


    /**
     * BlogRepository constructor.
     * @param Post $post
     * @param Tag $tag
     */
    public function __construct(
        Post $post,
        Tag $tag)
    {
        $this->model = $post;
        $this->tag = $tag;
    }

    /**
     * @param $post
     * @param $inputs
     * @param $user_id
     * @return mixed
     */
    private function savePost($post, $inputs, $user_id) {

        $post->title = $inputs['title'];
        $post->slug = str_slug($inputs['title'], "-");
        $post->category_id = $inputs['category_id'];
        if ($inputs['category_id'] !== 'Video') {
            $post->subcategory = $inputs['subcategory'];
        } else { $post->subcategory = 'Video'; }
        if (isset($inputs['thumb_img'])) {
            $post->thumb = $inputs['thumb_img'];
        }
        $post->video = $inputs['post-videolink'];
        $post->content = preg_replace('~style="[^"]*"~i', '', $inputs['content']);
        $post->photographer = $inputs['photographer'];
        $post->styling = $inputs['styling'];
        $post->outfit = $inputs['outfit'];
        $post->location = $inputs['location'];
        $post->website = $inputs['website'];
        $post->active = isset($inputs['active']);
        $post->original_id = 0;
        if (isset($inputs['is_commercial']) &&  $inputs['is_commercial'] == 1) {
            $post->is_commercial = true;
        }
        if($user_id) $post->user_id = $user_id;

        $post->save();

        return $post;
    }

    /**
     * @return mixed
     */
    private function queryActiveWithUserOrderByDate() {
        return $this->model
            ->select('id', 'created_at', 'updated_at', 'title', 'slug', 'user_id', 'category_id', 'user_id', 'thumb', 'views', 'likes')
            ->whereActive(true)
            ->with('user')
            ->orderBy('id', 'DESC');
    }


    /**
     * @param $n
     * @param $id
     * @return mixed
     */
    public function indexTag($n, $id)
    {
        $query = $this->queryActiveWithUserOrderByDate();

        return $query->whereHas('tags', function($q) use($id) { $q->where('tags.id', $id); })
            ->paginate($n);
    }

    public function countPostsWithTag($id) {

        $query = $this->queryActiveWithUserOrderByDate();

        return $query->whereHas('tags', function($q) use($id) { $q->where('tags.id', $id); })
            ->count();
    }

    /**
     * @param $n
     * @param $search
     * @return mixed
     */
    public function search($n, $search)
    {
        $query = $this->queryActiveWithUserOrderByDate();

        return $query->where(function($q) use ($search) {
            $q->where('title', 'like', "%$search%");
        })->paginate($n);
    }

    /**
     * @param $search
     * @return mixed
     */
    public function countPostsBySearch($search) {
        $query = $this->queryActiveWithUserOrderByDate();

        return $query->where(function($q) use ($search) {
            $q->where('title', 'like', "%$search%");
        })->count();
    }

    /**
     * @param $n
     * @param null $user_id
     * @param string $orderby
     * @param string $direction
     * @return mixed
     */
    public function index($n, $user_id = null, $orderby = 'id', $direction = 'desc')
    {
        $query = $this->model
            ->select('posts.id', 'posts.created_at', 'title', 'category_id', 'posts.seen', 'active', 'user_id', 'slug', 'firstname', 'lastname', 'language_id', 'name')
            ->join('users', 'users.id', '=', 'posts.user_id')
            ->join('categories', 'categories.id', '=', 'posts.category_id')
            ->orderBy($orderby, 'desc')
            ->where('original_id', 0);

        if($user_id)
        {
            $query->where('user_id', $user_id);
        }

        return $query->paginate($n);
    }

    /**
     * Get post collection.
     *
     * @param  string  $slug
     * @return array
     */
    public function show($category, $slug)
    {
        $post = $this->model->with('user', 'tags')->whereSlug($slug)->where('active', 1)->firstOrFail();

        $get_category_id = Category::where('name', $category)->pluck('id');
        
        if ($get_category_id != $post->category_id) {
            return 'fail';
        }

        $post->views = $post->views + 1;
        $post->save();
        $views = $post->views;
        $likes = Like::where('post_id', $post->id)->count();

        if ($post->original_id !== 0) {
            $translator = User::where('id', $post->translator_id)->first();
        }
        
        $previous_post = Post::where('id', '<', $post->id)->where('active', 1)->where('language_id', $post->language_id)->orderBy('id', 'desc')->take(1)->get();
        $next_post = Post::where('id', '>', $post->id)->where('active', 1)->where('language_id', $post->language_id)->orderBy('id', 'desc')->take(1)->get();

        return compact('post', 'views', 'likes', 'previous_post', 'next_post', 'translator');
    }

    /**
     * @param $post
     * @return mixed
     */
    public function edit($post)
    {
        $tags = [];

        foreach($post->tags as $tag) {
            array_push($tags, $tag->tag);
        }

        return compact('post', 'tags');
    }



    /**
     * Get post collection.
     *
     * @param  int  $id
     * @return array
     */
    public function GetByIdWithTags($id)
    {
        return $this->model->with('tags')->findOrFail($id);
    }

    /**
     * @param $inputs
     * @param $post
     * @param $user
     */
    public function update($inputs, $post, $user) {
        $post = $this->savePost($post, $inputs, $user->id);
        // Tag gestion
        $tags_id = [];
        if(array_key_exists('tags',  $inputs) && $inputs['tags'] != '') {

            $tags = explode(',', $inputs['tags']);

            foreach ($tags as $tag) {
                $tag_ref = $this->tag->whereTag($tag)->first();
                if(is_null($tag_ref)) {
                    $tag_ref = new $this->tag();
                    $tag_ref->tag = $tag;
                    $tag_ref->save();
                }
                array_push($tags_id, $tag_ref->id);
            }
        }

    }

    /**
     * Update "seen" in post.
     *
     * @param  array  $inputs
     * @param  int    $id
     * @return void
     */
    public function updateLanguage($inputs, $id)
    {
        $post = $this->getById($id);

        $language = Language::find($inputs['language_id']);

        $post->language_id =  $language->id;

        $post->save();
    }


    /**
     * Update "seen" in post.
     *
     * @param  array  $inputs
     * @param  int    $id
     * @return void
     */
    public function updateSeen($inputs, $id)
    {
        $post = $this->getById($id);

        $post->seen = $inputs['seen'] == 'true';

        $post->save();
    }

    /**
     * Update "active" in post.
     *
     * @param  array  $inputs
     * @param  int    $id
     * @return void
     */
    public function updateActive($inputs, $id)
    {
        $post = $this->getById($id);

        $post->active = $inputs['active'] == 'true';

        $post->save();
    }

    /**
     * Create a post.
     *
     * @param  array  $inputs
     * @param  int    $user_id
     * @return void
     */
    public function store($inputs, $user_id)
    {
        $post = new $this->model;
        $post = $this->savePost($post, $inputs, $user_id);

        // Tags gestion
        if(array_key_exists('tags',  $inputs) && $inputs['tags'] != '') {

            $tags = explode(',', $inputs['tags']);

            foreach ($tags as $tag) {
                $tag_ref = $this->tag->whereTag(trim($tag))->first();
                if(is_null($tag_ref)) {
                    $tag_ref = new $this->tag();
                    $tag_ref->tag = $tag;
                    $post->tags()->save($tag_ref);
                } else {
                    $post->tags()->attach($tag_ref->id);
                }
            }
        }
    }

    /**
     * @param int $post
     */
    public function destroy($post)
    {
        $post->tags()->detach();

        $post->delete();
    }

    /**
     * Get post slug.
     *
     * @param  int  $comment_id
     * @return string
     */
    public function getSlug($comment_id)
    {
        return $this->comment->findOrFail($comment_id)->post->slug;
    }

    /**
     * Get tag name by id.
     *
     * @param  int  $tag_id
     * @return string
     */
    public function getTagById($tag_id)
    {
        return $this->tag->findOrFail($tag_id)->tag;
    }


}