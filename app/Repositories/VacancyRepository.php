<?php

namespace App\Repositories;

use App\Models\Vacancy;

class VacancyRepository extends BaseRepository
{
    protected $model;

    public function __construct(Vacancy $vacancy)
    {
        $this->model = $vacancy;
    }

    /**
     * @param $n
     * @return mixed
     */
    public function index($n)
    {
        $query = $this->model
            ->orderBy('id', 'desc');

        return $query->paginate($n);
    }

    /**
     * Update "seen" in vacancy.
     *
     * @param  array  $inputs
     * @param  int    $id
     * @return void
     */
    public function updateSeen($inputs, $id)
    {
        $vacancy = $this->getById($id);

        $vacancy->seen = $inputs['seen'] == 'true';

        $vacancy->save();
    }


    /**
     * Update "active" in vacancy.
     *
     * @param  array  $inputs
     * @param  int    $id
     * @return void
     */
    public function updateActive($inputs, $id)
    {
        $vacancy = $this->getById($id);

        $vacancy->is_active = $inputs['active'] == 'true';

        $vacancy->save();
    }
}
