<?php

namespace App\Repositories;

use App\Models\Product;
use App\Models\ProductImage;

class ProductRepository extends BaseRepository
{
    protected $model;

    /**
     * ProductRepository constructor.
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->model = $product;
    }

    /**
     * @param $request
     * @return Product
     */
    public function createProduct($request)
    {
        $product = new Product();
        $product->fill($request->request->all());

        $categories = $request->request->get('category');
        $colours = implode(',', array_diff($request->request->get('colours'), ['']));

        $product->colours = $colours;
        $product->sku = mb_strimwidth(hexdec(uniqid()), 3, 10);
        $product->slug = str_slug($request->request->get('name'), '-');

        if ($request->hasFile('thumb')) $product->thumb = $request->file('thumb')->getClientOriginalName();

        $product->save();

        $product->categories()->sync($categories);

        return $product;
    }

    /**
     * @param $product_id
     * @param $imageName
     */
    public function createProductImages($product_id, $imageName)
    {
        $productImage = new ProductImage();

        $productImage->product_id = $product_id;
        $productImage->name = $imageName;

        $productImage->save();
    }

    /**
     * @param $request
     * @param $id
     * @return \App\Models\Product
     */
    public function updateProduct($request, $id)
    {
        $product = $this->getById($id);
        $product->fill($request->request->all());

        $product->categories()->sync($request->request->get('category'));
        $product->colours = implode(',', array_diff($request->request->get('colours'), ['']));
        $product->slug = str_slug($request->request->get('name'), '-');

        if ($request->hasFile('thumb')) $product->thumb = $request->file('thumb')->getClientOriginalName();

        $product->save();

        return $product;
    }

}
