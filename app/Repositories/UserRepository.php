<?php

namespace App\Repositories;

use App\Models\User, App\Models\Role, App\Models\Like;


class UserRepository extends BaseRepository
{

    /**
     * @var Role| Role
     */
	protected $role;

    /**
     * UserRepository constructor.
     * @param User $user
     * @param Role $role
     */
	public function __construct(
		User $user,
		Role $role)
	{
		$this->model = $user;
		$this->role = $role;
	}


    /**
     * @param $user
     * @param $inputs
     */
  	private function save($user, $inputs)
	{
		if(isset($inputs['seen'])) {
			$user->seen = $inputs['seen'] == 'true';
		} else {
            $role_id = Role::where('slug', $inputs['role'])->pluck('id');

			$user->firstname = $inputs['firstname'];
            $user->lastname = $inputs['lastname'];
			$user->email = $inputs['email'];
            $user->contact_email = $inputs['email'];
            $user->role_id = $role_id;

			}
		$user->save();
	}


    /**
     * @param $n
     * @param $role
     * @return mixed
     */
	public function index($n, $role)
	{
		if($role != 'total')
		{
			return $this->model
			->with('role')
			->whereHas('role', function($q) use($role) {
				$q->whereSlug($role);
			})
			->oldest('seen')
			->latest()
			->paginate($n);
		}

		return $this->model
		->with('role')
		->oldest('seen')
		->latest()
		->paginate($n);
	}


	/**
	 * Count the users.
	 * Count the users.
	 *
	 * @param  string  $role
	 * @return int
	 */
	public function count($role = null)
	{
		if($role)
		{
			return $this->model
			->whereHas('role', function($q) use($role) {
				$q->whereSlug($role);
			})->count();
		}

		return $this->model->count();
	}


    /**
     * @return array
     */
	public function counts()
	{
		$counts = [
			'admin' => $this->count('admin'),
			'redac' => $this->count('redac'),
			'user' => $this->count('user')
		];

		$counts['total'] = array_sum($counts);

		return $counts;
	}


    /**
     * @param $inputs
     * @param null $confirmation_code
     * @return mixed
     */
	public function store($inputs, $confirmation_code = null)
	{
		$user = new $this->model;

		$user->password = bcrypt($inputs['password']);

		if($confirmation_code) {
			$user->confirmation_code = $confirmation_code;
		} else {
			$user->confirmed = true;
		}

        $user->service = 'site';
        
		$this->save($user, $inputs);

		return $user;
	}


    /**
     * @param $inputs
     * @param $user
     */
	public function update($inputs, $user)
	{
		$user->confirmed = isset($inputs['confirmed']);

        $inputs['role'] = Role::find($inputs['role'])->slug;

		$this->save($user, $inputs);
	}


	/**
	 * Get statut of authenticated user.
	 *
	 * @return string
	 */
	public function getStatut()
	{
		return session('statut');
	}


	/**
	 * Valid user.
	 *
     * @param  bool  $valid
     * @param  int   $id
	 * @return void
	 */
	public function valid($valid, $id)
	{
		$user = $this->getById($id);

		$user->valid = $valid == 'true';

		$user->save();
	}


    /**
     * @param User $user
     * @throws \Exception
     */
	public function destroyUser(User $user)
	{
		$user->comments()->delete();

		$user->delete();
	}

	/**
	 * Update "seen" in post.
	 *
	 * @param  array  $inputs
	 * @param  int    $id
	 * @return void
	 */
	public function updateSeen($inputs, $id)
	{
		$user = $this->getById($id);

		$user->seen = $inputs['seen'] == 'true';

		$user->save();
	}


    /**
     * @param $confirmation_code
     */
	public function confirm($confirmation_code)
	{
		$user = $this->model->whereConfirmationCode($confirmation_code)->firstOrFail();

		$user->confirmed = true;
		$user->confirmation_code = null;
		$user->save();

        return $user;
	}

    public function upLike($user_id, $post_id)
    {
        $like = new Like();
        $like->user_id = $user_id;
        $like->post_id = $post_id;
        $like->save();
    }

}
