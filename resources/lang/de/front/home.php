<?php

return [
    'daily-dose' => 'Ihre tägliche Dosis von Nachrichten',
    'by' => 'durch',
    'all-categories' => 'Alle Kategorien',
    'latest-from-categories' => 'Neueste von Kategorien',
    'explore-the-latest-news' => 'Entdecken Sie die neuesten Nachrichten',
    'most-viewed-posts' => 'Die meisten Beiträge',
    'know-what-people-prefer' => 'Wissen, was die Menschen bevorzugen',
    'latest-videos' => 'Die neuesten Videos',
    'your-dose-of-visual-inspiration' => 'Ihre Dosis der visuellen Inspiration',
    'follow-us-on-instagram' => 'Folgen Sie uns auf Instagram',
    'follow-us' => 'Folge uns',
    'in-social-networks' => 'In sozialen Netzwerken',
    'blogger-of-week' => 'Blogger der Woche',
    'special-news' => 'Spezielle Neuigkeiten',
    'be-unique' => 'Seien Sie einzigartig in immer nur spezielle Nachrichten',
    'share-post' => 'Teile diesen Beitrag',
    'continue-reading' => 'Weiterlesen'

];