<?php

return [
    'fashion-job' => 'Arbeiten Job',
    'results' => 'ergebnisse',
    'current-vacancies' => 'Aktuelle Stellenangebote',
    'search-for-vacancies' => 'Suche nach freien Stellen',
    'add-vacancy' => 'Stellenangebot hinzufügen',
    'filter' => 'Filter',
    'industry' => 'Industrie',
    'category' => 'Kategorie',
    'availability' => 'Verfügbarkeit',
    'location' => 'Ort',
    'country' => 'Land',
    'city' => 'Stadt',
    'selected-options' => 'Ausgewählte Optionen',
    'clear-filter' => 'Filter löschen',
    'salary' => 'Gehalt',
    'click-to-see-more' => 'Klicken Sie, um weitere Ergebnisse zu sehen',
    'freelance' => 'Freiberuflich',
    'internship' => 'Berufspraktikum',
    'all' => 'Alle'
];