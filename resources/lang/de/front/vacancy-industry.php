<?php

return [
    'fashion' => 'Mode',
    'beauty' => 'Schönheit & Kosmetik',
    'advertising' => 'Werbung',
    'apparel' => 'Kleidung & Accessoires',
    'communications' => 'Mitteilungen',
    'consulting' => 'Unternehmensberatung',
    'retail' => 'Verkauf',
    'manufacturing' => 'Herstellung',
    'health' => 'Gesundheit'
];