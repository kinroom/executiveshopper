<?php

return [
    'login' => 'Anmeldung',
    'email' => 'Email',
    'register' => 'Neu registrieren',
    'with-social-networks' => 'Mit sozialen Netzwerken',
    'password' => 'Passwort',
    'what-name' => 'Wie heißen Sie?',
    'register-with-social' => 'Registrieren Sie sich mit sozialen Netzwerken',
    'please-fill' => 'Bitte füllen Sie das Feld aus, bevor Sie fortfahren',
    'continue' => 'Fortsetzen',
    'or-enter' => 'Oder drücken Sie die Eingabetaste',
    'what-lastname' => 'Was ist dein Nachname?',
    'what-email' => 'Wie lautet deine E-Mail Adresse?',
    'we-dont-spam' => 'Wir senden keine Spam, versprechen wir ...',
    'please-fill-valid-email' => 'Bitte füllen Sie eine gültige E-Mail-Adresse aus',
    'enter-password' => 'Geben Sie Ihr Passwort ein',
    'review-submit' => 'Überprüfen & abschicken',
    'this-will-help-us' => 'Dies hilft uns wissen, welche Art von Service Sie benötigen',
    'welcome' => 'Herzlich willkommen',
    'first-choose-status' => 'Wählen Sie zunächst Ihren Status',
    'blogger' => 'Blogger',
    'employer' => 'Arbeitgeber',
    'reader' => 'Leser'
 ];