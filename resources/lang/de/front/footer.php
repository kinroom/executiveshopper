<?php

return [
    'about-company' => 'Über das Unternehmen',
    'how-it-works' => 'Wie es funktioniert',
    'advertisement' => 'Werbung',
    'ask-me-anything' => 'Frag mich was',
    'email-subscribe' => 'Geben Sie Ihre E-Mail für wöchentliche Updates ein',
    'enter-your-email' => 'Gib deine E-Mail hier ein',
    'subscribe' => 'Abonnieren'
];