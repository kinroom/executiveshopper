<?php

return [
    'retail' => 'Einzelhandel Management & in-Store',
    'marketing' => 'Marketing & eCommerce',
    'design' => 'Design & Gestaltung',
    'product' => 'Produkt- & Lieferkette',
    'administration' => 'Verwaltung & IT'
];