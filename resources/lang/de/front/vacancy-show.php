<?php

return [
    'get-back' => 'Komm zurück',
    'add-vacancy' => 'Stellenangebot hinzufügen',
    'load' => 'Belastung',
    'send' => 'Senden',
    'get-contacts' => 'Kontakte erhalten',
    'freelance' => 'Freiberuflich',
    'internship' => 'Berufspraktikum'
];