<?php

return [
    'error-404' => '404',
    'info' => 'Seite nicht gefunden',
    'button' => 'Hauptseite'
];