<?php

return [
	'news' => 'Nachrichten',
    'read-article' => 'Artikel lesen',
    'show-more' => 'Zeig mehr',
	'articles' => 'artikeln',
	'filter_by' => 'filtern nach',
	'all' => 'alle'
];