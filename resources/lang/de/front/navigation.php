<?php

return [
    'search' => 'Suche...',
    'personal-profile' => 'Persönliches Profil',
    'menu' => 'Menü',
    'language' => 'Sprache',
    'blog' => 'Blog',
    'business' => 'Geschäft',
    'shopping' => 'Persönliche Shopping',
    'promotion' => 'Beförderung',
    'about' => 'Über uns',
    'work' => 'Arbeite mit uns',
    'boutique' => 'Boutique',
    'fashion-job' => 'Mode-Job'
];