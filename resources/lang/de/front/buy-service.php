<?php

return [
    'send-request' => 'Anfrage senden',
    'description' => 'Geben Sie Ihren Namen, E-Mail und Telefon ein, damit wir Sie kontaktieren können',
    'name' => 'NAME',
    'email' => 'EMAIL',
    'phone' => 'TELEFON',
    'are-you-sure' => 'Möchten Sie diesen Service wirklich kaufen?',
    'your-data' => 'Ihre Telefonnummer und E-Mail werden an uns gesendet, damit wir Sie kontaktieren können',
    'yes' => 'ja',
    'no' => 'Nein'
];