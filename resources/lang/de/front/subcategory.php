<?php

return [
    'trend' => 'Trend',
    'look' => 'Schau',
    'promotion' => 'Beförderung',
    'travel' => 'Reise',
    'hotel' => 'Hotel',
    'sport' => 'Sport',
    'food' => 'Essen',
    'car' => 'Auto',
    'home' => 'Wohndesign',
    'makeup' => 'Bilden',
    'hair' => 'Haar',
    'body' => 'Körper'
];