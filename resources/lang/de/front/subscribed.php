<?php

return [
    'successful-subscribe' => 'Sie haben erfolgreich unseren Newsletter abonniert',
    'thank-you' => 'Vielen Dank für Ihr Abonnement',
    'will-sell' => 'Der Newsletter wird an Ihre E-Mail-Adresse gesendet.'
];