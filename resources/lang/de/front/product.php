<?php

return [
    'color' => 'Farbe',
    'size' => 'Größe',
    'buy' => 'Der Tasche hinzufügen',
    'video' => 'Video',
    'size_fit' => 'Größe & Passen',
    'details' => 'Details',
    'material_care' => 'Material & Pflege',
    'delivery' => 'Gratisversand',
    'are-you-sure' => 'Möchten Sie dieses Produkt wirklich kaufen?'
];