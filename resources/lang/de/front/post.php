<?php

return [
    'share' => 'Teile diesen Beitrag',
    'by' => 'durch',
    'translator' => 'Übersetzer',
    'photographer' => 'Fotograf',
    'styling-makeup' => 'Styling / Schminke',
    'outfit' => 'Outfit',
    'location' => 'Ort',
    'website' => 'Webseite',
    'also-buy' => 'Auch können Sie diesen Service kaufen',
    'just-click' => 'Klicken Sie einfach auf die Schaltfläche und wissen Sie mehr',
    'about-how' => 'Wie Sie das tun können',
    'buy-service' => 'Kaufen Service',
    'previous-news' => 'Vorherige Nachrichten',
    'next-news' => 'Nächste Nachricht',
    'more-articles' => 'Weitere Artikel',
    'latest' => 'Neueste',
    'most-viewed' => 'Am häufigsten gesehen',
    'read-article' => 'Artikel lesen'
];