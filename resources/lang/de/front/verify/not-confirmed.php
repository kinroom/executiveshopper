<?php

return [
    'not-confirmed' => 'Sie müssen Ihre E-Mail bestätigen',
    'confirmed' => 'Ihr Konto wurde nicht bestätigt',
    'please-confirm' => 'Ihr Konto wurde nicht bestätigt. Bitte bestätigen Sie Ihre E-Mail mit dem Link, den wir Ihnen geschickt haben!',
    'didnt-get-link' => 'Sie haben keinen Link zur Bestätigung bekommen? Wir können es wieder senden.',
    'send-again' => 'Link erneut senden.'
];