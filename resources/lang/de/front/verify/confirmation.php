<?php

    return [
        'confirm-email' => 'Bestätige deine E-Mail',
        'thank-you' => 'Vielen Dank für Ihre Anmeldung',
        'to-complete' => 'Vielen Dank für die Registrierung Ihres Blogs. Um die Registrierung abzuschließen, müssen Sie bestätigen, dass Sie die E-Mail besitzen, mit der das Konto konfiguriert wird.',
        'confirm-your-email' => 'Bestätigen Sie Ihre E-Mail und öffnen Sie einen Link aus der Nachricht, die wir Ihnen zum Zeitpunkt der Registrierung zugesandt haben.',
        'confirmation-helps' => 'Bestätigungs-E-Mail hilft uns sicherzustellen, dass wir Ihre Kontoinformationen an den richtigen Ort senden.'
    ];