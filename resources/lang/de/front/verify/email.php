<?php

    return [
        'email-title' => 'Registrierungsbestätigung - Olga Burbelo',
        'email-intro' => 'Um Ihre Anmeldung zu bestätigen, bitte ',
        'email-link' => 'klick auf den Link'
    ];
