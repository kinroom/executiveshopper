<?php
    
    return [
        'success' => 'Erfolgreiche Bestätigung',
        'confirmed' => 'Ihr Konto wurde erfolgreich bestätigt',
        'dear' => 'Sehr geehrter',
        'you-can-login' => 'Ihr Konto wurde erfolgreich bestätigt und Sie können sich jetzt einloggen!'
    ];