<?php

return [
    'all' => 'Alle',
    'fashion' => 'Mode',
    'lifestyle' => 'Lebensstil',
    'beauty' => 'Schönheit',
    'video' => 'Video',
    'tag' => 'Tag',
    'search' => 'Suche',
    'articles' => 'artikeln'
];