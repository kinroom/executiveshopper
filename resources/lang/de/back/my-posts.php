<?php

return [
    'my-posts' => 'Meine Beiträge',
    'active-posts' => 'Aktive Beiträge',
    'approving-posts' => 'Genehmigung von Stellen',
    'add-post' => 'Beitrag melden',
    'show-more' => 'Zeig mehr',
    'want-delete' => 'Wollen Sie diesen Beitrag wirklich löschen?',
    'yes' => 'Ja',
    'no' => 'Nein',
    'have-no-posts' => 'Sie haben noch nichts gepostet'
];