<?php

return [
    'create-vacancy' => 'Stellen Sie eine freie Stelle',
    'accept-rules' => 'Ich akzeptiere die Regeln',
    'choose-company' => 'Wählen Sie die Firma, von der aus Sie diese Vakanz veröffentlichen möchten',
    'vacancy-name' => 'Name der freien Stelle',
    'choose-industry' => 'Wählen Sie die Branche',
    'choose-category' => 'Wählen Sie die Kategorie',
    'choose-availability' => 'Wählen Sie die Verfügbarkeit',
    'choose-country' => 'Land',
    'choose-city' => 'Stadt',
    'choose-salary' => 'Gehalt',
    'write-description' => 'Schreiben Sie über Ihre Vakanz',
    'publish-vacancy' => 'Veröffentlichen Sie diese Vakanz',
    'freelance' => 'Freiberuflich',
    'internship' => 'Berufspraktikum'
];