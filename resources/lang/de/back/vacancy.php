<?php
return [
    'vacancy-creating' => 'Stellen Sie eine freie Stelle',
    'accept-rules' => 'Ich akzeptiere die Regeln',
    'choose-company' => 'Wählen Sie die Firma, von der aus Sie diese Vakanz veröffentlichen möchten',
    'name-vacancy' => 'Name der freien Stelle',
    'choose-industry' => 'Wählen Sie die Branche',
    'choose-category' => 'Wählen Sie die Kategorie',
    'choose-availability' => 'Wählen Sie die Verfügbarkeit',
    'freelance' => 'Freiberuflich',
    'internship' => 'Berufspraktikum',
    'country' => 'Land',
    'city' => 'Stadt',
    'salary' => 'Gehalt',
    'write-about' => 'Schreiben Sie über Ihre Vakanz',
    'publish-vacancy' => 'Veröffentlichen Sie diese Vakanz',
    'confirmation' => 'Stellenausschreibung',
    'my-vacancies' => 'Meine offenen Stellen',
    'confirmation-message' => 'Ihre Vakanz wurde zur Überprüfung an den Administrator gesendet. Nach der Bestätigung, wird Ihre Vakanz auf der Website.'
];