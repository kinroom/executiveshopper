<?php

return [
    'propose-post' => 'Einen Beitrag vorschlagen',
    'accept-rules' => 'Ich akzeptiere die Regeln',
    'are-commercial' => 'Sind Postwerbung?',
    'post-name' => 'Name der Stelle',
    'choose-category' => 'Wählen Sie die Kategorie',
    'add-cover' => 'Deckel hinzufügen',
    'video-url' => 'Video-URL',
    'recommended-dimensions' => 'Empfohlene Abmessungen',
    'size-less' => 'Größe Gewicht kleiner als',
    'write-post' => 'Schreiben Sie Ihren Beitrag',
    'tags' => 'Tags (getrennt durch Kommas)',
    'author-translator' => 'Autor / Übersetzer',
    'photographer' => 'Fotograf',
    'styling-makeup' => 'Styling / Schminke',
    'outfit' => 'Outfit',
    'location' => 'Ort',
    'website' => 'Webseite',
    'send-post' => 'Post senden'
];