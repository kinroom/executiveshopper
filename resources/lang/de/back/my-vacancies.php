<?php

return [
    'my-vacancies' => 'Meine offenen Stellen',
    'first-create-company' => 'Bevor Sie eine Vakanz erstellen, müssen Sie Ihr Unternehmen in Ihr persönliches Profil einfügen',
    'add-company' => 'Fügen Sie eine Firma hinzu',
    'you-havent-posted' => 'Sie haben keine Vakanzen veröffentlicht',
    'delete-vacancy' => 'Möchten Sie diese Vakanz wirklich löschen?',
    'add-vacancy' => 'Stellenangebot hinzufügen',
    'show-more' => 'Zeig mehr',
    'availability' => 'Verfügbarkeit',
    'salary' => 'Gehalt',
    'location' => 'Ort',
    
];