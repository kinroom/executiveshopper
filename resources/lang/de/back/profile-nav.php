<?php

return [
    'profile-settings' => 'Profileinstellungen',
    'post-propose' => 'Einen Beitrag vorschlagen',
    'my-posts' => 'Meine Beiträge',
    'my-vacancies' => 'Meine offenen Stellen',
    'posts-active' => 'Aktiv',
    'posts-approving' => 'Genehmigung',
    'logout' => 'Ausloggen'
];