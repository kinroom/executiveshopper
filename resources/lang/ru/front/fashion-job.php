<?php

return [
    'fashion-job' => 'Вакансии',
    'results' => 'результаты',
    'current-vacancies' => 'Текущие вакансии',
    'search-for-vacancies' => 'Поиск вакансий',
    'add-vacancy' => 'Добавить вакансию',
    'filter' => 'Фильтр',
    'industry' => 'Индустрия',
    'category' => 'Категория',
    'availability' => 'Доступность',
    'location' => 'Страна',
    'country' => 'Страна',
    'city' => 'Город',
    'selected-options' => 'Выбранные опции',
    'clear-filter' => 'Очистить фильтр',
    'salary' => 'Зарплата',
    'click-to-see-more' => 'Больше вакансий',
    'freelance' => 'Фриланс',
    'internship' => 'Штат',
    'all' => 'Все'
];