<?php

return [
    'login' => 'Войти',
    'email' => 'Емейл',
    'register' => 'Регистрация',
    'with-social-networks' => 'Через социальные сети',
    'password' => 'Пароль',
    'what-name' => 'Укажите ваше имя?',
    'register-with-social' => 'Регистрация через социальные сети',
    'please-fill' => 'Пожалуйста, заполните поля перед продолжением',
    'continue' => 'Продолжить',
    'or-enter' => 'или нажмите Enter',
    'what-lastname' => 'Ваша фамилия?',
    'what-email' => 'Ваш емейл?',
    'we-dont-spam' => 'Мы не будем спамить вас, обещаем...',
    'please-fill-valid-email' => 'Пожалуйста введите корректный емейл',
    'enter-password' => 'Введите пароль',
    'review-submit' => 'Отправить',
    'this-will-help-us' => 'Это поможет понять нам, какой вид услуг нужно вам предоставить',
    'welcome' => 'Добро пожаловать',
    'first-choose-status' => 'Для начала, укажите вид вашей деятельности',
    'blogger' => 'Блоггер',
    'employer' => 'Работодатель',
    'reader' => 'Читатель'
 ];