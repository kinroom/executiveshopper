<?php

return [
    'get-back' => 'Назад',
    'add-vacancy' => 'Добавить вакансию',
    'load' => 'Загрузить',
    'send' => 'Отправить',
    'get-contacts' => 'Контакты',
    'freelance' => 'Фриланс',
    'internship' => 'Штат'
];