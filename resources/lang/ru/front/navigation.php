<?php

return [
    'search' => 'Поиск...',
    'personal-profile' => 'Личный кабинет',
    'menu' => 'Меню',
    'language' => 'Язык',
    'blog' => 'Блог',
    'business' => 'Бизнес',
    'shopping' => 'Персональный шоппинг',
    'promotion' => 'Промоушен',
    'about' => 'О нас',
    'work' => 'Сотрудничество',
    'boutique' => 'Бутик',
    'fashion-job' => 'Вакансии'
];