<?php

    return [
        'email-title' => 'Подтверждение регистрации - Executive shopper',
        'email-intro' => 'Для того, чтобы подтвердить ваш емейл, пожалуйста ',
        'email-link' => 'перейдите по ссылке'
    ];
