<?php

return [
    'daily-dose' => 'Ваша дневная порция информации',
    'by' => 'от',
    'all-categories' => 'Все категории',
    'latest-from-categories' => 'Последнии новости по категориям',
    'explore-the-latest-news' => 'Последние новости',
    'most-viewed-posts' => 'Самые просматриваемые публикации',
    'know-what-people-prefer' => 'Узнайте, что предпочитают люди',
    'latest-videos' => 'Последние видео',
    'your-dose-of-visual-inspiration' => 'Ваша доза визуального вдохновения',
    'follow-us-on-instagram' => 'Следите за нашими новостями в instagram',
    'follow-us' => 'Следите за нами',
    'in-social-networks' => 'В социальных сетях',
    'blogger-of-week' => 'Блоггер недели',
    'special-news' => 'Особая новость',
    'be-unique' => 'Будьте уникальным в получении только особых новостей',
    'share-post' => 'Поделиться статьей',
    'continue-reading' => 'Читать далее'

];