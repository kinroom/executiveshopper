<?php

return [
    'about-company' => 'О компании',
    'how-it-works' => 'Как это работает',
    'advertisement' => 'Реклама',
    'ask-me-anything' => 'Задайте вопрос',
    'email-subscribe' => 'Подписаться на рассылку',
    'enter-your-email' => 'Введите ваш емейл',
    'subscribe' => 'Подписаться'
];