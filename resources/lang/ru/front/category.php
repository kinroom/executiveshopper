<?php

return [
    'all' => 'Все',
    'fashion' => 'Мода',
    'lifestyle' => 'Жизнь',
    'beauty' => 'Красота',
    'video' => 'Видео',
    'tag' => 'Тег',
    'search' => 'Поиск',
    'articles' => 'статей'
];