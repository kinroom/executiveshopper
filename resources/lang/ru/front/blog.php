<?php

return [
	'news' => 'Новости',
    'read-article' => 'Читать статью',
    'show-more' => 'Показать больше',
	'articles' => 'статей',
	'filter_by' => 'фильтр',
	'all' => 'все'
];