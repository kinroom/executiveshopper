<?php

return [
    'share' => 'Поделиться статьей',
    'by' => 'от',
    'translator' => 'Переводчик',
    'photographer' => 'Фотограф',
    'styling-makeup' => 'Стайлинг / Мейкап',
    'outfit' => 'Стиль',
    'location' => 'Местоположение',
    'website' => 'Вебсайт',
    'also-buy' => 'Вы можете купить данный товар / услугу',
    'just-click' => 'Нажмите на кнопку и узнайте больше',
    'about-how' => 'о том, как это сделать',
    'buy-service' => 'Заказать',
    'previous-news' => 'Предыдущие новости',
    'next-news' => 'Следующие новости',
    'more-articles' => 'Больше статей',
    'latest' => 'Последние',
    'most-viewed' => 'Популярные',
    'read-article' => 'Читать статью'
];