<?php

return [
    'fashion' => 'Мода',
    'beauty' => 'Красота & Косметика',
    'advertising' => 'Реклама',
    'apparel' => 'Одежда & Аксессуары',
    'communications' => 'Связи',
    'consulting' => 'Консалтинг',
    'retail' => 'Ритейл',
    'manufacturing' => 'Производство',
    'health' => 'Здоровье',
    'freelance' => 'Фриланс',
    'internship' => 'Штат'
];