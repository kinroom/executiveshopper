<?php

return [
    'profile-settings' => 'Настройки профиля',
    'post-propose' => 'Предложить статью',
    'my-posts' => 'Мои статьи',
    'my-vacancies' => 'Мои вакансии',
    'posts-active' => 'Активные',
    'posts-approving' => 'Не активные',
    'logout' => 'Выйти'
];