<?php

return [
    'create-vacancy' => 'Добавить вакансию',
    'accept-rules' => 'Я принимаю правила',
    'choose-company' => 'Выберите компанию, от которой хотите опубликовать вакансию',
    'vacancy-name' => 'Название вакансии',
    'choose-industry' => 'Выберите индустрию',
    'choose-category' => 'Выберите категорию',
    'choose-availability' => 'Выберите доступность',
    'choose-country' => 'Страна',
    'choose-city' => 'Город',
    'choose-salary' => 'Зарплата',
    'write-description' => 'Опишите вакансию',
    'publish-vacancy' => 'Опубликовать вакансию',
    'freelance' => 'Фриланс',
    'internship' => 'Штат'
];