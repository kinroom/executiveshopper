<?php

return [
    'propose-post' => 'Предложить статью',
    'accept-rules' => 'Я принимаю правила',
    'are-commercial' => 'Является ли статья коммерческой?',
    'post-name' => 'Название статьи',
    'choose-category' => 'Выберите категорию',
    'add-cover' => 'Прикрепить изображение',
    'video-url' => 'Ссылка на видео',
    'recommended-dimensions' => 'Рекомендуемые параметры',
    'size-less' => 'Размер менее, чем',
    'write-post' => 'Текст статьи',
    'tags' => 'Теги (разделяйте запятыми)',
    'author-translator' => 'Автор / Переводчик',
    'photographer' => 'Фотограф',
    'styling-makeup' => 'Стайлинг / Мейкап',
    'outfit' => 'Стилист',
    'location' => 'Местоположение',
    'website' => 'Сайт',
    'send-post' => 'Отправить статью'

];