<?php

return [
    'successful-subscribe' => 'You have successfully subscribed to our newsletter',
    'thank-you' => 'Thank you for your subscription',
    'will-sell' => 'The newsletters will be sent to your e-mail address.'
];