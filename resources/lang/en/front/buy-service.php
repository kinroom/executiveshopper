<?php

return [
    'send-request' => 'Send request',
    'description' => 'Enter your name, email and phone, so we can contact you',
    'name' => 'NAME',
    'email' => 'EMAIL',
    'phone' => 'PHONE',
    'are-you-sure' => 'Do you really want to buy this service?',
    'your-data' => 'Your phone number and email will be sent to us, so we can contact you',
    'yes' => 'Yes',
    'no' => 'No'
];