<?php

return [
    'trend' => 'Trend',
    'look' => 'Look',
    'promotion' => 'Promotion',
    'travel' => 'Travel',
    'hotel' => 'Hotel',
    'sport' => 'Sport',
    'food' => 'Food',
    'car' => 'Car',
    'home' => 'Home design',
    'makeup' => 'Makeup',
    'hair' => 'Hair',
    'body' => 'Body'
];