<?php

return [
    'share' => 'Share this post',
    'by' => 'by',
    'translator' => 'Translator',
    'photographer' => 'Photographer',
    'styling-makeup' => 'Styling / Makeup',
    'outfit' => 'Outfit',
    'location' => 'Location',
    'website' => 'Website',
    'also-buy' => 'Also you can buy this service',
    'just-click' => 'Just click the button and know more',
    'about-how' => 'about how you can do that',
    'buy-service' => 'Buy service',
    'previous-news' => 'Previous news',
    'next-news' => 'Next news',
    'more-articles' => 'More articles',
    'latest' => 'Latest',
    'most-viewed' => 'Most viewed',
    'read-article' => 'Read article'
];