<?php

return [
    'login' => 'Login',
    'email' => 'E-mail',
    'register' => 'Register',
    'with-social-networks' => 'With social networks',
    'password' => 'Password',
    'what-name' => 'What is your name?',
    'register-with-social' => 'Register with social networks',
    'please-fill' => 'Please fill the field before continuing',
    'continue' => 'Continue',
    'or-enter' => 'or press Enter',
    'what-lastname' => 'What is your last name?',
    'what-email' => 'What is your email address?',
    'we-dont-spam' => 'We won`t send any spam, we promise...',
    'please-fill-valid-email' => 'Please fill a valid email address',
    'enter-password' => 'Enter your password',
    'review-submit' => 'Review & submit',
    'this-will-help-us' => 'This will help us know what kind of service you need',
    'welcome' => 'Welcome',
    'first-choose-status' => 'First, choose your status',
    'blogger' => 'Blogger',
    'employer' => 'Employer',
    'reader' => 'Reader'
 ];