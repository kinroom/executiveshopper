<?php

return [
    'retail' => 'Retail management & in-Store',
    'marketing' => 'Marketing & e-Commerce',
    'design' => 'Design & Creative',
    'product' => 'Product & Supply chain',
    'administration' => 'Administration & IT'
];