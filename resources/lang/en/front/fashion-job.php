<?php

return [
    'fashion-job' => 'Fashion Job',
    'results' => 'results',
    'current-vacancies' => 'Current vacancies',
    'search-for-vacancies' => 'Search for vacancies',
    'add-vacancy' => 'Add job vacancy',
    'filter' => 'Filter',
    'industry' => 'Industry',
    'category' => 'Category',
    'availability' => 'Availability',
    'location' => 'Location',
    'country' => 'Country',
    'city' => 'City',
    'selected-options' => 'Selected options',
    'clear-filter' => 'Clear filter',
    'salary' => 'Salary',
    'click-to-see-more' => 'Click to see more results',
    'freelance' => 'Freelance',
    'internship' => 'Internship',
    'all' => 'All'
];