<?php

return [
    'error-404' => '404',
    'info' => 'Page not found',
    'button' => 'Main page'
];