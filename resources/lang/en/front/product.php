<?php

return [
    'color' => 'Color',
    'size' => 'Size',
    'buy' => 'Add to bag',
    'video' => 'Video',
    'size_fit' => 'Size & Fit',
    'details' => 'Details',
    'material_care' => 'Material & Care',
    'delivery' => 'Free delivery',
    'are-you-sure' => 'Do you really want to buy this product?'
];