<?php

return [
    'about-company' => 'About company',
    'how-it-works' => 'How it works',
    'advertisement' => 'Advertisement',
    'ask-me-anything' => 'Ask me anything',
    'email-subscribe' => 'Enter your email for weekly updates',
    'enter-your-email' => 'Enter your email here',
    'subscribe' => 'Subscribe'
];