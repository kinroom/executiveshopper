<?php

return [
    'all' => 'All',
    'fashion' => 'Fashion',
    'lifestyle' => 'Lifestyle',
    'beauty' => 'Beauty',
    'video' => 'Video',
    'tag' => 'Tag',
    'search' => 'Search',
    'articles' => 'articles'
];