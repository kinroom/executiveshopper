<?php

return [
    'get-back' => 'Get back',
    'add-vacancy' => 'Add job vacancy',
    'load' => 'Load',
    'send' => 'Send',
    'get-contacts' => 'Get contacts',
    'freelance' => 'Freelance',
    'internship' => 'Internship'
];