<?php
    
    return [
        'success' => 'Successful confirmation',
        'confirmed' => 'Your account was successfully confirmed',
        'dear' => 'Dear',
        'you-can-login' => 'Your account was successfully confirmed and now you can login to the system!'
    ];