<?php

return [
    'not-confirmed' => 'You must confirm your email',
    'confirmed' => 'Your account was not confirmed',
    'please-confirm' => 'Your account was not confirmed. Please confirm your email, using link that we sent you!',
    'didnt-get-link' => 'You didn`t get a link for verification? We can send it again.',
    'send-again' => 'Resend link again.'
];