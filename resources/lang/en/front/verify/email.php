<?php

    return [
        'email-title' => 'Register confirmation - Olga Burbelo',
        'email-intro' => 'To confirm your registration, please ',
        'email-link' => 'click on the link'
    ];
