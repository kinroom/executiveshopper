<?php

    return [
        'confirm-email' => 'Confirm your email',
        'thank-you' => 'Thank you for your registration',
        'to-complete' => 'Thank you for registering our blog. To complete the registration process, you must confirm that you own the email, which is used to configure the account.',
        'confirm-your-email' => 'Confirm your e-mail, opening a link from the message that we sent to you at the time of registration.',
        'confirmation-helps' => 'Confirmation e-mail helps us make sure that we send your account information to the right place.'
    ];