<?php

return [
    'daily-dose' => 'Your daily dose of news',
    'by' => 'by',
    'all-categories' => 'All categories',
    'latest-from-categories' => 'Latest from categories',
    'explore-the-latest-news' => 'Explore the latest news',
    'most-viewed-posts' => 'Most viewed posts',
    'know-what-people-prefer' => 'Know what people prefer',
    'latest-videos' => 'Latest videos',
    'your-dose-of-visual-inspiration' => 'Your dose of visual inspiration',
    'follow-us-on-instagram' => 'Follow us on instagram',
    'follow-us' => 'Follow us',
    'in-social-networks' => 'In social networks',
    'blogger-of-week' => 'Blogger of week',
    'special-news' => 'Special news',
    'be-unique' => 'Be unique in getting only special news',
    'share-post' => 'Share this post',
    'continue-reading' => 'Continue reading'

];