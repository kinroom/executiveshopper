<?php

return [
    'search' => 'Search...',
    'personal-profile' => 'Personal profile',
    'menu' => 'Menu',
    'language' => 'Language',
    'blog' => 'Blog',
    'business' => 'Business',
    'shopping' => 'Personal Shopping',
    'promotion' => 'Promotion',
    'about' => 'About us',
    'work' => 'Work with us',
    'boutique' => 'Boutique',
    'fashion-job' => 'fashion-job'
];