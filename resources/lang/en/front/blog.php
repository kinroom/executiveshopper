<?php

return [
	'news' => 'News',
    'read-article' => 'Read article',
    'show-more' => 'Show more',
	'articles' => 'articles',
	'filter_by' => 'filter by',
	'all' => 'all'
];