<?php
return [
    'vacancy-creating' => 'Create a vacancy',
    'accept-rules' => 'I accept the rules',
    'choose-company' => 'Choose the company from which you want to public this vacancy',
    'name-vacancy' => 'Name of the vacancy',
    'choose-industry' => 'Choose the industry',
    'choose-category' => 'Choose the category',
    'choose-availability' => 'Choose the availability',
    'freelance' => 'Freelance',
    'internship' => 'Internship',
    'country' => 'Country',
    'city' => 'City',
    'salary' => 'Salary',
    'write-about' => 'Write about your vacancy',
    'publish-vacancy' => 'Publish this vacancy',
    'confirmation' => 'Vacancy confirmation',
    'my-vacancies' => 'My vacancies',
    'confirmation-message' => 'Your vacancy has been sent for review to the administrator. After confirmation, your vacancy will be able on the site.'
];