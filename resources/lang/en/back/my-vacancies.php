<?php

return [
    'my-vacancies' => 'My vacancies',
    'first-create-company' => 'Before you create a vacancy - you have to add your company into your personal proﬁle',
    'add-company' => 'Add a company',
    'you-havent-posted' => 'You have`nt posted any vacancy',
    'delete-vacancy' => 'Do you really want to delete this vacancy?',
    'add-vacancy' => 'Add job vacancy',
    'show-more' => 'Show more',
    'availability' => 'Availability',
    'salary' => 'Salary',
    'location' => 'Location',
    
];