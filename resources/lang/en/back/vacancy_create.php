<?php

return [
    'create-vacancy' => 'Create a vacancy',
    'accept-rules' => 'I accept the rules',
    'choose-company' => 'Choose the company from which you want to public this vacancy',
    'vacancy-name' => 'Name of the vacancy',
    'choose-industry' => 'Choose the industry',
    'choose-category' => 'Choose the category',
    'choose-availability' => 'Choose the availability',
    'choose-country' => 'Country',
    'choose-city' => 'City',
    'choose-salary' => 'Salary',
    'write-description' => 'Write about your vacancy',
    'publish-vacancy' => 'Publish this vacancy',
    'freelance' => 'Freelance',
    'internship' => 'Internship'
];