<?php

return [
    'my-posts' => 'My posts',
    'active-posts' => 'Active posts',
    'approving-posts' => 'Approving posts',
    'add-post' => 'Add post',
    'show-more' => 'Show more',
    'want-delete' => 'Do you really want to delete this post?',
    'yes' => 'Yes',
    'no' => 'No',
    'have-no-posts' => 'You have`nt posted anything yet'
];