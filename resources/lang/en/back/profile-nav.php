<?php

return [
    'profile-settings' => 'Profile settings',
    'post-propose' => 'Propose a post',
    'my-posts' => 'My posts',
    'my-vacancies' => 'My vacancies',
    'posts-active' => 'Active',
    'posts-approving' => 'Approving',
    'logout' => 'Log out'
];