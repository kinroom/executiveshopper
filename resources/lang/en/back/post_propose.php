<?php

return [
    'propose-post' => 'Propose a post',
    'accept-rules' => 'I accept the rules',
    'are-commercial' => 'Are post commercial?',
    'post-name' => 'Name of the post',
    'choose-category' => 'Choose the category',
    'add-cover' => 'Add cover',
    'video-url' => 'Video URL',
    'recommended-dimensions' => 'Recommended dimensions',
    'size-less' => 'Size weight less than',
    'write-post' => 'Write your post',
    'tags' => 'Tags (separated by commas)',
    'author-translator' => 'Author / Translator',
    'photographer' => 'Photographer',
    'styling-makeup' => 'Styling / Makeup',
    'outfit' => 'Outfit',
    'location' => 'Location',
    'website' => 'Website',
    'send-post' => 'Send post'

];