<?php

return [
    'fashion' => 'Fashion',
    'beauty' => 'Beauty & Cosmetic',
    'advertising' => 'Advertising',
    'apparel' => 'Apparel & Accessories',
    'communications' => 'Communications',
    'consulting' => 'Consulting',
    'retail' => 'Retail',
    'manufacturing' => 'Manufacturing',
    'health' => 'Health',
    'freelance' => 'Freelance',
    'internship' => 'Internship'
];