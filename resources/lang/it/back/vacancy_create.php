<?php

return [
    'create-vacancy' => 'Creazione di un posto vacante',
    'accept-rules' => 'Accetto le regole',
    'choose-company' => 'Scegliere la società da cui si desidera pubblico presente bando',
    'vacancy-name' => 'Nome della vacanza',
    'choose-industry' => 'Scegliere il settore',
    'choose-category' => 'Scegli la categoria',
    'choose-availability' => 'Scegliere la disponibilità',
    'choose-country' => 'Nazione',
    'choose-city' => 'Città',
    'choose-salary' => 'Stipendio',
    'write-description' => 'Scrivere sulla tua vacanza',
    'publish-vacancy' => 'Pubblicare questo posto vacante',
    'freelance' => 'Libero professionista',
    'internship' => 'Tirocinio'
];