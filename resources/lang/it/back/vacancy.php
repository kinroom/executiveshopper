<?php
return [
    'vacancy-creating' => 'Creazione di un posto vacante',
    'accept-rules' => 'Accetto le regole',
    'choose-company' => 'Scegliere la società da cui si desidera pubblico presente bando',
    'name-vacancy' => 'Nome della vacanza',
    'choose-industry' => 'Scegliere il settore',
    'choose-category' => 'Scegli la categoria',
    'choose-availability' => 'Scegliere la disponibilità',
    'freelance' => 'Libero professionista',
    'internship' => 'Tirocinio',
    'country' => 'Nazione',
    'city' => 'Città',
    'salary' => 'Stipendio',
    'write-about' => 'Scrivere sulla tua vacanza',
    'publish-vacancy' => 'Pubblicare questo posto vacante',
    'confirmation' => 'Conferma vacancy',
    'my-vacancies' => 'Le mie offerte di lavoro',
    'confirmation-message' => 'Il tuo posto vacante è stato inviato per la revisione per l\'amministratore. Dopo la conferma, la vostra vacanza sarà in grado sul sito.'
];