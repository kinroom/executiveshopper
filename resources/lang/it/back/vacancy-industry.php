<?php

return [
    'fashion' => 'Moda',
    'beauty' => 'Beauty & Cosmetic',
    'advertising' => 'Pubblicità',
    'apparel' => 'Abbigliamento e accessori',
    'communications' => 'Comunicazioni',
    'consulting' => 'Consulenza',
    'retail' => 'Al dettaglio',
    'manufacturing' => 'Produzione',
    'health' => 'Salute',
    'freelance' => 'Libero professionista',
    'internship' => 'Tirocinio'
];