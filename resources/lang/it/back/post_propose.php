<?php

return [
    'propose-post' => 'Proporre un post',
    'accept-rules' => 'Accetto le regole',
    'are-commercial' => 'Sono posta commerciale?',
    'post-name' => 'Nome del post',
    'choose-category' => 'Scegli la categoria',
    'add-cover' => 'Aggiungere copertura',
    'video-url' => 'Video URL',
    'recommended-dimensions' => 'Dimensioni consigliati',
    'size-less' => 'Peso dimensione inferiore a',
    'write-post' => 'Scrivi il tuo messaggio',
    'tags' => 'Tag (separati da virgole)',
    'author-translator' => 'Autore / Traduttore',
    'photographer' => 'Fotografo',
    'styling-makeup' => 'Styling / Trucco',
    'outfit' => 'Attrezzatura',
    'location' => 'Luogo',
    'website' => 'Sito web',
    'send-post' => 'Invia messaggio'

];