<?php

return [
    'profile-settings' => 'Impostazioni del profilo',
    'post-propose' => 'Proporre un post',
    'my-posts' => 'I miei messaggi',
    'my-vacancies' => 'Le mie offerte di lavoro',
    'posts-active' => 'Attivo',
    'posts-approving' => 'Approvazione',
    'logout' => 'Disconnettersi'
];