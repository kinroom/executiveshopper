<?php

return [
    'my-posts' => 'I miei messaggi',
    'active-posts' => 'Messaggi attivi',
    'approving-posts' => 'Approvazione messaggi',
    'add-post' => 'Aggiungere postale',
    'show-more' => 'Mostra di più',
    'want-delete' => 'Vuoi davvero eliminare questo post?',
    'yes' => 'Sì',
    'no' => 'No',
    'have-no-posts' => 'Non hai postato ancora nulla'
];