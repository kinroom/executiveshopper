<?php

return [
    'my-vacancies' => 'Le mie offerte di lavoro',
    'first-create-company' => 'Prima di creare un posto vacante - è necessario aggiungere la vostra azienda nel personal pro fi',
    'add-company' => 'Aggiungere una società',
    'you-havent-posted' => 'Non hai inviato alcun posto vacante',
    'delete-vacancy' => 'Vuoi davvero eliminare questo posto vacante?',
    'add-vacancy' => 'Aggiungere offerta di lavoro',
    'show-more' => 'Mostra di più',
    'availability' => 'Disponibilità',
    'salary' => 'Stipendio',
    'location' => 'Luogo',
    
];