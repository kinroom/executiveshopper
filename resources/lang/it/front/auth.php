<?php

return [
    'login' => 'Accesso',
    'email' => 'E-mail',
    'register' => 'Registro',
    'with-social-networks' => 'Con i social network',
    'password' => 'Parola d`ordine',
    'what-name' => 'Come ti chiami?',
    'register-with-social' => 'Registrati con le reti sociali',
    'please-fill' => 'Si prega di compilare il campo prima di continuare',
    'continue' => 'Proseguire',
    'or-enter' => 'oppure premere Invio',
    'what-lastname' => 'Qual`è il tuo cognome?',
    'what-email' => 'Qual è il tuo indirizzo e-mail?',
    'we-dont-spam' => 'Non inviare alcun spam, promettiamo ...',
    'please-fill-valid-email' => 'Si prega di compilare un indirizzo email valido',
    'enter-password' => 'Inserisci la tua password',
    'review-submit' => 'Review & presentare',
    'this-will-help-us' => 'Questo ci aiuterà a sapere che tipo di servizio è necessario',
    'welcome' => 'Benvenuto',
    'first-choose-status' => 'In primo luogo, scegliere il proprio stato',
    'blogger' => 'Blogger',
    'employer' => 'Employer',
    'reader' => 'Lettore'
 ];