<?php

return [
    'share' => 'Condividi questo post',
    'by' => 'di',
    'translator' => 'Traduttore',
    'photographer' => 'Fotografo',
    'styling-makeup' => 'Styling / Trucco',
    'outfit' => 'Attrezzatura',
    'location' => 'Luogo',
    'website' => 'Sito web',
    'also-buy' => 'Inoltre è possibile acquistare questo servizio',
    'just-click' => 'Basta fare clic sul pulsante e saperne di più',
    'about-how' => 'su come si può fare',
    'buy-service' => 'servizio di acquisto',
    'previous-news' => 'Notizie precedenti',
    'next-news' => 'Notizia successiva',
    'more-articles' => 'Altri articoli',
    'latest' => 'Più recente',
    'most-viewed' => 'I più visti',
    'read-article' => 'Leggi l`articolo'
];