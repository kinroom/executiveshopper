<?php

return [
	'news' => 'Notizia',
	'read-article' => 'Leggi l`articolo',
	'show-more' => 'Mostra di più',
	'articles' => 'articoli',
	'filter_by' => 'filtrare',
	'all' => 'tutti'
];