<?php

return [
    'get-back' => 'Riavere',
    'add-vacancy' => 'Aggiungere offerta di lavoro',
    'load' => 'Caricare',
    'send' => 'Inviare',
    'get-contacts' => 'Prendi contatti',
    'freelance' => 'Libero professionista',
    'internship' => 'Tirocinio'
];