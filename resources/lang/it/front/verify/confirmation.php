<?php

    return [
        'confirm-email' => 'Conferma la tua email',
        'thank-you' => 'Grazie per la tua registrazione',
        'to-complete' => 'Grazie per la registrazione del blog. Per completare il processo di registrazione, è necessario confermare che si possiede l`e-mail, che viene utilizzato per configurare l`account.',
        'confirm-your-email' => 'Conferma la tua e-mail, l`apertura di un collegamento dal messaggio che abbiamo inviato a voi al momento della registrazione.',
        'confirmation-helps' => 'Mail di conferma ci aiuta a fare in modo che inviamo le informazioni dell\'account al posto giusto.'
    ];