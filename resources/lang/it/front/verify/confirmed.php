<?php
    
    return [
        'success' => 'Conferma di successo',
        'confirmed' => 'Il tuo account è stato correttamente confermato',
        'dear' => 'Caro',
        'you-can-login' => 'Il tuo account è stato confermato con successo e ora è possibile accedere al sistema!'
    ];