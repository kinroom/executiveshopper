<?php

return [
    'not-confirmed' => 'È necessario confermare la tua e-mail',
    'confirmed' => 'Il tuo account non è stata confermata',
    'please-confirm' => 'Il tuo account non è stato confermato. Si prega di confermare la vostra e-mail, utilizzando link che ti abbiamo inviato!',
    'didnt-get-link' => 'Non hai ottenere un link per la verifica? Siamo in grado di inviare di nuovo.',
    'send-again' => 'Invia di nuovo di nuovo collegamento.'
];