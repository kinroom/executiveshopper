<?php

    return [
        'email-title' => 'Registrati conferma - Olga Burbelo',
        'email-intro' => 'Per confermare la registrazione, ',
        'email-link' => 'clicca sul link'
    ];
