<?php

return [
    'error-404' => '404',
    'info' => 'Pagina non trovata',
    'button' => 'Pagina principale'
];