<?php

return [
    'all' => 'Tutti',
    'fashion' => 'Moda',
    'lifestyle' => 'Stile di vita',
    'beauty' => 'Bellezza',
    'video' => 'Video',
    'tag' => 'Etichetta',
    'search' => 'Ricerca',
    'articles' => 'articoli'
];