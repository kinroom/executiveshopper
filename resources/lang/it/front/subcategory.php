<?php

return [
    'trend' => 'Tendenza',
    'look' => 'Guarda',
    'promotion' => 'Promozione',
    'travel' => 'Viaggio',
    'hotel' => 'Hotel',
    'sport' => 'Sport',
    'food' => 'Cibo',
    'car' => 'Auto',
    'home' => 'Arredamento d`interni',
    'makeup' => 'Trucco',
    'hair' => 'Capelli',
    'body' => 'Corpo'
];