<?php

return [
    'send-request' => 'Invia richiesta',
    'description' => 'Inserisci il tuo nome, email e telefono, in modo che possiamo contattarvi',
    'name' => 'NOME',
    'email' => 'E-MAIL',
    'phone' => 'TELEFONO',
    'are-you-sure' => 'Vuoi davvero comprare questo servizio?',
    'your-data' => 'Il tuo numero di telefono ed e-mail saranno inviati a noi, in modo che possiamo contattarvi',
    'yes' => 'Sì',
    'no' => 'No'
];