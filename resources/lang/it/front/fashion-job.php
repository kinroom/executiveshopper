<?php

return [
    'fashion-job' => 'Moda lavoro',
    'results' => 'risultati',
    'current-vacancies' => 'Offerte di lavoro attuali',
    'search-for-vacancies' => 'Ricerca alberghi',
    'add-vacancy' => 'Aggiungere offerta di lavoro',
    'filter' => 'Filtro',
    'industry' => 'Industria',
    'category' => 'Categoria',
    'availability' => 'Disponibilità',
    'location' => 'Luogo',
    'country' => 'Nazione',
    'city' => 'Città',
    'selected-options' => 'Opzioni selezionate',
    'clear-filter' => 'Filtro pulito',
    'salary' => 'Stipendio',
    'click-to-see-more' => 'Clicca per vedere più risultati',
    'freelance' => 'Libero professionista',
    'internship' => 'Tirocinio',
    'all' => 'Tutti'
];