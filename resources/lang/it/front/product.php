<?php

return [
    'color' => 'Colore',
    'size' => 'Dimensione',
    'buy' => 'Aggiungi al carrello',
    'video' => 'Video',
    'size_fit' => 'Dimensione & In forma',
    'details' => 'Dettagli',
    'material_care' => 'Materiale & Cura',
    'delivery' => 'Consegna gratuita',
    'are-you-sure' => 'Vuoi davvero per acquistare questo prodotto?'
];