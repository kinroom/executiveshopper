<?php

return [
    'search' => 'Ricerca...',
    'personal-profile' => 'Profilo personale',
    'menu' => 'Menu',
    'language' => 'Lingua',
    'blog' => 'Blog',
    'business' => 'Attività commerciale',
    'shopping' => 'Personal Shopping',
    'promotion' => 'Promozione',
    'about' => 'Riguardo a noi',
    'work' => 'Lavora con noi',
    'boutique' => 'Boutique',
    'fashion-job' => 'fashion-lavoro'
];