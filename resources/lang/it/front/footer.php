<?php

return [
    'about-company' => 'Circa la società',
    'how-it-works' => 'Come funziona',
    'advertisement' => 'Annuncio pubblicitario',
    'ask-me-anything' => 'Chiedimi qualunque cosa',
    'email-subscribe' => 'Inserisci la tua email per gli aggiornamenti settimanali',
    'enter-your-email' => 'Inserisci la tua email qui',
    'subscribe' => 'Sottoscrivi'
];