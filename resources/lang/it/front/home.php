<?php

return [
    'daily-dose' => 'La dose giornaliera di notizie',
    'by' => 'di',
    'all-categories' => 'Tutte le categorie',
    'latest-from-categories' => 'Ultime dalle categorie',
    'explore-the-latest-news' => 'Esplora le ultime notizie',
    'most-viewed-posts' => 'Messaggi più visti',
    'know-what-people-prefer' => 'Conoscere ciò che la gente preferisce',
    'latest-videos' => 'Gli ultimi video',
    'your-dose-of-visual-inspiration' => 'La dose di ispirazione visiva',
    'follow-us-on-instagram' => 'Seguici su Instagram',
    'follow-us' => 'Seguici',
    'in-social-networks' => 'Nelle reti sociali',
    'blogger-of-week' => 'Blogger di settimana',
    'special-news' => 'Notizie speciale',
    'be-unique' => 'Essere unico a ottenere solo notizie speciale',
    'share-post' => 'Condividi questo post',
    'continue-reading' => 'Continua a leggere'

];