<?php

return [
    'retail' => 'Retail Management & In-Store',
    'marketing' => 'Marketing & eCommerce',
    'design' => 'Design & Creative',
    'product' => 'Prodotto & Supply Chain',
    'administration' => 'Amministrazione e IT'
];