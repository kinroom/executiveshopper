<?php

return [
    'successful-subscribe' => 'Ti sei iscritto con successo alla nostra newsletter',
    'thank-you' => 'Grazie per l`abbonamento',
    'will-sell' => 'La newsletter verrà inviata al tuo indirizzo email.'
];