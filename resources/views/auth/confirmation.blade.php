@extends('front.template')

@section('title', trans('front/verify/confirmation.confirm-email') . ' - Executive Shopper')

@section('main')
    <div class="static-page-wrapper static-page-about-us-wrapper">
        <section class="fjob-list-section static-page-section clearfix">
            <article>
                <h1 class="static-page-heading">Executive shopper<br><span>{{trans('front/verify/confirmation.thank-you')}}!</span></h1>
                <p>{{trans('front/verify/confirmation.to-complete')}}</p>
                <p>{{trans('front/verify/confirmation.confirm-your-email')}}</p>
                <p>{{trans('front/verify/confirmation.confirmation-helps')}}</p>
                <p></p>
            </article>
        </section>
    </div>
@stop