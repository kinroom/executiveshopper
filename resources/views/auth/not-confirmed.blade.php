@extends('front.template')

@section('title', trans('front/verify/not-confirmed.not-confirmed') . ' - Executive Shopper')

@section('main')
    <div class="static-page-wrapper static-page-about-us-wrapper">
        <section class="fjob-list-section static-page-section clearfix">
            <article>
                <h1 class="static-page-heading">Executive shopper<br><span>{{trans('front/verify/not-confirmed.confirmed')}}!</span></h1>
                <p>{{trans('front/verify/not-confirmed.please-confirm')}}</p>
                <p>{{trans('front/verify/not-confirmed.didnt-get-link')}}</p>
                <p><a href="/{{App::getLocale()}}/resend-email">{{trans('front/verify/not-confirmed.send-again')}}</a></p>
                <p></p>
            </article>
        </section>
    </div>
@stop