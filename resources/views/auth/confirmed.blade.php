@extends('front.template')

@section('title', trans('front/verify/confirmed.success') . ' - Executive Shopper')

@section('main')
    <div class="static-page-wrapper static-page-about-us-wrapper">
        <section class="fjob-list-section static-page-section clearfix">
            <article>
                <h1 class="static-page-heading">Executive shopper<br><span>{{trans('front/verify/confirmed.confirmed')}}!</span></h1>
                <p>{{trans('front/verify/confirmed.dear')}} {{$firstname}} {{$lastname}}.</p>
                <p>{{trans('front/verify/confirmed.you-can-login')}}</p>
                <p></p>
            </article>
        </section>
    </div>
@stop