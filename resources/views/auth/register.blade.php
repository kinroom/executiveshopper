@extends('auth/layout')

@section('content')
    @foreach ($errors->all() as $error)
        {{ $error }}
    @endforeach
            <body class="register-page">
                <div class="container">
                    <div class="fs-form-wrap" id="fs-form-wrap">
                        <a class="header-logo" href="/">
                            <img src="{!! asset('img/executive_shopper_black.svg') !!}" alt="Executive Shopper Blog">
                        </a>
                        <form method="post" action="/{{App::getLocale()}}/auth/register" class="fs-form fs-form-full" id="registration" autocomplete="off" role="form">
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
                            <ol class="fs-fields">
                                <li data-input-trigger>
                                    <label class="fs-field-label fs-anim-upper" for="q3" data-info="{{ trans('front/auth.this-will-help-us') }}">{{ trans('front/auth.welcome') }}!<br/> {{ trans('front/auth.first-choose-status') }}:</label>
                                    <div class="fs-radio-group fs-radio-custom clearfix fs-anim-lower">
                                        <span><input id="q3b" name="role" type="radio" value="blogger" required/><label for="q3b" class="radio-blogger">{{ trans('front/auth.blogger') }}</label></span>
                                        <span><input id="q3c" name="role" type="radio" value="employer"/><label for="q3c" class="radio-employer">{{ trans('front/auth.employer') }}</label></span>
                                        <span><input id="q3a" name="role" type="radio" value="user"/><label for="q3a" class="radio-user">{{ trans('front/auth.reader') }}</label></span>
                                    </div>
                                </li>
                                <li>
                                    <label class="fs-field-label fs-anim-upper" for="firstname">{{ trans('front/auth.what-name') }}</label>
                                    <input class="fs-anim-lower" id="q1" name="firstname" type="text" placeholder="Dean" required/>
                                    <div class="social-register fs-anim-lower">
                                        <h2>{{ trans('front/auth.register-with-social') }}</h2>
                                        <a href="/vkontakte" id="social_vk"><i class="fa fa-vk"></i></a>
                                        <a href="/facebook" id="social_fb"><i class="fa fa-facebook"></i></a>
                                        <a href="/google" id="social_gg"><i class="fa fa-google"></i></a>
                                        <a href="/twitter" id="social_tw"><i class="fa fa-twitter"></i></a>
                                    </div>
                                </li>
                                <li>
                                    <label class="fs-field-label fs-anim-upper" for="lastname">{{ trans('front/auth.what-lastname') }}</label>
                                    <input class="fs-anim-lower" id="q2" name="lastname" type="text" placeholder="Moriarty" required/>
                                </li>
                                <li>
                                    <label class="fs-field-label fs-anim-upper" for="email" data-info="{{ trans('front/auth.we-dont-spam') }}">{{ trans('front/auth.what-email') }}</label>
                                    <input class="fs-anim-lower" id="q3" name="email" type="email" placeholder="dean@road.us" required/>
                                </li>
                                <li>
                                    <label class="fs-field-label fs-anim-upper" for="password" >{{ trans('front/auth.enter-password') }}</label>
                                    <input class="fs-anim-lower" id="q4" name="password" type="password" placeholder="***" required/>
                                </li>
                            </ol><!-- /fs-fields -->
                            <button class="fs-submit" type="submit">{{ trans('front/auth.register') }}</button>
                        </form><!-- /fs-form -->
                    </div><!-- /fs-form-wrap -->

                </div><!-- /container -->
                <link href="{{ asset('css/register.css') }}" rel="stylesheet">
                <script src="{{asset('js/classie.js')}}" ></script>
                <script src="{{asset('js/fullscreenForm.js')}}"></script>
                <script>

                    $(function() {

                        /* Set the role of user to a route */
                        $("input[name='role']").on('click', function() {
                           var role = $(this).val();

                            $("#social_vk").attr("href", '/vkontakte/' + role)
                            $("#social_fb").attr("href", '/facebook/' + role)
                            $("#social_gg").attr("href", '/google/' + role)
                            $("#social_tw").attr("href", '/twitter/' + role)
                        })

                    });

                    (function() {
                        var formWrap = document.getElementById( 'fs-form-wrap' );

                        [].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {
                            new SelectFx( el, {
                                stickyPlaceholder: false,
                                onChange: function(val){
                                    document.querySelector('span.cs-placeholder').style.backgroundColor = val;
                                }
                            });
                        } );
                        new FForm( formWrap, {
                            onReview : function() {
                                classie.add( document.body, 'overview' ); // for demo purposes only
                            }
                        } );
                    })();

                </script>

            </body>
@stop