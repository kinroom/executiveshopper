
@foreach ($slider as $key => $slide)
    <tr {!! session('statut') == 'admin'? 'class="warning"' : '' !!}>
        <td class="text-primary"><strong>{{ $key + 1 }}</strong></td>
        <td class="text-primary"><strong>{{ $slide->post->category->name }}</strong></td>
        <td class="text-primary"><strong>{{ $slide->post->title }}</strong></td>
        <td class="text-primary"><strong>{{ $slide->post->user->firstname . ' ' . $slide->post->user->lastname }}</strong></td>
    <td>{!! link_to('/','See' , ['class' => 'btn btn-success btn-block btn']) !!}</td>

    <td>{!! link_to_route('admin.slider.edit', 'Edit', [$slide->id], ['class' => 'btn btn-warning btn-block']) !!}</td>
        <td>
            {!! Form::open(['method' => 'DELETE', 'route' => ['admin.slider.destroy', $slide->id]]) !!}
                {!! Form::destroy('Destroy', 'Destroy') !!}
            {!! Form::close() !!}
        </td>
    </tr>
@endforeach