@extends('back.template')

@section('main')

    @include('back.partials.entete', ['title' => 'Slider', 'icone' => 'pencil', 'fil' => link_to('slider', 'Slider') . ' / ' . 'Dashboard'])

    <div class="col-sm-12">
        @yield('form')

        {!! Form::open(['method' => 'UPDATE', 'url' => '/slider/' . $slide->id, 'enctype' => 'multipart/form-data']) !!}
         <div class="form-group">
            {!! Form::label('post', 'Post title:') !!}
            {!! Form::select('post', $posts, $slide->post, ['class' => 'form-control', 'required' => 'required']) !!}
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            {!! Form::submit('Update') !!}
        {!! Form::close() !!}
    </div>
@stop
