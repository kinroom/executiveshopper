@extends('back.template')

@section('title', "Slider")

@section('main')
    @include('back.partials.entete', ['title' => 'Slider' . link_to_route('admin.slider.create', 'Slider add', [], ['class' => 'btn btn-info pull-right']), 'icone' => 'folder-open', 'fil' => 'Slider\Add'])

    @if(session()->has('ok'))
        @include('partials/error', ['type' => 'success', 'message' => session('ok')])
    @endif
        @if (count($slider) > 0)
            <div class="row col-lg-12">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th> Slide number </th>
                                <th> Category </th>
                                <th> Title </th>
                                <th> Author </th>
                            </tr>
                        </thead>
                        <tbody>
                            @include('back.slider.table')
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
@stop