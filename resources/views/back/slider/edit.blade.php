@extends('back.template')

@section('title', "Edit the slide")

@section('main')

    @include('back.partials.entete', ['title' => 'Edit slider', 'icone' => 'fa fa-sliders', 'fil' =>'Slider'])

    {!! Form::open(['route' => ['admin.slider.update', $slide->id], 'method' => 'put',  'class' => 'form-horizontal panel']) !!}
    <div class="form-group">
        {!! Form::label('post', 'Post title:') !!}
        {!! Form::select('post', $posts, $slide->post->id, ['class' => 'form-control', 'required' => 'required']) !!}
    </div>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    {!! Form::submit('Update') !!}
    {!! Form::close() !!}
@stop
