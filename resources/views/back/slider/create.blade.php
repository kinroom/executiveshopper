@extends('back.template')
@section('title', "Create a slide")

@section('main')

    @include('back.partials.entete', ['title' => 'Add slider', 'icone' => 'fa fa-sliders', 'fil' =>'Slider'])

    @if(session()->has('ok'))
        @include('partials/error', ['type' => 'success', 'message' => session('ok')])
    @endif

    {!! Form::open(['route' => 'admin.slider.store', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
        <div class="form-group">
            {!! Form::label('post', 'Post title:') !!}
            {!! Form::select('post', $posts, null, ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
    <div class="form-group">
        {!! Form::submit('Add') !!}
    </div>
   {!! Form::close() !!}
@stop
