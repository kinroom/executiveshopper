@extends('front.template')

@section('title', trans('back/vacancy_create.create-vacancy'))

@section('main')
    <section class="fjob-list-section profile-page clearfix">
        @include('back.profile.profile-left')

        @if (isset($companies) && count($companies) > 0)
            <div class="profile-right clearfix">
                {!! Form::open(['url' => App::getLocale() . '/fashion-job', 'method' => 'post', 'class' => 'clearfix propose-post-form']) !!}
                    <div class="rules-block">
                        {!! Form::checkbox('accept-rules','1', '', ['class' => 'checkbox-filter profile-checkbox', 'id' => 'accept-rules', 'required' => 'required']) !!}
                        {!! Form::label('accept-rules', trans('back/vacancy_create.accept-rules')) !!}
                    </div>
                    <div class="clearfix addvacancy-inform-block addvacancy-inform-block-onlyone">
                        <span class="addvacancy-block-heading">{{trans('back/vacancy_create.choose-company')}}</span>
                        @foreach ($companies as $company)
                            {!! Form::checkbox("company-$company->name", "$company->id", '', ['class' => 'checkbox-filter profile-checkbox post-main-category-checkbox', 'id' => "company-$company->name"]) !!}
                            {!! Form::label("company-$company->name", "$company->name") !!}
                        @endforeach
                    </div>
                    <input class="block-input-profile block-input-profile-post-name block-input-profile-vacancy-name" type="text" placeholder="{{trans('back/vacancy_create.vacancy-name')}}" name="title" id="title" required>
                    <div class="clearfix addvacancy-inform-block addvacancy-inform-block-onlyone">
                        <span class="addvacancy-block-heading">{{trans('back/vacancy_create.choose-industry')}}</span>
                        @foreach ($industries as $industry)
                            {!! Form::checkbox("industry-$industry->name", "$industry->id", '', ['class' => 'checkbox-filter profile-checkbox post-main-category-checkbox', 'id' => "industry-$industry->name"]) !!}
                            {!! Form::label("industry-$industry->name", trans('back/vacancy-industry.'.$industry->name)) !!}
                        @endforeach
                    </div>
                    <div class="clearfix addvacancy-inform-block addvacancy-inform-block-onlyone">
                        <span class="addvacancy-block-heading">{{trans('back/vacancy_create.choose-category')}}</span>
                        @foreach ($categories as $category)
                            {!! Form::checkbox("category-$category->name", "$category->id", '', ['class' => 'checkbox-filter profile-checkbox post-main-category-checkbox', 'id' => "category-$category->name"]) !!}
                            {!! Form::label("category-$category->name", trans('back/vacancy-category.'.$category->name)) !!}
                        @endforeach
                    </div>
                    <div class="clearfix addvacancy-inform-block addvacancy-inform-block-onlyone">
                        <span class="addvacancy-block-heading">{{trans('back/vacancy_create.choose-availability')}}</span>
                        {!! Form::checkbox('availability-freelance', '0', '', ['class' => 'checkbox-filter profile-checkbox post-main-category-checkbox', 'id' => 'availability-freelance']) !!}
                        {!! Form::label('availability-freelance', trans('back/vacancy_create.freelance')) !!}
                        {!! Form::checkbox('availability-internship', '1', '', ['class' => 'checkbox-filter profile-checkbox post-main-category-checkbox', 'id' => 'availability-internship']) !!}
                        {!! Form::label('availability-internship', trans('back/vacancy_create.internship')) !!}
                    </div>
                    <div class="clearfix addvacancy-inform-block">
                        <ul class="filter-list filter-list-select">
                            <li class="clearfix">
                                <label for="filter-country">{{trans('back/vacancy_create.choose-country')}}</label>
                                <select name="filter-country" id="filter-country">
                                    <option>All</option>
                                    @foreach ($countries as $key=>$country)
                                        <option value="{{$country->id_country}}">{{$country->country_name_en}}</option>
                                    @endforeach
                                </select>
                            </li>
                            <li class="clearfix">
                                <label for="filter-city">{{trans('back/vacancy_create.choose-city')}}</label>
                                <select name="filter-city" id="filter-city">
                                    <option>All</option>
                                </select>
                            </li>
                            <li class="clearfix">
                                <div class="post-fields">
                                    <div class="clearfix tagsinput addvacancy-salary">
                                        <label for="addvacancy-salary">{{trans('back/vacancy_create.choose-salary')}}</label>
                                        <input type="text" name="salary" id="addvacancy-salary" placeholder="$2000 - $3000">
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix addvacancy-inform-block">
                        <span class="write-post-label write-vacancy-label">{{trans('back/vacancy_create.write-description')}}</span>
                        <div class="text-edit-container">
                            {!! Form::textarea('description', '', '',['required' => 'required']) !!}
                        </div>
                    </div>
                    <div class="clearfix">
                        <input type="submit" value="{{trans('back/vacancy_create.publish-vacancy')}}" class="send-post-button" id="send-vacancy-button">
                    </div>
                    {!! Form::hidden('company_id', '', ['id' => 'company_id', 'required' => 'required']) !!}
                    {!! Form::hidden('industry_id', '', ['id' => 'industry_id', 'required' => 'required']) !!}
                    {!! Form::hidden('category_id', '', ['id' => 'cat_id', 'required' => 'required']) !!}
                    {!! Form::hidden('availability_id', '', ['id' => 'availability_id', 'required' => 'required']) !!}
                {!! Form::close() !!}
            </div>
        @else
            <div class="profile-right profile-right-vacancies-addcompany clearfix">
                <div class="clearfix">
                    <h2>{{trans('back/vacancy_create.first-create-company')}}</h2>
                </div>
                <a href="/profile/" class="noposts-button novacancy-button vacancies-addcompany-button">{{trans('back/vacancy_create.add-company')}}</a>
            </div>
        @endif
        <div class="filter-overlay filter-overlay-profile" style="opacity: 0;"></div>
    </section>
@stop

@section('scripts')
    {!! HTML::script('ckeditor/ckeditor.js') !!}
    <script>
        var config = {
            language: '{{ config('app.locale') }}',
            height: 300,
            filebrowserBrowseUrl: '{!! url($url) !!}',
            toolbarGroups: [
                { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
                { name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
                { name: 'links' },
                { name: 'insert' },
                { name: 'forms' },
                { name: 'tools' },
                { name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
                { name: 'others' },
                //'/',
                { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
                { name: 'styles' },
                { name: 'colors' }
            ]
        };
        CKEDITOR.replace('description', config);
    </script>
@stop

