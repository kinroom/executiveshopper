@extends('back.template')

@section('title', "All vacancies")

@section('main')

    @include('back.partials.entete', ['title' => 'Vacancies', 'icone' => 'pencil', 'fil' => 'Vacancies'])

    @if(session()->has('ok'))
        @include('partials/error', ['type' => 'success', 'message' => session('ok')])
    @endif

    <div class="row col-lg-12">
        <div class="pull-right link">{!! $vacancies !!}</div>
    </div>

    <div class="row col-lg-12">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>
                        Title
                    </th>
                    <th>
                        Date
                    </th>
                    <th>
                        Seen
                    </th>
                    <th>
                        Published
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach ($vacancies as $vacancy)
                    <tr>
                        <td class="text-primary"><strong>{{ $vacancy->title }}</strong></td>
                        <td>{{ $vacancy->created_at }}</td>
                        <td>{!! Form::checkbox('seen', $vacancy->id, $vacancy->seen) !!}</td>
                        <td>{!! Form::checkbox('is_active', $vacancy->id, $vacancy->is_active) !!}</td>
                        <td>{!! link_to('fashion-job/' . $vacancy->id . '/', 'See', ['class' => 'btn btn-success btn-block btn']) !!}</td>
                        <td>
                            {!! Form::open([ 'method' => 'DELETE', 'route' => ['fashion-job.destroy', $vacancy->id] ]) !!}
                                {!! Form::destroy('Destroy', 'Do you really want to delete it?') !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row col-lg-12">
        <div class="pull-right link">{!! $vacancies !!}</div>
    </div>
@stop

@section('scripts')

    <script>

        $(function() {

            // Seen gestion
            $(document).on('change', ':checkbox[name="seen"]', function() {
                $(this).parents('tr').toggleClass('warning');
                $(this).hide().parent().append('<i class="fa fa-refresh fa-spin"></i>');
                var token = $('input[name="_token"]').val();
                var url = '{{ route('admin.vacancyseen', ':id') }}';
                url = url.replace(':id', this.value);
                $.ajax({
                            url: url,
                            type: 'PUT',
                            data: "seen=" + this.checked + "&_token=" + token
                        })
                        .done(function() {
                            $('.fa-spin').remove();
                            $('input:checkbox[name="seen"]:hidden').show();
                        })
                        .fail(function() {
                            $('.fa-spin').remove();
                            chk = $('input:checkbox[name="seen"]:hidden');
                            chk.show().prop('checked', chk.is(':checked') ? null:'checked').parents('tr').toggleClass('warning');
                            alert('{{ trans('back/blog.fail') }}');
                        });
            });

            // Active gestion
            $(document).on('change', ':checkbox[name="is_active"]', function() {
                $(this).hide().parent().append('<i class="fa fa-refresh fa-spin"></i>');
                var token = $('input[name="_token"]').val();
                var url = '{{ route('admin.vacancyactive', ':id') }}';
                url = url.replace(':id', this.value);
                $.ajax({
                            url:  url,
                            type: 'PUT',
                            data: "active=" + this.checked + "&_token=" + token
                        })
                        .done(function() {
                            $('.fa-spin').remove();
                            $('input:checkbox[name="is_active"]:hidden').show();
                        })
                        .fail(function() {
                            $('.fa-spin').remove();
                            chk = $('input:checkbox[name="is_active"]:hidden');
                            chk.show().prop('checked', chk.is(':checked') ? null:'checked').parents('tr').toggleClass('warning');
                            alert('{{ trans('back/blog.fail') }}');
                        });
            });


        });

    </script>

@stop
