@extends('front.template')

@section('title', trans('back/vacancy.confirmation'))

@section('main')
    <section class="fjob-list-section profile-page clearfix">
        @include('back.profile.profile-left')
            <div class="profile-right profile-right-noposts clearfix">
                <h2>{{trans('back/vacancy.confirmation-message')}}</h2>
                <a href="/profile/vacancies" class="noposts-button novacancy-button">{{trans('back/vacancy.my-vacancies')}}</a>
            </div>
        <div class="filter-overlay filter-overlay-profile" style="opacity: 0;"></div>
    </section>
@stop