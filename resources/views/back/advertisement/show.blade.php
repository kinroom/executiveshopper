@extends('back.template')

@section('title', "Advertisement")

@section('main')
    @include('back.partials.entete', ['title' => 'Advertisement', 'icone' => 'fa fa-line-chart', 'fil' => 'Advertisement'])

    <div>
        <p>Place: {{$advertisement->place}}</p>
    </div>
    <div>
        <p>Advertisement:</p>
        {!! $advertisement->code !!}
    </div>
@stop