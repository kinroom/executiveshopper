@extends('back.template')

@section('title', "Advertisement")

@section('main')

    @include('back.partials.entete', ['title' => 'Advertisement', 'icone' => 'fa fa-line-chart', 'fil' => 'Advertisement'])
    <p>
        {!! $advertisement->code !!}
    </p>
    {!! Form::open(['route' => ['admin.advertisement.update', $advertisement->id], 'method' => 'put']) !!}
    <div class="form-group">
        @if ($advertisement->place == 'main')
            {!! Form::label('place', 'Place:') !!}
            <div class="radio">
                <label>{!! Form::radio('place', 'main', 1) !!}Main</label>
            </div>
            <div class="radio">
                <label>{!! Form::radio('place', 'category') !!}Category</label>
            </div>
        @else
            {!! Form::label('place', 'Place:') !!}
            <div class="radio">
                <label>{!! Form::radio('place', 'main') !!}Main</label>
            </div>
            <div class="radio">
                <label>{!! Form::radio('place', 'category',1) !!}Category</label>
            </div>
        @endif
    </div>
    <div class="form-group">
        {!! Form::label('ads', 'Code:') !!}
        {!! Form::text('code', "$advertisement->code", ['class' => 'form-control', 'autocomplete' => 'off', 'required' => 'required']) !!}
    </div>
    <input type="submit" value="Update" class='btn btn-success'/>
    {!! Form::close() !!}

@stop