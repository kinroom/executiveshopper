@extends('back.template')

@section('title', "Advertisement")

@section('main')
    @include('back.partials.entete', ['title' => 'Advertisement', 'icone' => 'fa fa-line-chart', 'fil' => 'Advertisement'])
    {!! Form::open(['url' => route('admin.advertisement.store'), 'method' => 'post']) !!}
        <div class="form-group">
            {!! Form::label('place', 'Place:') !!}
            <div class="radio">
                <label>{!! Form::radio('place', 'main') !!}Main</label>
            </div>
            <div class="radio">
                <label>{!! Form::radio('place', 'category') !!}Category</label>
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('ads', 'Code:') !!}
            {!! Form::text('code', '', ['class' => 'form-control', 'autocomplete' => 'off', 'required' => 'required']) !!}
        </div>
        <input type="submit" value="Create" class='btn btn-success'/>
    {!! Form::close() !!}
@stop