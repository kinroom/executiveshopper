@extends('back.template')

@section('title', "Advertisement")

@section('main')
    @include('back.partials.entete', ['title' => 'Advertisement' . link_to_route('admin.advertisement.create', 'Advertisement add', [], ['class' => 'btn btn-info pull-right']), 'icone' => 'folder-open', 'fil' => 'Advertisement\Add'])

        <table class="table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Code</th>
                <th>Place</th>
                <th>See</th>
                <th>Update</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            @foreach($advertisements as $advertisement)
                <tr>
                    <td class="text-primary"><strong>{{ $advertisement->id}} </strong></td>
                    <td>{!! $advertisement->code !!}</td>
                    <td>{{$advertisement->place }}</td>
                    <td>{!! link_to_route("admin.advertisement.show",'See', [$advertisement->id] , ['class' => 'btn btn-success btn-block btn']) !!}</td>
                    <td>{!! link_to_route('admin.advertisement.edit', 'Edit', [$advertisement->id], ['class' => 'btn btn-warning btn-block']) !!}</td>
                    <td>
                        {!! Form::open(['method' => 'DELETE', 'route' => ['admin.advertisement.destroy', $advertisement->id]]) !!}
                        {!! Form::destroy('Destroy', 'Do you really want to delete it?') !!}
                        {!! Form::close() !!}
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
@stop