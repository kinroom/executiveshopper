@extends('back.template')

@section('title', "All posts")

@section('main')

    @include('back.partials.entete', ['title' => 'Translations', 'icone' => 'pencil', 'fil' => 'Posts/'.ucfirst($language)])

    @if(session()->has('ok'))
        @include('partials/error', ['type' => 'success', 'message' => session('ok')])
    @endif

    <div class="row col-lg-12">
        <div class="pull-right link">{!! $posts !!}</div>
    </div>

    <div class="row col-lg-12">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>
                        Title
                    </th>
                    <th>
                        Date
                    </th>
                    <th>
                        Translator
                    </th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($posts as $post)
                        <tr {!! !$post->seen && session('statut') == 'admin'? 'class="warning"' : '' !!}>
                            <td class="text-primary"><strong>{{ $post->title }}</strong></td>
                            <td>{{ $post->created_at }}</td>
                            <td>{{ $post->firstname . ' ' . $post->lastname }}</td>
                            <td>{!! link_to_route('articles.post.show', 'See', [$post->name, $post->slug], ['class' => 'btn btn-success btn-block btn']) !!}</td>
                            <td>{!! link_to_route('admin.trans.edit', 'Edit', $post->id, ['class' => 'btn btn-warning btn-block']) !!}</td>
                            <td>
                                {!! Form::open(['method' => 'DELETE', 'route' => ['admin.blog.destroy', $post->id]]) !!}
                                {!! Form::destroy('Destroy', 'Do you really want to delete it?') !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row col-lg-12">
        <div class="pull-right link">{!! $posts !!}</div>
    </div>

@stop
