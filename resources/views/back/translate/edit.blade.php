@extends('front.template')

@section('title', "Edit the post")

@section('main')
    <section class="fjob-list-section profile-page clearfix">
        <div class="profile-right clearfix">
            {!! Form::open(['route' => ['admin.trans.update', $post->id], 'method' => 'put', "class" => "propose-post-form", 'enctype'=>"multipart/form-data"]) !!}
            {!! Form::hidden('user_id', $post->user_id) !!}
            {!! Form::text('title', "$post->title", ['class' => 'block-input-profile block-input-profile-post-name', 'placeholder' => 'Name of the post', 'required' => 'required']) !!}
                <span class="write-post-label">Write your post</span>
                <div class="text-edit-container">
                    {!! Form::textarea('content', "$post->content", ['required' => 'required'] ) !!}
                </div>

                <span class="write-post-label">Write description for SEO:</span>
                <div class="text-edit-container">
                    {!! Form::textarea('description', "$post->description") !!}
                </div>

                <div class="post-fields">
                    {!! Form::label('keywords', 'Keywords') !!}
                    {!! Form::text('keywords', "$post->keywords", ['id' => 'keywords']) !!}
                </div>
                <div class="post-fields">
                    <div class="clearfix tagsinput">
                        {!! Form::hidden('tags', "$post->tags", ['id' => 'tags']) !!}
                    </div>
                    <div class="clearfix">
                        {!! Form::label('author', 'Author / Translator') !!}
                        {!! Form::text('author', "$post->author", ['id' => 'author']) !!}
                    </div>
                    <div class="clearfix">
                        {!! Form::label('photographer', 'Photographer') !!}
                        {!! Form::text('photographer', "$post->photographer", ['id' => 'photographer']) !!}
                    </div>
                    <div class="clearfix">
                        {!! Form::label('styling', 'Styling / Makeup') !!}
                        {!! Form::text('styling', "$post->styling", ['id' => 'styling']) !!}
                    </div>
                    <div class="clearfix">
                        {!! Form::label('outfit', 'Outfit') !!}
                        {!! Form::text('outfit', "$post->outfit", ['id' => 'outfit']) !!}
                    </div>
                    <div class="clearfix">
                        {!! Form::label('location', 'Location') !!}
                        {!! Form::text('location', "$post->location", ['id' => 'location']) !!}
                    </div>
                    <div class="clearfix post-fields-website">
                        {!! Form::label('website', 'Website') !!}
                        {!! Form::text('website', "$post->website", ['id' => 'website']) !!}
                    </div>
                </div>
                <div class="clearfix">
                    <input type="submit" value="Send post" class="send-post-button" id="send-post-button">
                </div>
                {!! Form::hidden('category_id', "$post->category_id", ['id' => 'category_id']) !!}
                {!! Form::hidden('subcategory', "$post->subcategory", ['id' => 'subcategory']) !!}
                {!! Form::close() !!}
            </div>
        </div>
        <div class="filter-overlay filter-overlay-profile" style="opacity: 0;"></div>
    </section>
@stop

@section('scripts')
    {!! HTML::script('ckeditor/ckeditor.js') !!}
    <script>
        var config = {
            language: '{{ config('app.locale') }}',
            height: 300,
            filebrowserBrowseUrl: '{!! url($url) !!}',
            toolbarGroups: [
                { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
                { name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
                { name: 'links' },
                { name: 'insert' },
                { name: 'forms' },
                { name: 'tools' },
                { name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
                { name: 'others' },
                //'/',
                { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
                { name: 'styles' },
                { name: 'colors' }
            ]
        };
        CKEDITOR.replace( 'content', config);
    </script>
@stop
