@extends('front.template')

@section('main')
    <section class="fjob-list-section profile-page clearfix">
        <div class="profile-right clearfix">
            {!! Form::open(['route' => ['admin.trans.store', $post->id], 'method' => 'post', "class" => "propose-post-form"]) !!}
                <span class="post-choose-category">Choose the language</span>
                <div class="post-categories-block">
                    @foreach($languages as $language)
                        @if ($language->id !== $post->language_id)
                            {!! Form::checkbox('language', $language->id, '', ['class' => 'checkbox-filter profile-checkbox post-main-category-checkbox', 'id' => "$language->name"]) !!}
                            {!! Form::label($language->name, strtoupper($language->name)) !!}
                        @endif
                    @endforeach
                    {!! Form::text('title', $post->title, ['class' => 'block-input-profile block-input-profile-post-name', 'placeholder' => trans('back/post_propose.post-name'), 'required' => 'required']) !!}

                    <span class="write-post-label">{{trans('back/post_propose.write-post')}}</span>
                    <div class="text-edit-container">
                        {!! Form::textarea('content', $post->content, ['required' => 'required'] ) !!}
                    </div>
                        <span class="write-post-label">Write description for SEO:</span>
                        <div class="text-edit-container">
                            {!! Form::textarea('description', "$post->description") !!}
                        </div>

                        <div class="post-fields">
                            {!! Form::label('keywords', 'Keywords') !!}
                            {!! Form::text('keywords', "$post->keywords", ['id' => 'keywords']) !!}
                        </div>
                    <div class="post-fields">
                        <div class="clearfix tagsinput">
                            {!! Form::label('tags', trans('back/post_propose.tags')) !!}
                            {!! Form::text('tags', '', ['id' => 'tags']) !!}
                        </div>
                        <div class="clearfix">
                            <input type="submit" value="{{trans('back/post_propose.send-post')}}" class="send-post-button" id="send-post-button">
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
        <div class="filter-overlay filter-overlay-profile" style="opacity: 0;"></div>
    </section>
@stop

@section('scripts')



    {!! HTML::script('ckeditor/ckeditor.js') !!}
        <script>
            var config = {
                language: '{{ config('app.locale') }}',
                height: 300,
                toolbarGroups: [
                    { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
                    { name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
                    { name: 'links' },
                    { name: 'insert' },
                    { name: 'forms' },
                    { name: 'tools' },
                    { name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
                    { name: 'others' },
                    //'/',
                    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                    { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
                    { name: 'styles' },
                    { name: 'colors' }
                ]
            };
            CKEDITOR.replace( 'content', config);
        </script>
@stop