<div class="modal fade login-main-modal modal-ajax" tabindex="-1" role="dialog" id="modal-delete-post">  <!-- MODAL DELETE POST -->
    <div class="modal-dialog modal-md">
        <div class="modal-content modal-login-content">
            <h4>{{trans('back/my-vacancies.delete-vacancy')}}</h4>
            {!! Form::open(['method' => 'post','url' => ['/' . App::getLocale() . '/vacancy-delete/']]) !!}
            {!! Form::hidden('id','',['id' => 'id']) !!}
            <input value='{{trans('back/my-posts.yes')}}' type="submit" class="delete-company-modal-buttons delete-company-modal-buttons-yes"/>
            <button class="delete-company-modal-buttons delete-company-modal-buttons-no" type="button" data-dismiss="modal">{{trans('back/my-posts.no')}}</button>
            <button class="close-modal-button" type="button" data-dismiss="modal"><img src="/img/icons/close-icon.svg" alt="Close modal"></button>
            {!! Form::close() !!}
        </div>
    </div>
</div>
