@extends('front.template')

@section('title', trans('back/my-vacancies.my-vacancies'))

@section('main')
    <section class="fjob-list-section profile-page clearfix">
        @include('back.profile.profile-left')
        @if (count($vacancies) > 0 && count($company_exist) > 0)
            <div class="profile-right profile-right-myposts clearfix">
                <div class="clearfix">
                    <a href="/{{App::getLocale()}}/fashion-job/create/" class="noposts-button myposts-add-button myvacancies-add-button">{{trans('back/my-vacancies.add-vacancy')}}</a>
                </div>
                <ul class="vacancies-list" id="articles">
                    @foreach($vacancies as $key => $vacancy)
                        <li class="clearfix">
                            <div class="clearfix">
                                <div class="vacancies-left">
                                    @if ($companies[$key]['logo'] != '' && file_exists(base_path() .'/public/img/uploads/companies/' . $companies[$key]['user_id'] . '/' . $companies[$key]['id'] . '/' . $companies[$key]['logo']))
                                        <img src="/img/uploads/companies/{{$companies[$key]['user_id']}}/{{$companies[$key]['id']}}/{{$companies[$key]['logo']}}" alt="{{$companies[$key]['name']}}">
                                    @else
                                        <img src="/img/company_default.png">
                                    @endif
                                </div>
                                <div class="vacancies-right clearfix">
                                    <div class="job-heading">
                                        <h2>
                                            @if ($vacancy->is_active == 1)
                                                <a href="/{{App::getLocale()}}/fashion-job/{{$vacancy->id}}/">{{$vacancy->title}}</a>
                                            @else
                                                <a href="#">{{$vacancy->title}}</a>
                                            @endif
                                        </h2>
                                        <span class="job-company-name">{{$companies[$key]['name']}}</span>
                                        <span class="job-date">{!!  date('d.m.Y', strtotime($vacancy->created_at)) !!}</span>
                                        <span class="delete-post-button" data-toggle="modal" data-target="#modal-delete-post" id="{{$vacancy->id}}"></span>
                                    </div>
                                    <div class="job-text">
                                        <p>{{$vacancy->description}}</p>
                                    </div>
                                    <div class="vacancy-bottom clearfix">
                                        <div class="clearfix">
                                            <div class="vacancy-bottom-left">
                                                <span>{{trans('back/my-vacancies.salary')}}:</span>
                                            </div>
                                            <div class="vacancy-bottom-right">
                                                <span>{{$vacancy->salary}}</span>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                            <div class="vacancy-bottom-left">
                                                <span>{{trans('back/my-vacancies.availability')}}:</span>
                                            </div>
                                            <div class="vacancy-bottom-right">
                                                <span>{{$vacancy->availability_id}}</span>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                            <div class="vacancy-bottom-left">
                                                <span>{{trans('back/my-vacancies.location')}}:</span>
                                            </div>
                                            <div class="vacancy-bottom-right">
                                                <span>{{$vacancy->country}}</span>
                                                <span class="vacancy-bottom-city">{{$vacancy->city}}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
                @if (isset($more_vacancies) && count($more_vacancies) > 0)
                    <button class="more-posts more-vacancies" id='1+{{$vacancy->user_id}}' onclick="profileVacancies(this.id)"> <!-- SHOW MORE BUTTON -->
                        <span>{{trans('back/my-vacancies.show-more')}}</span>
                        <i class="fa fa-angle-down show-more-icon"></i>
                    </button>
                @endif
            </div>
        @elseif (isset($company_exist) && count($company_exist) == 0)
            <div class="profile-right profile-right-vacancies-addcompany clearfix">
                <div class="clearfix">
                    <h2>{{trans('back/my-vacancies.first-create-company')}}</h2>
                </div>
                <a href="/{{App::getLocale()}}/profile/" class="noposts-button novacancy-button vacancies-addcompany-button">{{trans('back/my-vacancies.add-company')}}</a>
            </div>
        @else
            <div class="profile-right profile-right-noposts clearfix">
                <h2>{{trans('back/my-vacancies.you-havent-posted')}}</h2>
                <a href="/{{App::getLocale()}}/fashion-job/create/" class="noposts-button novacancy-button">{{trans('back/my-vacancies.add-vacancy')}}</a>
            </div>
        @endif
        <div class="filter-overlay filter-overlay-profile" style="opacity: 0;"></div>
    </section>
@stop

@section('footer')
    @include('back.profile.delete-vacancy')
@stop