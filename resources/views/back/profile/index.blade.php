@extends('front.template')

@section('title', trans('back/profile.my-profile'))

@section('main')
    <section class="fjob-list-section profile-page clearfix">
        @if ($role !== 'user')
            @include('back.profile.profile-left')
        @endif
        <div class="profile-right clearfix">
            {!! Form::open(['url' => 'profile', 'id' => 'profile', 'method' => 'post', 'class' => 'clearfix', 'enctype'=>"multipart/form-data"]) !!}
                {!! Form::hidden('id', $profile['id']) !!}
            <h1></h1>
                <div class="profile-photo-block">
                    <div class="profile-photo notempty" id="image-holder">
                        @if ($profile['avatar'] !== '' && $profile['service'] == 'site' && file_exists(base_path() .'/public/img/uploads/profiles/' . $profile['id'] . '/' . $profile['avatar']))
                            <img src="/img/uploads/profiles/{{$profile['id']}}/{{$profile['avatar']}}" />
                        @elseif ($profile['avatar'] !== '' && $profile['service'] !== 'site')
                            <img src="{{$profile['avatar']}}" />
                        @else
                            <img src="/img/avatar.gif" />
                        @endif
                    </div>
                    {!! Form::file('photo',['id' => 'fileUpload'])!!}
                    <span>{{ trans('back/profile.recommended-size') }}</span>
                    <span>340 x 340 px</span>
                    <span>{{ trans('back/profile.size-weight') }} 100 kb</span>
                </div>
                <div class="profile-user-block">
                    {!! Form::text('name', $profile['firstname'], ['class' => 'block-input-profile', 'placeholder' => trans('back/profile.my-name')]) !!}
                    {!! Form::text('surname', $profile['lastname'], ['class' => 'block-input-profile', 'placeholder' => trans('back/profile.my-surname')]) !!}
                    <div class="clearfix">
                        <div class="profile-form-one-col">
                            {!! Form::email('email', $profile['contact_email'], ['class' => 'block-input-profile', 'placeholder' => trans('back/profile.my-email')]) !!}
                            {!! Form::checkbox('hide_email', 1, $profile['hide_email'], ['class' => 'checkbox-filter profile-checkbox', 'id' => 'hide_email']) !!}
                            {!! Form::label('hide_email',trans('back/profile.hide-email')) !!}
                        </div>
                        <div class="profile-form-one-col profile-form-one-col-gender">
                            <span>{{trans('back/profile.what-gender')}}</span>
                            @if ($profile['gender'] == 0)
                                {!! Form::checkbox('user-male', 0, 1, ['class' => 'checkbox-filter profile-checkbox', 'id' => 'user-male']) !!}
                                {!! Form::label('user-male',trans('back/profile.male')) !!}
                                {!! Form::checkbox('user-female', 0, 0, ['class' => 'checkbox-filter profile-checkbox', 'id' => 'user-female']) !!}
                                {!! Form::label('user-female',trans('back/profile.female')) !!}
                            @else
                                {!! Form::checkbox('user-male', 0, 0, ['class' => 'checkbox-filter profile-checkbox', 'id' => 'user-male']) !!}
                                {!! Form::label('user-male',trans('back/profile.male')) !!}
                                {!! Form::checkbox('user-female', 0, 1, ['class' => 'checkbox-filter profile-checkbox', 'id' => 'user-female']) !!}
                                {!! Form::label('user-female',trans('back/profile.female')) !!}
                            @endif

                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="profile-form-one-col">
                            {!! Form::tel('number', $profile['number'], ['placeholder' => trans('back/profile.what-telephone'), 'pattern' => '[0-9]{0,20}', 'title' => trans('back/profile.only-numbers')]) !!}
                            {!! Form::checkbox('hide_number', 1, $profile['hide_number'], ['class' => 'checkbox-filter profile-checkbox', 'id' => 'hide_number']) !!}
                            {!! Form::label('hide_number', trans('back/profile.hide-phone')) !!}
                        </div>
                        <div class="profile-form-one-col">
                            {!! Form::text('birthday', $profile['birthday'], ['placeholder' => trans('back/profile.what-birthday'), 'pattern' => '\d{1,2}.\d{1,2}.\d{4}', 'title' => 'dd.mm.yyyy']) !!}
                            {!! Form::checkbox('hide_birthday', 1, $profile['hide_birthday'], ['class' => 'checkbox-filter profile-checkbox', 'id' => 'hide_birthday']) !!}
                            {!! Form::label('hide_birthday',trans('back/profile.hide-birth')) !!}
                        </div>
                    </div>
                    {!! Form::text('site', $profile['site'], ['class' => 'block-input-profile', 'placeholder' => trans('back/profile.my-site')]) !!}
                    {!! Form::button(trans('back/profile.save-changes'), ['type' => 'submit', 'class' => 'profile-user-submit-button', 'data-toggle' => 'modal', 'data-targer' => '.saved-modal']) !!}
                </div>
            {!! Form::close() !!}

            @if ($role == 'employer' || $role == 'redac' || $role == 'admin')
                <div class="clearfix">
                    @foreach ($companies as $key=>$company)
                        <?php $key++; ?>
                        {!! Form::open(['url' => 'company', 'method' => 'post', "class" => "clearfix company-profile-form company-profile-form$key company-visible", 'enctype'=>"multipart/form-data", "name"=>"company".$key]) !!}
                        {!! Form::hidden('user_id', $profile['id']) !!}
                        {!! Form::hidden('company_id', $company['id'], ['id' => 'company_id'])!!}
                        <div class="profile-user-block profile-company-block">
                            {!! Form::text("name", $company['name'], ['class' => 'block-input-profile block-input-profile-company-name', 'placeholder' => trans('back/profile.company-name'), 'id' => 'name', 'required' => 'required']) !!}
                            <div class="profile-photo logo-photo notempty" id="logo-holder{{$key}}">
                                @if ($company['logo'] !== '' && file_exists(base_path() .'/public/img/uploads/companies/' . $profile['id'] . '/' . $company['id'] . '/' . $company['logo']))
                                    <img src="/img/uploads/companies/{{$profile['id']}}/{{$company['id']}}/{{$company['logo']}}" />
                                @else
                                    <img src="/img/company_default.png" />
                                @endif
                            </div>
                            <span>{{trans('back/profile.load-logotype')}}</span>
                            <span class="load-logo-span">{{trans('back/profile.recommended-size')}} 150 x 150 px</span>
                            <span class="load-logo-span">{{trans('back/profile.size-weight')}} 100 kb</span>
                            {!! Form::file('logo',["id" => "loadLogo$key"])!!}
                            {!! Form::textarea('description',$company['description'],['class' => 'company-description-profile', 'id' => 'company-description', 'cols' => '30', 'rows' => '9', 'placeholder' => trans('back/profile.company-describe'), 'required' => 'required' ])!!}
                            {!! Form::text('website', $company['site'], ['class' => 'block-input-profile', 'placeholder' => trans('back/profile.company-website')]) !!}
                            {!! Form::email('email', $company['email'], ['class' => 'block-input-profile', 'placeholder' => trans('back/profile.email')]) !!}
                            <?php $numbers = explode(',',$company['number']); ?>
                            @for ($i = 0; $i < count($numbers); $i++)
                                {!! Form::tel("number$i", $numbers[$i], ['class' => 'block-input-profile tel-input-profile tel-visible', 'placeholder' => trans('back/profile.telephone-number'), 'pattern' => '[0-9]{6,14}', 'title' => trans('back/profile.only-numbers')]) !!}
                            @endfor
                            @for ($i = count($numbers); $i < 3; $i++)
                                {!! Form::tel("number$i", '', ['class' => 'block-input-profile tel-notvisible tel-input-profile', 'placeholder' => trans('back/profile.telephone-number'), 'pattern' => '[0-9]{6,14}', 'title' => trans('back/profile.only-numbers')]) !!}
                            @endfor

                            @if (count($numbers) < 3)
                                <span class="add-tel-number">+ {{ trans('back/profile.add-number')}}</span>
                            @endif
                            <div class="clearfix company-buttons-block">
                                <input class="profile-user-submit-button company-save-button company{{$key}}-save-button" type="submit" value="{{ trans('back/profile.save-changes')}}">
                                <button type="reset" class="profile-user-submit-button delete-company-button delete-company{{$key}}-button">{{ trans('back/profile.company-delete')}}</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    @endforeach

                    @for ($i = count($companies) + 1; $i < 6; $i++)
                        @if ($i == 1)
                            {!! Form::open(['url' => 'company', 'method' => 'post', "class" => "clearfix company-profile-form company-profile-form$i company-visible", 'enctype'=>"multipart/form-data", "name"=>"company".$i]) !!}
                        @else
                            {!! Form::open(['url' => 'company', 'method' => 'post', "class" => "clearfix company-profile-form company-profile-form$i company-notvisible", 'enctype'=>"multipart/form-data","name"=>"company".$i]) !!}
                        @endif

                            {!! Form::hidden('user_id', $profile['id']) !!}
                            {!! Form::hidden('company_id', '', ['id' => 'company_id'])!!}

                                <div class="profile-user-block profile-company-block">
                                    {!! Form::text("name", '', ['class' => 'block-input-profile block-input-profile-company-name', 'placeholder' => trans('back/profile.company-name'), 'required' => 'required']) !!}
                                    <div class="profile-photo logo-photo notempty" id="logo-holder{{$i}}">
                                        <img src="/img/company_default.png" alt="Company name">
                                    </div>
                                    <span>{{trans('back/profile.load-logotype')}}</span>
                                    <span class="load-logo-span">{{trans('back/profile.recommended-size')}} 150 x 150 px</span>
                                    <span class="load-logo-span">{{trans('back/profile.size-weight')}} 100 kb</span>
                                    {!! Form::file('logo',["id" => "loadLogo$i"])!!}
                                    {!! Form::textarea('description','',['class' => 'company-description-profile', 'id' => 'company-description', 'cols' => '30', 'rows' => '9', 'placeholder' => trans('back/profile.company-describe'), 'required' => 'required' ])!!}
                                    {!! Form::text('website', '', ['class' => 'block-input-profile', 'placeholder' => 'Corporate website']) !!}
                                    {!! Form::email('email', '', ['class' => 'block-input-profile', 'placeholder' => 'Email', 'required' => 'required']) !!}
                                    {!! Form::tel('number0', '', ['class' => 'block-input-profile tel-input-profile tel-visible', 'placeholder' => trans('back/profile.telephone-number'), 'pattern' => '[0-9]{6,14}', 'title' => trans('back/profile.only-numbers')]) !!}
                                    {!! Form::tel('number2', '', ['class' => 'block-input-profile tel-notvisible tel-input-profile', 'placeholder' => trans('back/profile.telephone-number'), 'pattern' => '[0-9]{6,14}', 'title' => trans('back/profile.only-numbers')]) !!}
                                    {!! Form::tel('number3', '', ['class' => 'block-input-profile tel-notvisible tel-input-profile', 'placeholder' => trans('back/profile.telephone-number'), 'pattern' => '[0-9]{6,14}', 'title' => trans('back/profile.only-numbers')]) !!}
                                    <span class="add-tel-number">+ Add telephone number</span>
                                    <div class="clearfix company-buttons-block">
                                        <input class="profile-user-submit-button company-save-button company{{$i}}-save-button" type="submit" value="{{ trans('back/profile.save-changes')}}">
                                        <button type="reset" class="profile-user-submit-button delete-company-button delete-company{{$i}}-button">{{ trans('back/profile.company-delete')}}</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                    @endfor
                        @if (count($companies) < 5)
                            <div class="clearfix">
                                <div class="profile-user-block profile-company-block add-one-more-company-button-wrapper">
                                    <span class="add-one-more-company-button">{{ trans('back/profile.add-company')}}</span>
                                </div>
                            </div>
                        @endif
                </div>
            @endif
        <div class="filter-overlay filter-overlay-profile" style="opacity: 0;"></div>
        </div>
    </section>
@stop

@section('footer')
    <div class="modal fade login-main-modal modal-ajax" tabindex="-1" role="dialog" aria-labelledby="savedChanges" id="saved-modal">  <!-- MODAL SAVED -->
        <div class="modal-dialog modal-sm">
            <div class="modal-content modal-login-content">
                <h4>{{ trans('back/profile.successful')}}</h4>
                <p>{{ trans('back/profile.saved-company')}}</p>
                <button class="close-modal-button" type="button" data-dismiss="modal"><img src="{{asset('img/icons/close-icon.svg')}}" alt="Close modal"></button>
            </div>
        </div>
    </div>
    <div class="modal fade login-main-modal modal-ajax" tabindex="-1" role="dialog" aria-labelledby="savedChanges" id="saved-company-modal">  <!-- MODAL SAVED -->
        <div class="modal-dialog modal-sm">
            <div class="modal-content modal-login-content">
                <h4>{{ trans('back/profile.successful')}}</h4>
                <p>{{ trans('back/profile.saved-company')}}</p>
                <button class="close-modal-button" type="button" data-dismiss="modal"><img src="{{asset('img/icons/close-icon.svg')}}" alt="Close modal"></button>
            </div>
        </div>
    </div>
    @for ($i = 1; $i < 6; $i++)
        <div class="modal fade login-main-modal modal-ajax" tabindex="-1" role="dialog" aria-labelledby="savedChanges" id="delete-company{{$i}}-modal">  <!-- MODAL COMPANY SAVED -->
            <div class="modal-dialog modal-md">
                <div class="modal-content modal-login-content">
                    <h4>{{ trans('back/profile.company-deleting')}}</h4>
                    <button class="delete-company-modal-buttons delete-company-modal-buttons-yes"  id="delete-companies{{$i}}" type="button" data-dismiss="modal">{{ trans('back/profile.yes')}}</button>
                    <button class="delete-company-modal-buttons delete-company-modal-buttons-no" type="button" data-dismiss="modal">{{ trans('back/profile.no')}}</button>
                    <button class="close-modal-button" type="button" data-dismiss="modal"><img src="{{asset('img/icons/close-icon.svg')}}" alt="Close modal"></button>
                </div>
            </div>
        </div>
    @endfor
    <div class="modal fade login-main-modal modal-ajax" tabindex="-1" role="dialog" aria-labelledby="savedChanges" id="image-warning-modal">  <!-- MODAL SAVED -->
        <div class="modal-dialog modal-sm">
            <div class="modal-content modal-login-content">
                <h4>{{ trans('back/profile.info')}}</h4>
                <p>{{ trans('back/profile.image-require')}}</p>
                <button class="close-modal-button" type="button" data-dismiss="modal"><img src="{{asset('img/icons/close-icon.svg')}}" alt="Close modal"></button>
            </div>
        </div>
    </div>
@stop
