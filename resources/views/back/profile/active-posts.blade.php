@extends('front.template')

@section('title', trans('back/my-posts.active-posts'))

@section('main')
    <section class="fjob-list-section profile-page clearfix">
        @include('back.profile.profile-left')
            @if (count($active_posts) > 0)
            <div class="profile-right profile-right-myposts clearfix">
                <div class="clearfix">
                    <a href="{{ route('profile.post.create') }}" class="noposts-button myposts-add-button">{{trans('back/my-posts.add-post')}}</a>
                </div>
                <ul class="myposts-items-list" id="articles">
                        @foreach($active_posts as $post)
                        @if($post->active == 1 && $post->active_seen == 0)
                            <li class="clearfix myposts-new-item">
                        @else
                            <li class="clearfix">
                                @endif
                                <div class="myposts-item-preview-left">
                                    @if ($post->thumb != '' && file_exists(base_path() .'/public/img/uploads/thumbs/first/' . $post->user_id . '/' . $post->thumb))
                                        <img src="/img/uploads/thumbs/first/{{$post->user_id}}/{{$post->thumb}}" alt="{{$post->title}}">
                                    @else
                                        <img src="/img/post-default-mini.jpg" alt="{{$post->title}}">
                                    @endif
                                </div>
                                <div class="myposts-item-right">
                                    <h2><a href="{{ route('profile.post.activate', $post->id) }}">{{ $post->title }}</a></h2>
                                    <span class="myposts-date">{{date('d.m.Y', strtotime($post->created_at))}}</span>
                                    <span class="delete-post-button" data-toggle="modal" data-target="#modal-delete-post" id="{{$post->id}}"></span>
                                </div>
                            </li>
                        @endforeach
                </ul>
                @if (isset($more_posts) && count($more_posts) > 0)
                    <button class="more-posts" id='1+{{$post->user_id}}+active_posts+{{$count}}' name="more-posts" onclick="profilePosts(this.id)"> <!-- SHOW MORE BUTTON -->
                        <span>{{trans('back/my-posts.show-more')}}</span>
                        <i class="fa fa-angle-down show-more-icon"></i>
                    </button>
                @endif
            </div>
        @else
            <div class="profile-right profile-right-noposts clearfix">
                <h2>{{trans('back/my-posts.have-no-posts')}}</h2>
                <a href="/{{App::getLocale()}}/blog/create/" class="noposts-button">{{trans('back/my-posts.add-post')}}</a>
            </div>
        @endif
        <div class="filter-overlay filter-overlay-profile" style="opacity: 0;"></div>
    </section>
@stop

@section('footer')
    @include('back.profile.delete-post')
@stop


