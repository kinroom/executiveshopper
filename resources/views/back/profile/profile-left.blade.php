<?php $actual_link = $_SERVER['REQUEST_URI']; ?>
<div class="profile-left">
    <nav>
        <ul class="profile-menu">
            <li @if (stripos($actual_link, '/profile') && (strlen($actual_link) <= 12)) class="active-profile-menu" @endif >
                <a href="/profile/">{{trans('back/profile-nav.profile-settings')}}</a>
            </li>
            @if ($role == 'blogger' || $role == 'redac' || $role == 'admin')
                <li @if (stripos($actual_link, 'post/create')) class="active-profile-menu" @endif >
                    <a href="{{ route('profile.post.create') }}">{{trans('back/profile-nav.post-propose')}}</a>
                </li>
                @if (stripos($actual_link, 'posts'))
                    <li @if (stripos($actual_link, '/profile/posts') || stripos($actual_link, 'profile/active-posts') || stripos($actual_link, 'profile/approving-posts')) class="active-profile-menu" @endif>
                        <a href="/profile/posts/">{{trans('back/profile-nav.my-posts')}}<span class="profile-notification">{{ $count }}</span></a>
                        <ul class="my-posts-list">
                            <li>
                                <a href="/profile/active-posts/" @if (stripos($actual_link, 'profile/active-posts')) class="my-posts-active-menu" @endif>{{trans('back/profile-nav.posts-active')}}</a>
                            </li>
                            <li><a href="/profile/approving-posts/" @if (stripos($actual_link, 'profile/approving-posts')) class="my-posts-active-menu" @endif>{{trans('back/profile-nav.posts-approving')}}</a></li>
                        </ul>
                    </li>
                @else
                    <li>
                        <a href="/profile/posts/">{{trans('back/profile-nav.my-posts')}}<span class="profile-notification">{{ $count }}</span></a>
                    </li>
                @endif
            @endif
            @if ($role == 'employer' || $role == 'redac' || $role == 'admin')
                <li @if (stripos($actual_link, 'profile/vacancies') || stripos($actual_link, 'fashion-job')) class="active-profile-menu" @endif>
                    <a href="/profile/vacancies/">{{trans('back/profile-nav.my-vacancies')}}</a>
                </li>
            @endif
            <li>
                <a href="/auth/logout">{{trans('back/profile-nav.logout')}}</a>
            </li>
            <li class="active-label"></li>
        </ul>
    </nav>
    <span class="profile-menu-label profile-menu-label-closed">
        <i class="fa fa-bars"></i>
        <span>Profile menu</span>
    </span>
</div>