@extends('back.template')

@section('title', "Shop category")

@section('main')
    @include('back.partials.entete', ['title' => 'Product category' . link_to_route('product_category.create', 'Add', [], ['class' => 'btn btn-info pull-right']), 'icone' => 'fa fa-line-chart', 'fil' => 'Category'])

    <table class="table">
        <thead>
        <tr>
            <th>Name</th>
            <th>Update</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>

        @foreach($roots as $category)
            <tr>
                <td><b>{{ $category->name }}</b></td>
                <td>{!! link_to_route('product_category.edit', 'Edit', ['id' => $category->id], ['class' => 'btn btn-warning btn-block']) !!}</td>
                <td>
                    {!! Form::open(['method' => 'DELETE', 'url' => route('product_category.destroy', ['id' => $category->id])]) !!}
                    {!! Form::destroy('Destroy', 'Destroy') !!}
                    {!! Form::close() !!}
                </td>
            </tr>
                @if (count($category->children) > 0)
                    @foreach($category->children as $subCategory)
                        <tr>
                            <td> - {{ $subCategory->name }}</td>
                            <td>{!! link_to_route('product_category.edit', 'Edit', ['id' => $subCategory->id], ['class' => 'btn btn-warning btn-block']) !!}</td>
                            <td>
                                {!! Form::open(['method' => 'DELETE', 'url' => route('product_category.destroy', ['id' => $subCategory->id])]) !!}
                                {!! Form::destroy('Destroy', 'Destroy') !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @foreach($subCategory->children as $child)
                            <tr>
                                <td> -- {{ $child->name }}</td>
                                <td>{!! link_to_route('product_category.edit', 'Edit', ['id' => $child->id], ['class' => 'btn btn-warning btn-block']) !!}</td>
                                <td>
                                    {!! Form::open(['method' => 'DELETE', 'url' => route('product_category.destroy', ['id' => $child->id])]) !!}
                                    {!! Form::destroy('Destroy', 'Destroy') !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach

                    @endforeach
                @endif

        @endforeach

        </tbody>
    </table>
@stop