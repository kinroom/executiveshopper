@extends('back.template')

@section('title', "Product category - create")

@section('main')
    @include('back.partials.entete', ['title' => 'Product category', 'icone' => 'fa fa-line-chart', 'fil' => 'Create'])
    {!! Form::open(['url' => route('product_category.store'), 'method' => 'post']) !!}
        <div class="form-group">
            {!! Form::label('parent', 'Parent category:') !!}
            <select name="parent_id" id="category" class="selectpicker show-tick form-control">
                <option value=""></option>
                @foreach($roots as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @if (count($category->children) > 0)
                            @foreach($category->children as $subCategory)
                                <option value="{{ $subCategory->id }}"> - {{ $subCategory->name }}</option>
                                @foreach($subCategory->children as $child)
                                    <option value="{{ $child->id }}"> -- {{ $child->name }}</option>
                                @endforeach
                            @endforeach
                        @endif
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <div class="form-group">
                {!! Form::label('name', 'Name:') !!}
                {!! Form::text('name', '', ['class' => 'form-control', 'required' => 'required']) !!}
            </div>
        </div>
        <div class="form-group">
            <div class="form-group">
                {!! Form::label('description', 'Description:') !!}
                {!! Form::textarea('description', '', ['class' => 'form-control', 'required' => 'required']) !!}
            </div>
        </div>
        <div class="form-group">
            <div class="form-group">
                {!! Form::label('keywords', 'Keywords:') !!}
                {!! Form::text('keywords', '', ['class' => 'form-control', 'required' => 'required']) !!}
            </div>
        </div>
        <input type="submit" value="Create" class='btn btn-success'/>
    {!! Form::close() !!}
@stop