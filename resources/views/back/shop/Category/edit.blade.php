@extends('back.template')

@section('title', "Product category - edit")

@section('main')
    @include('back.partials.entete', ['title' => 'Product category', 'icone' => 'fa fa-line-chart', 'fil' => 'Edit'])
    {!! Form::open(['url' => route('product_category.update', ['id' => $category->id]), 'method' => 'put']) !!}
        <div class="form-group">
            <div class="form-group">
                {!! Form::label('parent', 'Parent category:') !!}
                <select name="parent_id" id="category" class="selectpicker show-tick form-control">
                    <option value=""></option>
                    @foreach($roots as $parent)
                        <option value="{{ $parent->id }}" @if ($category->parent_id == $parent->id) selected @endif>{{ $parent->name }}</option>
                        @if (count($parent->children) > 0)
                            @foreach($parent->children as $subCategory)
                                <option value="{{ $subCategory->id }}" @if ($category->parent_id == $subCategory->id) selected @endif> - {{ $subCategory->name }}</option>
                                @foreach($subCategory->children as $child)
                                    <option value="{{ $child->id }}" @if ($category->parent_id == $child->id) selected @endif> -- {{ $child->name }}</option>
                                @endforeach
                            @endforeach
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="form-group">
                {!! Form::label('name', 'Name:') !!}
                {!! Form::text('name', $category->name, ['class' => 'form-control', 'required' => 'required']) !!}
            </div>
        </div>
        <div class="form-group">
            <div class="form-group">
                {!! Form::label('description', 'Description:') !!}
                {!! Form::textarea('description', $category->description, ['class' => 'form-control', 'required' => 'required']) !!}
            </div>
        </div>
        <div class="form-group">
            <div class="form-group">
                {!! Form::label('keywords', 'Keywords:') !!}
                {!! Form::text('keywords', $category->keywords, ['class' => 'form-control', 'required' => 'required']) !!}
            </div>
        </div>
        <input type="submit" value="Update" class='btn btn-success'/>
    {!! Form::close() !!}
@stop