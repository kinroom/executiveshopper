@extends('back.template')

@section('title', "Shop category")

@section('main')
    @include('back.partials.entete', ['title' => 'Product' . link_to_route('product.create', 'Add', [], ['class' => 'btn btn-info pull-right']), 'icone' => 'fa fa-line-chart', 'fil' => 'Item'])

    <table class="table">
        <thead>
        <tr>
            <th>Name</th>
            <th>Show</th>
            <th>Update</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($products as $key => $product)
            <tr>
                <td>{{ $product->name }}</td>
                <td>
                    @if ($product->categories()->first())
                        {!! link_to_route('product.show', 'See', ['hierarchy' => $product->treeLink($product), 'slug' => $product->slug], ['class' => 'btn btn-success btn-block btn']) !!}
                    @endif
                </td>
                <td>{!! link_to_route('product.edit', 'Edit', ['id' => $product->id], ['class' => 'btn btn-warning btn-block']) !!}</td>
                <td>
                    {!! Form::open(['method' => 'DELETE', 'url' => route('product.destroy', ['id' => $product->id])]) !!}
                        {!! Form::destroy('Destroy', 'Destroy') !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {!! $products->render() !!}
@stop