@extends('back.template')

@section('title', $product->name . " - edit")

@section('main')
    @include('back.partials.entete', ['title' => $product->name, 'icone' => 'fa fa-line-chart', 'fil' => 'Edit'])
    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach($errors->all() as $error)
                <li>{{  $error }}</li>
            @endforeach
        </ul>
    @endif
    {!! Form::open(['url' => route('product.update', ['id' => $product->id]), 'method' => 'put', 'enctype' => "multipart/form-data"]) !!}
    <div class="form-group">
        <label for="category" class="control-label">Category:</label>

        <select name="category[]" id="category" class="selectpicker show-tick form-control" multiple>
            @foreach($roots as $category)
                <optgroup label="{{ $category->name }}">
                    @if (count($category->children) > 0)
                        @foreach($category->children as $subCategory)
                            <option value="{{ $subCategory->id }}" @if (in_array($subCategory->id, $categoriesIds)) selected @endif>{{ $subCategory->name }}</option>
                            @foreach($subCategory->children as $child)
                                <option value="{{ $child->id }}" @if (in_array($child->id, $categoriesIds)) selected @endif> - {{ $child->name }}</option>
                            @endforeach
                        @endforeach
                    @endif
                </optgroup>
            @endforeach
        </select>
 </div>
 <div class="form-group">
     {!! Form::label('name', 'Name:') !!}
     {!! Form::text('name', $product->name, ['class' => 'form-control', 'required' => 'required']) !!}
 </div>
 <div class="form-group">
     {!! Form::label('thumb', 'Main image:') !!}
     {!! Form::file('thumb', ['id' => 'product_thumb'])!!}
 </div>

 <div class="form-group">
     {!! Form::label('images[]', 'Product images:') !!}
     <input id="product-images" name="images[]" type="file" class="file" multiple data-min-file-count="1">
 </div>
 <div class="form-group">
     {!! Form::label('price', 'Price:') !!}
     {!! Form::text('price', $product->price, ['class' => 'form-control', 'required' => 'required']) !!}
 </div>
 <div class="form-group" id="colours">
     {!! Form::label('colours', 'Colours:') !!}
     @if ($colors = old('colours'))
         @foreach($colors as $key => $color)
             {!! Form::text('colours[' . $key .']', $color, ['class' => 'pick-a-color form-control', 'id' => 'color' . $key]) !!}
         @endforeach
     @else
         @foreach(explode(',', $product->colours) as $key => $color)
             {!! Form::text('colours[]', $color, ['class' => 'pick-a-color form-control', 'id' => 'color' . $key]) !!}
         @endforeach
     @endif
 </div>
 <div class="form-group">
     {!! Form::label('sizes', 'Sizes:') !!}
     (Sizes must be separated by one comma and one space. Example: S, L, M)
     {!! Form::text('sizes', $product->sizes, ['class' => 'form-control', 'required' => 'required']) !!}
 </div>
 <div class="form-group">
     {!! Form::label('video', 'Video:') !!}
     {!! Form::textarea('video', $product->video, ['class' => 'form-control', 'required' => 'required']) !!}
 </div>
 <div class="form-group">
     {!! Form::label('size_fit', 'Size & Fit:') !!}
     {!! Form::textarea('size_fit', $product->size_fit, ['class' => 'form-control', 'required' => 'required']) !!}
 </div>
 <div class="form-group">
     {!! Form::label('details', 'Details:') !!}
     {!! Form::textarea('details', $product->details, ['class' => 'form-control', 'required' => 'required']) !!}
 </div>
 <div class="form-group">
     {!! Form::label('material_cafe', 'Material & Cafe:') !!}
     {!! Form::textarea('material_cafe', $product->material_cafe, ['class' => 'form-control', 'required' => 'required']) !!}
 </div>
 <div class="form-group">
     {!! Form::label('delivery', 'Free delivery:') !!}
     {!! Form::textarea('delivery', $product->delivery, ['class' => 'form-control', 'required' => 'required']) !!}
 </div>
    <div class="form-group">
        <div class="form-group">
            {!! Form::label('description', 'Description:') !!}
            {!! Form::textarea('description', $product->description, ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="form-group">
            {!! Form::label('keywords', 'Keywords:') !!}
            {!! Form::text('keywords', $product->keywords, ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
    </div>
 <input type="submit" value="Update" class='btn btn-success'/>
 {!! Form::close() !!}
@stop

@section('scripts')
 <script src="{{ asset('plugins/fileinput/fileinput.min.js') }}"></script>
 <link rel="stylesheet" type="text/css" href="{{ asset('plugins/fileinput/fileinput.min.css') }}"/>
 <script src="{{ asset('plugins/fileinput/themes/explorer/theme.js') }}"></script>
 <link rel="stylesheet" type="text/css" href="{{ asset('plugins/fileinput/themes/explorer/theme.css') }}"/>

 <script src="{{ asset('plugins/colorpicker/pick-a-color.js') }}"></script>
 <script src="{{ asset('plugins/colorpicker/tinycolor-0.9.15.min.js') }}"></script>
 <link rel="stylesheet" type="text/css" href="{{ asset('plugins/colorpicker/pick-a-color-1.2.3.min.css') }}"/>

 <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

 <script>
     $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });

     $('#category').selectpicker();

     var thumb = JSON.parse('{!! json_encode($thumb) !!}');

     if (thumb !== null) {
         var thumbName = '{!! $product->thumb !!}';
         var thumbPath = thumb.path;
         var thumbDeletePath = thumb.deletePath;
     }


     $("#product_thumb").fileinput({
         showUpload: false,
         initialPreview : [thumbPath],
         initialPreviewAsData: true,
         initialPreviewConfig: [
             {
                 caption: thumbName,
                 url: thumbDeletePath
             }
         ]
     });

     var images = {!! json_encode($images) !!};
     var config = {!! json_encode($imagesConfig) !!};

     $("#product-images").fileinput({
         uploadUrl: false,
         theme: "explorer",
         dropZoneEnabled: true,
         layoutTemplates: {actionUpload: ""},
         uploadAsync: true,
         showUpload: false,
         maxFileCount: 10,
         overwriteInitial: false,
         initialPreviewAsData: true,
         initialPreview : images,
         initialPreviewConfig: config,
     });

     $(document).on("change", "input[id^='color']", function () {
         var newId = 'color' + (Number($(this).attr('id').replace(/\D+/g,"")) + 1);

         var newPicker = $('#' + newId);

         if (!newPicker.length) {
             var input = "<input class='pick-a-color form-control' name='colours[]' id='" + newId + "' type='text'>";
             $('#colours').append(input);

             $('#' + newId).pickAColor({
                 showSpectrum            : true,
                 showSavedColors         : true,
                 saveColorsPerElement    : true,
                 fadeMenuToggle          : true,
                 showAdvanced			: true,
                 showBasicColors         : true,
                 showHexInput            : true,
                 allowBlank				: true,
                 inlineDropdown			: true
             });
         }
     });

     $(".pick-a-color").pickAColor({
         showSpectrum            : true,
         showSavedColors         : true,
         saveColorsPerElement    : true,
         fadeMenuToggle          : true,
         showAdvanced			: true,
         showBasicColors         : true,
         showHexInput            : true,
         allowBlank				: true,
         inlineDropdown			: true
     });

     var config = {
         language: '{{ config('app.locale') }}',
         height: 100,
         filebrowserBrowseUrl: '{!! url($url) !!}',
         toolbarGroups: [
             { name: 'editing'},
             { name: 'forms' },
             { name: 'others' },
             { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
             { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
             { name: 'styles' },
             { name: 'colors' }
         ]
     };
     CKEDITOR.replace('size_fit', config);
     CKEDITOR.replace('details', config);
     CKEDITOR.replace('material_cafe', config);
     CKEDITOR.replace('delivery', config);
     CKEDITOR.replace('video', {
         language: '{{ config('app.locale') }}',
         height: 300,
         filebrowserBrowseUrl: '{!! url($url) !!}',
         toolbar: [
             { name: 'insert', items: ['oembed'] }
         ]
     });
 </script>
@stop