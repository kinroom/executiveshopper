@extends('back.template')

@section('title', "Dashboard")

@section('main')
	@include('back.partials.entete', ['title' => 'Dashboard', 'icone' => 'dashboard', 'fil' => 'Dashboard'])
	<div class="row">
		@include('back/partials/pannel', ['color' => 'green', 'icone' => 'user', 'nbr' => $nbrUsers, 'name' => 'New users', 'url' => route('admin.user.index'), 'total' => 'Users'])
		@include('back/partials/pannel', ['color' => 'yellow', 'icone' => 'pencil', 'nbr' => $nbrPosts, 'name' => 'New posts', 'url' => route('admin.blog.order'), 'total' => 'Posts'])
		@include('back/partials/pannel', ['color' => 'primary', 'icone' => 'envelope', 'nbr' => $nbrVacancies, 'name' => 'New vacancy', 'url' => 'vacancy', 'total' => 'Vacancies'])
	</div>
@stop


