<!DOCTYPE html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>@yield('title')</title>
		<meta name="description" content="">
        <meta name="csrf-token" id="token" content="{{ csrf_token() }}">

        <meta name="viewport" content="width=device-width, initial-scale=1">
        {!! HTML::style('css/main_back.css') !!}
        {!! HTML::style('packages/bootstrap-select-1.12.2/dist/css/bootstrap-select.css') !!}
        @yield('head')
	</head>

    <body>
        <div id="wrapper">
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    @if(session('statut') == 'admin')
                        {!! link_to_route('admin.dashboard', 'Admin', [], ['class' => 'navbar-brand']) !!}
                    @else
                        {!! link_to_route('blog.index','Redactor', [], ['class' => 'navbar-brand']) !!}
                    @endif
                </div>

                <ul class="nav navbar-right top-nav">
                    <li><a href="/">Home</a></li>
                    <li class="dropdown">
                        <a href="/auth/logout"><span class="fa fa-sign-out"></span> <b class="caret"></b></a>
                    </li>
                </ul>

                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li {!! classActivePath('profile') !!}>
                        <a href="/{{App::getLocale()}}/profile"><span class="fa fa-user"></span>Profile</a>
                        </li>
                            @if(session('statut') == 'admin')
                                <li {!! classActivePath('admin') !!}>
                                    <a href="{!! route('admin.dashboard') !!}"><span class="fa fa-fw fa-dashboard"></span> Dashboard</a>
                                </li>
                            @endif
                            <li {!! classActiveSegment(1, 'blog') !!}>
                                <a href="#" data-toggle="collapse" data-target="#articlemenu"><span class="fa fa-fw fa-pencil"></span>Posts<span class="fa fa-fw fa-caret-down"></span></a>
                                <ul id="articlemenu" class="collapse">
                                    <li><a href="{!! route('admin.blog.order') !!}">See all</a></li>
                                    <li><a href="{!! route('profile.post.create') !!}">Add</a></li>
                                    <li><a href="{!! route('admin.sticky_post') !!}">Sticky Post</a></li>
                                </ul>
                            </li>
                        <li {!! classActiveSegment(1, 'translations') !!}>
                            <a href="#" data-toggle="collapse" data-target="#translationmenu"><span class="fa fa-fw fa-pencil"></span>Translations<span class="fa fa-fw fa-caret-down"></span></a>
                            <ul id="translationmenu" class="collapse">
                                <li><a href="{!! route('admin.trans.index', 'en') !!}">English</a></li>
                                <li><a href="{!! route('admin.trans.index', 'de') !!}">Deutsch</a></li>
                                <li><a href="{!! route('admin.trans.index', 'it') !!}">Italian</a></li>
                                <li><a href="{!! route('admin.trans.index', 'ru') !!}">Russian</a></li>
                            </ul>
                        </li>
                        <li {!! classActivePath('admin/vacancy') !!}>
                            <a href="{!! route('admin.vacancy') !!}"><span class="fa fa-fw fa-pencil"></span>Vacancies</a>
                        </li>

                        <li {!! classActiveSegment(1, 'shop') !!}>
                            <a href="#" data-toggle="collapse" data-target="#shopmenu"><span class="fa fa-fw fa-pencil"></span>Shop<span class="fa fa-fw fa-caret-down"></span></a>
                            <ul id="shopmenu" class="collapse">
                                <li><a href="{!! route('product.list') !!}">Products</a></li>
                                <li><a href="{!! route('product_category.index') !!}">Categories</a></li>
                            </ul>
                        </li>

                        @if(session('statut') == 'admin')
                            <li {!! classActiveSegment(1, 'user') !!}>
                                <a href="#" data-toggle="collapse" data-target="#usermenu"><span class="fa fa-fw fa-users"></span>Users<span class="fa fa-fw fa-caret-down"></span></a>
                                <ul id="usermenu" class="collapse">
                                    <li><a href="{!! route('admin.user.index') !!}">See all</a></li>
                                    <li><a href="{!! route('admin.week_blogger') !!}">Blogger of week</a></li>
                                </ul>
                            </li>

                            <li {!! classActivePath('admin/advertisement') !!}>
                                <a href="{!! route('admin.advertisement.index') !!}"><span class="fa fa-fw fa-photo"></span>Advertisement</a>
                            </li>
                            <li {!! classActivePath('admin/ slider') !!}>
                                <a href="{!! route('admin.slider.index') !!}"><span class="fa fa-fw fa-sliders"></span>Slider</a>
                            </li>
                        @endif
                    </ul>
                </div>
            </nav>

            <div id="page-wrapper">
                <div class="container-fluid">
                    @yield('main')
                </div>
            </div>
        </div>

       <script src="{{ asset('js/main.js') }}"></script>
       <script src="{{ asset('js/jquery-2.1.4.min.js') }}"></script>
       <script src="{{ asset('js/bootstrap.min.js') }}"></script>
       <script src="{{ asset('packages/bootstrap-select-1.12.2/dist/js/bootstrap-select.min.js') }}"></script>
       <script src="{{ asset('js/ajax.js') }}"></script>
       @yield('scripts')
    </body>
</html>