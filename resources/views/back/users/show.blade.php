@extends('back.template')

@section('title', "User")

@section('main')
	@include('back.partials.entete', ['title' => 'Dashboard', 'icone' => 'user', 'fil' => link_to('user', 'Users') . ' / ' . 'Card'])
	<p>{{'Name: ' .  $user->firstname . ' ' . $user->lastname }}</p>
	<p>{{'Enail: ' .  $user->email }}</p>
	<p>{{'Role: ' .  $user->role->title }}</p>
	<p>{{ $user->confirmed ? 'Confirmed' : 'Not confirmed' }}</p>
@stop