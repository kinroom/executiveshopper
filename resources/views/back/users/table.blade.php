

    @foreach ($users as $user)
		<tr {!! !$user->seen? 'class="warning"' : '' !!}>
			<td class="text-primary"><strong>{{ $user->firstname }}</strong></td>
            <td class="text-primary"><strong>{{ $user->lastname }}</strong></td>
			<td>{{ $user->role->title }}</td>
			<td>{!! Form::checkbox('seen', $user->id, $user->seen) !!}</td>
			<td>{!! link_to_route('admin.user.show', 'See', [$user->id], ['class' => 'btn btn-success btn-block btn']) !!}</td>
			<td>{!! link_to_route('admin.user.edit', 'Edit', [$user->id], ['class' => 'btn btn-warning btn-block']) !!}</td>
			<td>
				{!! Form::open(['method' => 'DELETE', 'route' => ['admin.user.destroy', $user->id]]) !!}
				{!! Form::destroy('Destroy', 'Do you really want to delete it?') !!}
				{!! Form::close() !!}
			</td>
		</tr>
	@endforeach