@extends('back.template')

@section('title', "Roles")

@section('main')

  @include('back.partials.entete', ['title' => 'Dashboard', 'icone' => 'user', 'fil' => link_to('user', 'Users') . ' / ' .'Roles'])
	<div class="col-sm-12">
		@if(session()->has('ok'))
			@include('partials/error', ['type' => 'success', 'message' => session('ok')])
		@endif
		{!! Form::open(['url' => 'user/roles', 'method' => 'post', 'class' => 'form-horizontal panel']) !!}	
			@foreach ($roles as $role) 
				{!! Form::control('text', 0, $role->slug, $errors, $role->slug, $role->title) !!}
			@endforeach
			{!! Form::submit(trans('front/form.send')) !!}
		{!! Form::close() !!}
	</div>
@stop