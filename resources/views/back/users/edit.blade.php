@extends('back.template')

@section('title', "Edit the user")

@section('main')
	@include('back.partials.entete', ['title' => 'Users', 'icone' => 'user', 'fil' => link_to('user', 'Users') . ' /Edit'])
user/{{ $user->id }}
	<div class="col-sm-12">
		{!! Form::model($user, ['route' => ['admin.user.update', $user->id], 'method' => 'put', 'class' => 'form-horizontal panel']) !!}
            {!! Form::control('text', 0, 'firstname', $errors, 'Firstname') !!}
            {!! Form::control('text', 0, 'lastname', $errors, 'Lastname') !!}
			{!! Form::control('email', 0, 'email', $errors, 'Email') !!}
		<div class="form-group">
			{!! Form::label('role', 'Role:') !!}
			{!! Form::select('role', $roles, $user->role_id, ['class' => 'form-control']) !!}
		</div>
			{!! Form::submit('Update') !!}
		{!! Form::close() !!}
	</div>
@stop