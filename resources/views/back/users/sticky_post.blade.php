@extends('back.template')

@section('title', "Sticky post")

@section('main')
    @include('back.partials.entete', ['title' => 'Sticky Post', 'icone' => 'user', 'fil' => link_to('user', 'Posts') . ' / ' . 'Sticky post'])
    <form action="{{route('admin.new_sticky_post')}}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="ui-widget">
            <div class="form-group">
                {!! Form::label('post', 'Post title:') !!}
                {!! Form::select('post', $posts, $post, ['class' => 'form-control', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <input type="submit" value="Attach" class='btn btn-success'/>
            </div>

        </div>
    </form>
@stop
@section('scripts')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="//code.jquery.com/ui/1.9.1/jquery-ui.js"></script>
    <script>
    </script>
@stop