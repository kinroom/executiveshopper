@extends('back.template')

@section('title', "Blogger of week")

@section('main')
    @include('back.partials.entete', ['title' => 'Blogger of week', 'icone' => 'user', 'fil' => link_to('user', 'Users') . ' / ' . 'Blogger of week'])
    {!! Form::open(['route' => 'admin.new_top_blogger', 'method' => 'post']) !!}
        <div class="ui-widget">
            <div class="form-group">
                <label for="search">Name: </label>
                <input id="search" name="search" onkeyup="liveSearch(this.value, 'week-blogger')" class='form-control' autocomplete="off">
            </div>
            <div class="form-group">
                <label for="description">Description: </label>
                <textarea id="description" name="description" class='form-control' autocomplete="off"></textarea>
            </div>
            <input type="submit" value="New" class='btn btn-success'/>
        </div>
    {!! Form::close() !!}
    @if ( isset($firstname) && isset($lastname))
        <h2>Current blogger of week:</h2>
        <p>First Name: {{$firstname}}</p>
        <p>Last Name: {{$lastname}}</p>
        <p>Description: {{$description}}</p>
    @endif
@stop

@section('scripts')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.9.1/jquery-ui.js"></script>
@stop