@extends('front.template')

@section('title', trans('back/post_propose.propose-post'))

@section('main')
    <section class="fjob-list-section profile-page clearfix">
        @include('back.profile.profile-left')
        <div class="profile-right clearfix">
            {!! Form::open(['route' => 'profile.post.store', 'method' => 'post', "class" => "propose-post-form", 'enctype'=>"multipart/form-data"]) !!}
            {!! Form::hidden('user_id', $user_id) !!}
            <div class="rules-block">
                {!! Form::checkbox('is_commercial','1', '', ['class' => 'checkbox-filter profile-checkbox', 'id' => 'is_commercial']) !!}
                {!! Form::label('is_commercial', trans('back/post_propose.are-commercial')) !!}
            </div>
            {!! Form::text('title', '', ['class' => 'block-input-profile block-input-profile-post-name', 'placeholder' => trans('back/post_propose.post-name'), 'required' => 'required']) !!}
            <span class="post-choose-category">{{trans('back/post_propose.choose-category')}}</span>
            <div class="post-categories-block">
                @foreach($categories as $category)
                    @if ($category->name !== 'video')
                        {!! Form::checkbox("$category->name","$category->id", '', ['class' => 'checkbox-filter profile-checkbox post-main-category-checkbox', 'id' => "$category->name"]) !!}
                        {!! Form::label("$category->name", trans('front/category.'.$category->name)) !!}
                        <?php $subcategories = explode(',', $category->subcategories); ?>
                        @if (count($subcategories) > 0)
                            <div class="subs-block fashion-subs-block">
                                @for ($i = 0;$i < count($subcategories); $i++)
                                        {!! Form::checkbox("$subcategories[$i]","$subcategories[$i]", '', ['class' => 'checkbox-filter profile-checkbox post-sub-category-checkbox', 'id' => "$subcategories[$i]"]) !!}
                                        {!! Form::label("$subcategories[$i]", trans('front/subcategory.'.trim($subcategories[$i]))) !!}
                                @endfor
                            </div>
                        @endif
                    @elseif ($category->name == 'video')
                        {!! Form::checkbox('post-video',"$category->id", '', ['class' => 'checkbox-filter profile-checkbox post-main-category-checkbox', 'id' => 'post-video']) !!}
                        {!! Form::label('post-video', trans('front/category.video')) !!}
                    @endif
                @endforeach
                <div class="post-fields">
                    <div class="clearfix tagsinput tagsinput-videolink">
                        <label for="post-videolink">{{trans('back/post_propose.video-url')}} <span>(YouTube/Vimeo)</span></label>
                        {!! Form::text('post-videolink', '', ['id' => 'post-videolink']) !!}
                    </div>
                </div>
                <div class="post-image-outer">
                    <div class="post-image-inner post-preview" id="post-preview">
                        <img src="/img/post-default.jpg" alt="Post name">
                    </div>
                </div>
                {!! Form::file('thumb', ['class' => 'post-preview-file', 'id' => 'post-preview-file'])!!}
				<span class="post-preview-text">
					{{trans('back/post_propose.recommended-dimensions')}} 1660 x 740 px<br>
                    {{trans('back/post_propose.size-less')}} 600 kb
				</span>
                <span class="write-post-label">{{trans('back/post_propose.write-post')}}</span>
                <div class="text-edit-container">
                    {!! Form::textarea('content', '', ['required' => 'required'] ) !!}
                </div>
                <div class="post-fields">
                    <div class="clearfix tagsinput">
                        {!! Form::label('tags', trans('back/post_propose.tags')) !!}
                        {!! Form::text('tags', '', ['id' => 'tags']) !!}
                    </div>
                    <div class="clearfix">
                        {!! Form::label('photographer', trans('back/post_propose.photographer')) !!}
                        {!! Form::text('photographer', '', ['id' => 'photographer']) !!}
                    </div>
                    <div class="clearfix">
                        {!! Form::label('styling', trans('back/post_propose.styling-makeup')) !!}
                        {!! Form::text('styling', '', ['id' => 'styling']) !!}
                    </div>
                    <div class="clearfix">
                        {!! Form::label('outfit', trans('back/post_propose.outfit')) !!}
                        {!! Form::text('outfit', '', ['id' => 'outfit']) !!}
                    </div>
                    <div class="clearfix">
                        {!! Form::label('location', trans('back/post_propose.location')) !!}
                        {!! Form::text('location', '', ['id' => 'location']) !!}
                    </div>
                    <div class="clearfix post-fields-website">
                        {!! Form::label('website', trans('back/post_propose.website')) !!}
                        {!! Form::text('website', '', ['id' => 'website']) !!}
                    </div>
                </div>
                <div class="clearfix">
                    <input type="submit" value="{{trans('back/post_propose.send-post')}}" class="send-post-button" id="send-post-button">
                </div>
                {!! Form::hidden('category_id', '', ['id' => 'category_id']) !!}
                {!! Form::hidden('subcategory', '', ['id' => 'subcategory']) !!}
                {!! Form::close() !!}
            </div>
        </div>
            <div class="filter-overlay filter-overlay-profile" style="opacity: 0;"></div>
    </section>
@stop

@section('scripts')
	{!! HTML::script('ckeditor/ckeditor.js') !!}
    <script src="{{asset('js/ajax.js')}}"></script>

	<script>
        var config = {
            language: '{{ config('app.locale') }}',
            height: 300,
            filebrowserBrowseUrl: '{!! url($url) !!}',
            toolbarGroups: [
                { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
                { name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
                { name: 'links' },
                { name: 'insert' },
                { name: 'forms' },
                { name: 'tools' },
                { name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
                { name: 'others' },
                //'/',
                { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
                { name: 'styles' },
                { name: 'colors' }
            ]
        };
        CKEDITOR.replace( 'content', config);
  </script>
@stop