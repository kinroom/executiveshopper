@extends('back.template')

@section('title', "All posts")

@section('main')

  @include('back.partials.entete', ['title' => 'Dashboard' . link_to_route('profile.post.create', 'Add', [], ['class' => 'btn btn-info pull-right']), 'icone' => 'pencil', 'fil' => 'Posts'])

	@if(session()->has('ok'))
    @include('partials/error', ['type' => 'success', 'message' => session('ok')])
	@endif

  <div class="row col-lg-12">
    <div class="pull-right link">{!! $links !!}</div>
  </div>

  <div class="row col-lg-12">
    <div class="table-responsive">
      <table class="table">
        <thead>
          <tr>
            <th>
              Title
            </th>
            <th>
              Date
            </th>
            <th>
              Published
            </th>
              <th>
                Author
              </th>
              <th>
                Language
              </th>
              <th>
                Seen
              </th>
          </tr>
        </thead>
        <tbody>
          @foreach ($posts as $post)
            <tr {!! !$post->seen && (session('statut') == 'admin' ||  session('statut') == 'redac') ? 'class="warning"' : '' !!}>
              <td class="text-primary"><strong>{{ $post->title }}</strong></td>
              <td>{{ $post->created_at }}</td>
              <td>{!! Form::checkbox('active', $post->id, $post->active) !!}</td>
              <td>{{ $post->firstname . ' ' . $post->lastname }}</td>
              <td>{!! Form::select('language', $languages, $post->language_id, ['class' => 'form-control', 'id' => $post->id]) !!}</td>
              <td>{!! Form::checkbox('seen', $post->id, $post->seen) !!}</td>
              @if ($post->active == 0)
                <td>{!! link_to_route('admin.post.see', 'See', [$post->name, $post->id], ['class' => 'btn btn-success btn-block btn']) !!}</td>
              @else
                <td>{!! link_to_route('articles.post.show', 'See', [$post->name, $post->slug], ['class' => 'btn btn-success btn-block btn']) !!}</td>
              @endif
              <td>{!! link_to_route('admin.blog.edit', 'Edit', [$post->id], ['class' => 'btn btn-warning btn-block']) !!}</td>
                @if ($post->language_id !== null)
                    <td>{!! link_to_route('admin.trans.translate','Translate', $post->id,['class' => 'btn btn-info btn-block btn']) !!}</td>
                @endif
              <td>
                {!! Form::open(['method' => 'DELETE', 'route' => ['admin.blog.destroy', $post->id]]) !!}
                {!! Form::destroy('Destroy', 'Do you really want to delete it?') !!}
                {!! Form::close() !!}
              </td>
            </tr {!! !$post->seen && (session('statut') == 'admin' ||  session('statut') == 'redac') ? 'class="warning"' : '' !!}>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
  <div class="row col-lg-12">
    <div class="pull-right link">{!! $links !!}</div>
  </div>
@stop

@section('scripts')

  <script>

    $(function() {

      // Language action
      $("select[name='language']").on('change', function() {
        var token = $('input[name="_token"]').val();
        var url = '{{ route('admin.postlanguage', ':id') }}';
        url = url.replace(':id', $(this).attr('id'));
        $.ajax({
          url: url,
          type: 'PUT',
          data: "language_id=" + $(this).val() + "&_token=" + token
        }).done(function() {

        }).fail(function() {

        });
      });


      // Seen gestion
      $(document).on('change', ':checkbox[name="seen"]', function() {
        $(this).parents('tr').toggleClass('warning');
        $(this).hide().parent().append('<i class="fa fa-refresh fa-spin"></i>');
        var token = $('input[name="_token"]').val();
        var url = '{{ route('admin.postseen', ':id') }}';
        url = url.replace(':id', this.value);
        $.ajax({
                  url: url,
                  type: 'PUT',
                  data: "seen=" + this.checked + "&_token=" + token
                })
                .done(function() {
                  $('.fa-spin').remove();
                  $('input:checkbox[name="seen"]:hidden').show();
                })
                .fail(function() {
                  $('.fa-spin').remove();
                  chk = $('input:checkbox[name="seen"]:hidden');
                  chk.show().prop('checked', chk.is(':checked') ? null:'checked').parents('tr').toggleClass('warning');
                  alert('{{ trans('back/blog.fail') }}');
                });
      });

      // Active gestion
      $(document).on('change', ':checkbox[name="active"]', function() {
        $(this).hide().parent().append('<i class="fa fa-refresh fa-spin"></i>');
        var token = $('input[name="_token"]').val();
        var url = '{{ route('admin.postactive', ':id') }}';
        url = url.replace(':id', this.value);
        $.ajax({
                  url: url,
                  type: 'PUT',
                  data: "active=" + this.checked + "&_token=" + token
                })
                .done(function() {
                  $('.fa-spin').remove();
                  $('input:checkbox[name="active"]:hidden').show();
                })
                .fail(function() {
                  $('.fa-spin').remove();
                  chk = $('input:checkbox[name="active"]:hidden');
                  chk.show().prop('checked', chk.is(':checked') ? null:'checked').parents('tr').toggleClass('warning');
                  alert('{{ trans('back/blog.fail') }}');
                });
      });


    });

  </script>

@stop