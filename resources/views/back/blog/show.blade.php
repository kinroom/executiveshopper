@extends('front.template')

@section('title', "$post->title")

@section('main')
    <div class="latest-news-section clearfix">

        @if ($category !== 'video')
            <div class="post-main-img">
                @if ($post->thumb != '')
                    <img src="/img/uploads/thumbs/fourth/{!! $post->user_id !!}/{!! $post->thumb !!}" alt="{{ $post->title }}">
                @else
                    <img src="/img/post-default.jpg" alt="{{ $post->title }}">
                @endif
            </div>
        @endif

        <div class="blog-article-section">
            <article class="blog-article">
                <a class="blog-article-category-link" href="/category/{{$category}}/">{{ trans('front/category.'.$category) }}</a>
                <h1 class="blog-article-title">{{ $post->title }}</h1>
                <div class="blog-article-author-share">
                    <div class="blog-article-author-share-date-author">
                        <span class="latest-news-date"> {!! $post->created_at !!}</span>
                        <span class="latest-news-author">{{trans('front/post.by') }} {{$author->firstname}} {{$author->lastname}}</span>
                    </div>
                </div>

                @if (($post->video !== ''))
                    <div class="blog-video-wrapper">
                        https://vimeo.com/151002965
                    </div>
                @endif

                <div class="clearfix">
                    {!! $post->content !!}
                </div>

                @if (($post->photographer != ''))
                    <div class="under-blog-article">
                        <span class="under-blog-1">{{trans('front/post.photographer') }}</span>
                        <span class="under-blog-2">{{ $post->photographer }}</span>
                    </div>
                @endif

                @if (($post->styling != ''))
                    <div class="under-blog-article">
                        <span class="under-blog-1">{{trans('front/post.styling-makeup') }}</span>
                        <span class="under-blog-2">{{ $post->styling }}</span>
                    </div>
                @endif

                @if (($post->outfit != ''))
                    <div class="under-blog-article">
                        <span class="under-blog-1">{{trans('front/post.outfit') }}</span>
                        <span class="under-blog-2">{{ $post->outfit }}</span>
                    </div>
                @endif

                @if (($post->location != ''))
                    <div class="under-blog-article">
                        <span class="under-blog-1">{{trans('front/post.location') }}</span>
                        <span class="under-blog-2">{{ $post->location }}</span>
                    </div>
                @endif

                @if (($post->website != ''))
                    <div class="under-blog-article">
                        <span class="under-blog-1">{{trans('front/post.website') }}</span>
                        <span class="under-blog-2">{{ $post->website }}</span>
                    </div>
                @endif

                @if (($post->is_commercial))
                    <div class="blog-buy">
                        <h2>{{trans('front/post.also-buy') }}</h2>
                        <span class="promo-how">{{trans('front/post.just-click') }}<span>{{trans('front/post.about-how') }}</span></span>
                        <div class="blog-buy-divider"></div>
                        @if (Auth::guest())
                            <a href="#" class="button-buy-service" data-toggle="modal" data-target="#modal-buy-service-not-registered">{{trans('front/post.buy-service') }}</a>
                        @else
                            <a href="#" class="button-buy-service" data-toggle="modal" data-target="#modal-buy-service">{{trans('front/post.buy-service') }}</a>
                        @endif
                    </div>
                @endif

                <div class="blog-tags">
                    @if($post->tags->count())
                        @if($post->tags->count() > 0)
                            @foreach($post->tags as $tag)
                                <a href="/blog/tag?tag={!! $tag->id !!}" class="blog-tag">{!! $tag->tag !!}</a>
                            @endforeach
                        @endif
                    @endif
                </div>
            </article>

        </div>

    </div>
@stop


@section('footer')

    @if (Auth::guest())
        <div class="modal fade login-main-modal modal-ajax" tabindex="-1" role="dialog" id="modal-buy-service-not-registered">  <!-- MODAL BUY SERVICE FORM-->
            <div class="modal-dialog modal-sm">
                <div class="modal-content modal-login-content">
                    <h4>Send request</h4>
                    <p>Enter your name, email and phone, so we can contact you</p>
                    {!! Form::open(['class' => 'buy-service-form', 'id' => 'buy-service']) !!}
                    {!! Form::hidden('post', "$post->title") !!}
                    {!! Form::label('name', 'Name')!!}
                    {!! Form::text('name','',['required' => 'required']) !!}
                    {!! Form::label('email', 'Email')!!}
                    {!! Form::email('email', '', ['id' => 'email'])!!}
                    {!! Form::label('phone', 'Phone')!!}
                    {!! Form::text('phone', '',['required' => 'required'])!!}
                    {!! Form::close() !!}
                    <button class="delete-company-modal-buttons delete-company-modal-buttons-yes" type="button" id="buy-service-send">Send request</button>
                </div>
            </div>
        </div>
    @else
        <div class="modal fade login-main-modal modal-ajax" tabindex="-1" role="dialog" id="modal-buy-service">  <!-- MODAL BUY SERVICE -->
            <div class="modal-dialog modal-md">
                <div class="modal-content modal-login-content">
                    {!! Form::open(['class' => 'buy-service-form', 'id' => 'buy-service']) !!}
                    {!! Form::hidden('post', "$post->title") !!}
                    {!! Form::hidden('name', "$user->firstname  $user->lastname") !!}
                    {!! Form::hidden('email', "$user->email")!!}
                    {!! Form::hidden('phone', "$user->phone")!!}
                    {!! Form::close() !!}
                    <h4>Do you really want to buy this service?</h4>
                    <p>Your phone number and email will be sent to us, so we can contact you</p>
                    <button class="delete-company-modal-buttons delete-company-modal-buttons-yes" type="button" id="buy-service-yes">Yes</button>
                    <button class="delete-company-modal-buttons delete-company-modal-buttons-no" type="button" data-dismiss="modal">No</button>
                    <button class="close-modal-button" type="button" data-dismiss="modal"><img src="/img/icons/close-icon.svg" alt="Close modal"></button>
                </div>
            </div>
        </div>
    @endif
@stop
