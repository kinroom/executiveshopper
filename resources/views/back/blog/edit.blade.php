@extends('front.template')

@section('title', "Edit the post")

@section('main')
    <section class="fjob-list-section profile-page clearfix">
        <div class="profile-right clearfix">
            {!! Form::open(['route' => ['admin.blog.update', $post->id], 'method' => 'put', "class" => "propose-post-form", 'enctype'=>"multipart/form-data"]) !!}
                {!! Form::hidden('user_id', $post->user_id) !!}
                {!! Form::text('title', "$post->title", ['class' => 'block-input-profile block-input-profile-post-name', 'placeholder' => 'Name of the post', 'required' => 'required']) !!}
                <span class="post-choose-category">Choose the category</span>
                <div class="post-categories-block">
                    @foreach($categories as $category)
                        @if ($category->name !== 'Video')
                            {!! Form::checkbox($category->name, $category->id, '', ['class' => 'checkbox-filter profile-checkbox post-main-category-checkbox', 'id' => $category->name]) !!}
                            {!! Form::label($category->name, ucfirst($category->name)) !!}
                            <?php $subcategories = explode(',', $category->subcategories); ?>
                            @if (count($subcategories) > 0)
                                <div class="subs-block fashion-subs-block">
                                    @for ($i = 0;$i < count($subcategories); $i++)
                                        {!! Form::checkbox($subcategories[$i],$subcategories[$i], '', ['class' => 'checkbox-filter profile-checkbox post-sub-category-checkbox', 'id' => $subcategories[$i]]) !!}
                                        {!! Form::label($subcategories[$i], ucfirst(trim($subcategories[$i]))) !!}
                                    @endfor
                                </div>
                            @endif
                        @elseif($category->name == 'Video')
                            {!! Form::checkbox('post-video','Video', '', ['class' => 'checkbox-filter profile-checkbox post-main-category-checkbox', 'id' => 'post-video']) !!}
                            {!! Form::label('post-video', 'Video') !!}
                        @endif
                    @endforeach
                    <div class="post-fields">
                        <div class="clearfix tagsinput tagsinput-videolink">
                            <label for="post-videolink">Video URL <span>(YouTube/Vimeo)</span></label>
                            {!! Form::text('post-videolink', '$post->video', ['id' => 'post-videolink']) !!}
                        </div>
                    </div>
                    <div class="post-image-outer">
                        <div class="post-image-inner post-preview" id="post-preview">
                            @if ($post['thumb'] !== '')
                                <img src="/img/uploads/thumbs/fourth/{{$post->user_id}}/{{$post->thumb}}" />
                            @else
                                <img src="/img/post-default.jpg" alt="Post name">
                            @endif

                        </div>
                    </div>
                    {!! Form::file('thumb', ['class' => 'post-preview-file', 'id' => 'post-preview-file'])!!}
                    <span class="post-preview-text">
                        Recommended demensions 1660 x 740 px<br>
                        Size weight less then 600 kb
                    </span>
                    <span class="write-post-label">Write your post</span>
                    <div class="text-edit-container">
                        {!! Form::textarea('content', "$post->content", ['required' => 'required'] ) !!}
                    </div>
                        <span class="write-post-label">Write description for SEO:</span>
                        <div class="text-edit-container">
                            {!! Form::textarea('description', "$post->description") !!}
                        </div>

                        <div class="post-fields">
                            {!! Form::label('keywords', 'Keywords') !!}
                            {!! Form::text('keywords', "$post->keywords", ['id' => 'keywords']) !!}
                        </div>
                    <div class="post-fields">
                        <div class="clearfix">
                            {!! Form::label('photographer', 'Photographer') !!}
                            {!! Form::text('photographer', "$post->photographer", ['id' => 'photographer']) !!}
                        </div>
                        <div class="clearfix">
                            {!! Form::label('styling', 'Styling / Makeup') !!}
                            {!! Form::text('styling', "$post->styling", ['id' => 'styling']) !!}
                        </div>
                        <div class="clearfix">
                            {!! Form::label('outfit', 'Outfit') !!}
                            {!! Form::text('outfit', "$post->outfit", ['id' => 'outfit']) !!}
                        </div>
                        <div class="clearfix">
                            {!! Form::label('location', 'Location') !!}
                            {!! Form::text('location', "$post->location", ['id' => 'location']) !!}
                        </div>
                        <div class="clearfix post-fields-website">
                            {!! Form::label('website', 'Website') !!}
                            {!! Form::text('website', "$post->website", ['id' => 'website']) !!}
                        </div>
                    </div>
                    <div class="clearfix">
                        <input type="submit" value="Send post" class="send-post-button" id="send-post-button">
                    </div>
                    {!! Form::hidden('category_id', "$post->category_id", ['id' => 'category_id']) !!}
                    {!! Form::hidden('subcategory', "$post->subcategory", ['id' => 'subcategory']) !!}
                {!! Form::close() !!}
            </div>
        </div>
        <div class="filter-overlay filter-overlay-profile" style="opacity: 0;"></div>
    </section>
@stop

@section('scripts')
    {!! HTML::script('ckeditor/ckeditor.js') !!}
    <script>
        var config = {
            language: '{{ App::getLocale() }}',
            height: 300,
            filebrowserBrowseUrl: '{!! url($url) !!}',
            toolbarGroups: [
                { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
                { name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
                { name: 'links' },
                { name: 'insert' },
                { name: 'forms' },
                { name: 'tools' },
                { name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
                { name: 'others' },
                //'/',
                { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
                { name: 'styles' },
                { name: 'colors' }
            ]
        };
        CKEDITOR.replace( 'content', config);
    </script>
@stop
