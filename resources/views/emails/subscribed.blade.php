@extends('front.template')

@section('title', trans('front/subscribed.successful-subscribe'))

@section('main')
    <div class="static-page-wrapper static-page-about-us-wrapper">
        <section class="fjob-list-section static-page-section clearfix">
            <article>
                <h1 class="static-page-heading">Executive shopper<br><span>{{trans('front/subscribed.thank-you')}}!</span></h1>
                <p>{{trans('front/subscribed.will-sell')}}</p>
            </article>
        </section>
    </div>
@stop