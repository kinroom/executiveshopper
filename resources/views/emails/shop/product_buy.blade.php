<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
</head>
<body>
<div>
    <h4>{{ $product['name'] }}</h4>
    <p>Product name: {{ $product['name'] }}</p>
    <p>Size: {{ $product['size'] }}</p>

    @if ($product['color'] !== 'None')
        <p>Colour: </p>
        <table style="height: 20px; width: 20px;background: {{$product['color']}};"><tr><td></td></tr></table>
    @else
        <p>Colour: {{ $product['color'] }}</p>
    @endif
    <p>Price: ${{ $product['price'] }}</p>

    <h4>Client name: {{ $user['name'] }}</h4>
    <p>Email: {{ $user['email'] }}</p>
    <p>Phone: {{ $user['phone'] }}</p>
</div>
</body>
</html>