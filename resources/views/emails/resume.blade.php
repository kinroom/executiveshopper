<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
</head>
<body>
<div>
    <h4>{!! $title !!}</h4>
    <p>For: {{$company}}</p>
    <p>Vacancy: {{$vacancy}}</p>
    <p>Resume in attachment!</p>
</div>
</body>
</html>