<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
</head>
<body>
<div>
    <h4>{!! $title !!}</h4>
    <p>Post: {{$post}}</p>
    <p>Name: {{$name}}</p>
    <p>Email: {{$email}}</p>
    <p>Phone: {{$phone}}</p>
</div>
</body>
</html>