@extends('front.template')

@section('title', 'Executive Shopper')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{asset('css/animate.css')}}" />
@stop

@section('main')
    @if (count($slider) > 0)
         <section class="slider-wrapper">
            <div class="main-slider">
                <ul class="clearfix">
                   @foreach ($slider as $slide)
                        <li>
                            @if ($slide->post->image !== '' && file_exists(base_path() .'/public/img/uploads/thumbs/fourth/' . $slide->post->user_id . '/' . $slide->post->thumb))
                                <img alt="{!! $slide->post->alt !!}" src="/img/uploads/thumbs/fourth/{{ $slide->post->user_id }}/{{ $slide->post->thumb }}" />
                            @else
                                <img alt="{!! $slide->post->alt !!}" src="/img/post-default.jpg" />
                            @endif
                            <div class="slide-text">
                                <span class="slide-text-top animated">{{ trans('front/category.' . strtolower($slide->post->category->name)) }}</span>
                                <div class="slide-divider animated"></div>
                                <h2 class="slide-text-heading animated"><a href="{{ route('articles.post.show', [$slide->post->category->name, $slide->post->slug]) }}">{{ $slide->post->title }}</a></h2>
                                <span class="slide-text-bottom animated">{{ trans('front/home.by') }} {{ $slide->post->user->firstname . ' ' .  $slide->post->user->lastname }}</span>
                            </div>
                        </li>
                    @endforeach

                </ul>
                <div id="slider-nav-left" class="slider-controls"><i class="fa fa-angle-left"></i></div>
                <div id="slider-nav-right" class="slider-controls slider-controls-right"><i class="fa fa-angle-right"></i></div>
            </div>
        </section>
    @endif

    @if (count($latest_posts) > 0 )
        <section class="latest-news-section clearfix"> <!-- LATEST NEWS SECTION -->
            <h2 class="latest-news-section-heading">{{ trans('front/home.daily-dose') }}</h2>
            <ul>
                @foreach ($latest_posts as $key => $latest_post)
                    <li class="latest-news-item" style="height: 339px;"> <!-- LATEST NEWS ITEM TEMPLATE -->
                        <div class="latest-news-preview-img-block-outer">
                            <a href="{{ route('articles.post.show', [$latest_post->category->name, $latest_post->slug]) }}" class="latest-news-preview-img-block-inner">
                                @if ($latest_post->thumb !== '' && file_exists(base_path() .'/public/img/uploads/thumbs/first/' . $latest_post->user_id . '/' .$latest_post->thumb))
                                    <img alt="{!! $latest_post->title !!}" src="/img/uploads/thumbs/first/{!! $latest_post->user_id !!}/{!! $latest_post->thumb !!}" />
                                @else
                                    <img alt="{!! $latest_post->title !!}" src="/img/post-default-feautered.jpg" />
                                @endif
                                <div class="latest-news-preview-img-overlay">
                                    <span>{{ trans('front/blog.read-article') }}</span>
                                    <div class="read-article-divider"></div>
                                </div>
                            </a>
                        </div>
                        <a class="latest-news-category-link" href="{{ route('category.show', $latest_post->category->name) }}">{{ trans('front/category.' . $latest_post->category->name) }}</a>
                        <h3><a class="latest-news-item-heading" href="{{ route('articles.post.show', [$latest_post->category->name, $latest_post->slug]) }}">{!! $latest_post->title !!}<div class="news-underline"></div></a></h3>
                        <span class="latest-news-date"> {!!  date('d.m.Y', strtotime($latest_post->created_at)) !!} </span>
                        <span class="latest-news-author">{{ trans('front/home.by') }} {!! $latest_post->user->firstname . ' ' . $latest_post->user->lastname !!}</span>
                        <div class="likes-views-buttons">
                            <button onclick="Like({{ $latest_post->id }}, 'daily_')">
                                <i class="fa fa-heart"></i>
                                <span id="daily_{{ $latest_post->id }}">{{$latest_post->likes}}</span>
                            </button>
                                <span class="views-number">
                                    <i class="fa fa-eye"></i>
                                    {{ $latest_post->views }}
                                </span>
                        </div>
                    </li>
                @endforeach
            </ul>
        </section>
    @endif

    @if (count($latest_from_cat_1) > 0)
        <section class="latest-from-category-section"> <!-- LATEST FROM CATEGORIES SECTION -->
            <a class="all-categories-link" href="{{ route('category.show', 'all') }}">{{ trans('front/home.all-categories') }}</a>
            <h2 class="latest-from-category-heading">{{ trans('front/home.latest-from-categories') }}</h2>
            <span class="latest-from-category-subheading">{{ trans('front/home.explore-the-latest-news') }}</span>
            <div class="latest-from-category-tabs">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#firsttab" aria-controls="home" role="tab" data-toggle="tab">{{ trans('front/category.fashion') }}</a></li>
                    <li role="presentation"><a href="#secondtab" aria-controls="profile" role="tab" data-toggle="tab">{{ trans('front/category.lifestyle') }}</a></li>
                    <li role="presentation"><a href="#thirdtab" aria-controls="messages" role="tab" data-toggle="tab">{{ trans('front/category.beauty') }}</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane animated fadeIn active" id="firsttab">
                        <div class="latest-from-category-posts clearfix">
                            <div class="latest-from-category-left">
                                <div class="latest-from-category-left-inner">
                                    <div class="latest-category-arrow"></div>
                                </div>
                            </div>
                            <div class="latest-from-category-right animated fadeInRight">
                                <div class="latest-from-category-right-inner">
                                    <ul>
                                        @foreach ($latest_from_cat_1 as $latest_first)
                                            <li> <!-- LATEST FROM CATEGORIES FASHION ITEM TEMPLATE -->
                                                <a href="{{ route('articles.post.show', [$latest_first->category->name, $latest_first->slug]) }}">
                                                    @if($latest_first->thumb !== '' && file_exists(base_path() .'/public/img/uploads/thumbs/second/' . $latest_first->user_id . '/' . $latest_first->thumb))
                                                        <img alt="{!! $latest_first->title !!}" src="/img/uploads/thumbs/second/{!! $latest_first->user_id !!}/{!! $latest_first->thumb !!}" />
                                                    @else
                                                        <img alt="{!! $latest_first->title !!}" src="/img/post-default.jpg" />
                                                    @endif
                                                </a>
                                                <h3><a href="{{ route('articles.post.show', [$latest_first->category->name, $latest_first->slug]) }}">{{$latest_first->title}}</a></h3>
                                                <span>{!!  date('d.m.Y', strtotime($latest_first->created_at)) !!}</span>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane animated fadeIn" id="secondtab">
                        <div class="latest-from-category-posts clearfix">
                            <div class="latest-from-category-left">
                                <div class="latest-from-category-left-inner">
                                    <div class="latest-category-arrow"></div>
                                </div>
                            </div>
                            <div class="latest-from-category-right animated fadeInRight">
                                <div class="latest-from-category-right-inner">
                                    <ul>
                                        @foreach ($latest_from_cat_2 as $latest_second)
                                            <li> <!-- LATEST FROM CATEGORIES FASHION ITEM TEMPLATE -->
                                                <a href="{{ route('articles.post.show', [$latest_first->category->name, $latest_first->slug]) }}">
                                                    @if($latest_second->thumb !== ''  && file_exists(base_path() .'/public/img/uploads/thumbs/second/' . $latest_second->user_id . '/' . $latest_second->thumb))
                                                        <img alt="{!! $latest_second->title !!}" src="/img/uploads/thumbs/second/{!!$latest_second->user_id !!}/{!! $latest_second->thumb !!}" />
                                                    @else
                                                        <img alt="{!! $latest_second->title !!}" src="/img/post-default.jpg" />
                                                    @endif
                                                </a>
                                                <h3><a href="{{ route('articles.post.show', [$latest_second->category->name, $latest_second->slug]) }}">{{ $latest_second->title }}</a></h3>
                                                <span>{!! date('d.m.Y', strtotime($latest_second->created_at)) !!}</span>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane animated fadeIn" id="thirdtab">
                        <div class="latest-from-category-posts clearfix">
                            <div class="latest-from-category-left">
                                <div class="latest-from-category-left-inner">
                                    <div class="latest-category-arrow"></div>
                                </div>
                            </div>
                            <div class="latest-from-category-right animated fadeInRight">
                                <div class="latest-from-category-right-inner">
                                    <ul>
                                        @foreach ($latest_from_cat_3 as $latest_third)
                                            <li> <!-- LATEST FROM CATEGORIES FASHION ITEM TEMPLATE -->
                                                <a href="{{ route('articles.post.show', [$latest_first->category->name, $latest_first->slug]) }}">
                                                    @if($latest_third->thumb !== '' && file_exists(base_path() .'/public/img/uploads/thumbs/second/' . $latest_third->user_id . '/' . $latest_third->thumb))
                                                        <img alt="{!! $latest_third->title !!}" src="/img/uploads/thumbs/second/{!!$latest_third->user_id !!}/{!! $latest_third->thumb !!}" />
                                                    @else
                                                        <img alt="{!! $latest_third->title !!}" src="/img/post-default.jpg" />
                                                    @endif
                                                </a>
                                                <h3><a href="{{ route('articles.post.show', [$latest_third->category->name, $latest_third->slug]) }}">{{ $latest_third->title }}</a></h3>
                                                <span>{!!  date('d.m.Y', strtotime($latest_third->created_at)) !!}</span>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif

    @if (count($most_viewed) > 0)
        <section class="most-viewed-section clearfix"> <!-- MOST VIEWED POSTS SECTION -->
            <h2 class="most-viewed-heading">{{ trans('front/home.most-viewed-posts') }}</h2>
            <span class="most-viewed-subheading">{{ trans('front/home.know-what-people-prefer') }}</span>
            <div class="most-viewed-left">
                <ul>
                    @foreach($most_viewed as $key => $most_view)
                        <li class="most-viewed-item clearfix"> <!-- MOST VIEWED POSTS ITEM TEMPLATE -->
                            <div class="most-viewed-post-left">
                                <a href="{{ route('articles.post.show', [$most_view->category->name, $most_view->slug]) }}">
                                    @if ($most_view->thumb !== '' && file_exists(base_path() .'/public/img/uploads/thumbs/third/' . $most_view->user_id . '/' . $most_view->thumb))
                                        <img alt="{!! $most_view->img_alt !!}" src="/img/uploads/thumbs/third/{!! $most_view->user_id !!}/{!! $most_view->thumb !!}" />
                                    @else
                                        <img alt="{!! $most_view->img_alt !!}" src="/img/post-default-feautered.jpg" />
                                    @endif
                                    <div class="latest-news-preview-img-overlay">
                                        <span>{{ trans('front/blog.read-article') }}</span>
                                        <div class="read-article-divider"></div>
                                    </div>
                                </a>
                            </div>
                            <div class="most-viewed-post-right">
                                <a class="most-viewed-category-link" href="{{ route('category.show', $most_view->category->name) }}/">{{ trans('front/category.'. $most_view->category->name) }}</a>
                                <h3>
                                    <a href="{{ route('articles.post.show', [$most_view->category->name, $most_view->slug]) }}"> {!! $most_view->title !!}<div class="most-viewed-underline"></div></a>
                                </h3>
                                <p>{!! $most_view->summary !!}</p>
                                <span class="most-viewed-date">{!!  date('d.m.Y', strtotime($most_view->created_at)) !!}</span>
                                <div class="most-viewed-author-likes">
                                    <span>{{ trans('front/home.by') }} <a>{!! $most_view->user->firstname . ' ' . $most_view->user->lastname !!}</a></span>
                                    <div class="most-viewed-likes">
                                        <button onclick="Like({{ $most_view->id }}, 'most_')">
                                            <i class="fa fa-heart"></i>
                                            <span id="most_{{ $most_view->id }}">{{$most_view->likes}}</span>
                                        </button>
                                        <span class="views-number"><i class="fa fa-eye"></i>{!! $most_view->views!!}</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>

            <div class="most-viewed-right"> <!-- BLOGGER OF THE WEEK SECTION -->
                @if (isset($week_blogger))
                    <div class="blogger-of-the-week">
                        <a class="blogger-of-the-week-img">
                            @if ($week_blogger->avatar !== '' && $week_blogger->service == 'site'))
                                <img src="/img/uploads/profiles/{!!$week_blogger->id!!}/{!! $week_blogger->avatar !!}" alt="{{ $week_blogger->firstname . ' ' . $week_blogger->lastname }}">
                            @elseif ($week_blogger->avatar !== '' && $week_blogger->service !== 'site' && file_exists(base_path() .'/public/img/uploads/profiles/' . $week_blogger->id . '/' . $week_blogger->avatar))
                                <img src="{{$week_blogger->avatar}}" alt="{{ $week_blogger->firstname . ' ' . $week_blogger->lastname }}" />
                            @else
                                <img src="/img/avatar.gif" alt="{{ $week_blogger->firstname . ' ' . $week_blogger->lastname }}"/>
                            @endif
                        </a>
                        <span>{{ trans('front/home.blogger-of-week') }}</span>
                        <h3>
                            <a>{{ $week_blogger->firstname . ' ' . $week_blogger->lastname }}</a>
                        </h3>
                        <div class="blogger-of-the-week-dots">
                            <i class="fa fa-circle"></i>
                            <br>
                            <i class="fa fa-circle"></i>
                            <br>
                            <i class="fa fa-circle"></i>
                            <br>
                        </div>
                        <p>{{$week_blogger->description}}</p>
                    </div>
                @endif
                <div class="main-advertising">
                    @if (count($advertisement) > 0)
                        <span>{!! $advertisement['code'] !!}</span>
                    @else
                        <span>Place for advertising</span>
                    @endif
                </div>
            </div>
        </section>
    @endif

    @if (count($latest_videos) > 0)
        <section class="latest-video-section"> <!-- LATEST VIDEO SECTION -->
            <h2 class="most-viewed-heading">{{ trans('front/home.latest-videos') }}</h2>
            <span class="most-viewed-subheading">{{ trans('front/home.your-dose-of-visual-inspiration') }}</span>
            <div class="latest-video-wrapper">
                <div class="latest-video-posts clearfix">
                    <div class="latest-video-left">
                        <div class="latest-video-left-inner">
                            <ul>
                                @foreach($latest_videos as $video)
                                    <li> <!-- LATEST VIDEO ITEM TEMPLATE -->
                                        {!! $video->video !!}
                                        <h3><a href="{{ route('articles.post.show', ['video', $video->slug]) }}">{{ $video->title }}</a></h3>
                                        <span class="latest-video-date">{{ $video->created_at }}</span>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="latest-video-right">
                        <div class="latest-video-right-inner">
                            @foreach($latest_videos as $key => $video)
                                <div class="video-{{ $key + 1 }} animated fadeIn"></div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif

    @if (isset($special_news))
        <section class="special-news-section clearfix"> <!-- SPECIAL NEWS SECTION -->
            <h2 class="most-viewed-heading">{{ trans('front/home.special-news') }}</h2>
            <span class="most-viewed-subheading">{{ trans('front/home.be-unique') }}</span>
            <div class="special-news-item"> <!-- SPECIAL NEWS ITEM TEMPLATE -->
                <div class="special-news-left">
                    <div class="special-news-left-inner">
                        @if ($special_news->thumb != ''  && file_exists(base_path() .'/public/img/uploads/thumbs/fourth/' . $special_news->user_id . '/' . $special_news->thumb))
                            <img alt="{!! $special_news[0]['title'] !!}" src="/img/uploads/thumbs/fourth/{!! $special_news->user_id !!}/{!! $special_news->thumb !!}">
                        @else
                            <img alt="{!! $special_news[0]['title'] !!}" src="/img/post-default.jpg">
                        @endif
                    </div>
                </div>
                <div class="special-news-right">
                    <div class="special-news-right-inner">
                        <div class="special-news-right-inner-text">
                            <a href="{{ route('category.show', $special_news->category->name) }}" class="special-news-category-link">{{ trans('front/category.' . $special_news->category->name) }}</a>
                            <h3>{!! $special_news->title !!}</h3>
                            <div class="special-news-social">
                                <span>{{ trans('front/home.share-post') }}</span>
                                <a href="/{{App::getLocale()}}/share-twitter/{{ $special_news->category->name }}/{{ $special_news->slug }}/{{ $special_news->title }}"><i class="fa fa-twitter"></i></a>
                                <a href="/{{App::getLocale()}}/share-pinterest/{{ $special_news->category->name }}/{{ $special_news->slug }}/{{ $special_news->title }}"><i class="fa fa-pinterest"></i></a>
                                <a href="/{{App::getLocale()}}/share-facebook/{{ $special_news->category->name }}/{{ $special_news->slug }}/{{ $special_news->title }}"><i class="fa fa-facebook"></i></a>
                            </div>
                            <p>{!! $special_news->content !!}</p>
                            <a href="{{ route('articles.post.show', [$special_news->category->name, $special_news->slug]) }}" class="special-news-button">{{ trans('front/blog.read-article') }}<i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif
    <section class="follow-instagram-section"> <!-- FOLLOW US ON INSTAGRAM SECTION -->
        <span class="most-viewed-subheading follow-instagram-subheading">#ExecutiveShopper</span>
        <h2 class="most-viewed-heading follow-instagram-heading">{{ trans('front/home.follow-us-on-instagram') }}</h2>
        <div class="instagram-photos">
            <!-- LightWidget WIDGET --><script src="//lightwidget.com/widgets/lightwidget.js"></script><iframe src="//lightwidget.com/widgets/cf37b265047a5d31bad6734feadb7aba.html" id="lightwidget_cf37b26504" name="lightwidget_cf37b26504"  scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe>
        </div>
    </section>
    <h2 class="most-viewed-heading">{{ trans('front/home.follow-us') }}</h2>
    <span class="most-viewed-subheading">{{ trans('front/home.in-social-networks') }}</span>
@stop

@section('footer')
    <script src="{{ asset('js/instansive.js') }}"></script>
    <script src="{{ asset('js/main-slider.js') }}"></script>
@stop