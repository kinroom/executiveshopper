<div class="navigation-box row">
    <div class="top-nav col-sm-6 col-xs-9 col-sm-offset-1">
        <div class="shift-mobile-nav" data-hook="toggle-menu">
            <i class="fa fa-bars" aria-hidden="true"></i>
        </div>
        <ul class="category-list">
            @foreach($roots as $category)
                <li>
                    <span class="submenu-container"><a href="javascript:;">{{ $category->name }}<i class="fa" aria-hidden="true"></i></a></span>
                    <div class="sub-menu tree-lists">
                        @foreach($category->children as $subCategory)
                            <div class="column col-sm-4">
                                <span class="sub-menu-title">{{ $subCategory->name }}</span>
                                <ul>
                                    @foreach($subCategory->children as $child)
                                        <li><a href="{{ route('boutique', implode('/', $subCategory->getAncestorsAndSelf()->lists('slug')->toArray()) . '/' . $child->slug) }}">{{ $child->name }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        @endforeach
                    </div>
                </li>
            @endforeach

        </ul>
    </div>

    <!--<div class="acc-bag-container col-sm-3 col-xs-3 col-sm-offset-1">
        <a href="#">
            <img src="images/icon-user.png" alt="Account icon">
            <span>My Account</span>
        </a>
        <a href="#">
            <img src="images/icon-bag.png" alt="Bag icon">
            <span>My Bag</span>
            <span class="count">2</span>
        </a>
    </div>-->
</div>