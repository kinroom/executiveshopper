@extends('front.template')

@section('title', $categoryName . ' - Burbell')

@if (isset($category)) @section('keywords'){{ $category->keywords }}@stop @endif
@if (isset($category)) @section('description'){{ $category->description }}@stop @endif

@section('main')

    <section class="main-content"> <!-- BODY -->
        @include('front.shop.navigation')
    </section>

    <section class="latest-news-section product-list clearfix">
        <h1 class="category-heading">{{ $categoryName }}</h1>
        <div class="category-nav clearfix">
            <div class="category-nav-inner-wrapper">
                <div class="articles-number">
                    <span class='articles-number-span'><span>{{ $counter }}</span> {{ trans('front/shop.products') }}</span>
                </div>
            </div>
        </div>

        <ul class="category-news-list category-products clearfix" id="articles">
            @include('front.shop.list')
        </ul>

        @if ($counter > 6)
            <button class="more-posts">
                <span>{{ trans('front/blog.show-more') }}</span>
                <i class="fa fa-angle-down show-more-icon"></i>
            </button>
        @endif
    </section>

    <link rel="stylesheet" type="text/css" href="{{ asset('css/boutique.css') }}"/>

    <script src="{{asset('js/boutique.js')}}"></script>

    <script type="text/javascript">
        var page = 1;
        var pages = Math.ceil({{ $counter }} / 6);

        $('.more-posts').on('click', function() {
                page++;
                loadMoreData(page, pages);
        });

        function loadMoreData(page, pages){
            $.ajax({
                url: '?page=' + page,
                type: 'get',
            }).done(function(data) {
                $(".category-news-list").append(data);
                equalheight('.latest-news-item:not(.slide-latest-news-item)');
                if (page == pages) {
                    $('.more-posts').remove();
                }
            }).fail(function(jqXHR, ajaxOptions, thrownError) {
                console.log('server not responding...');
            });
        }
    </script>
@stop