@foreach ($products as $product)
    <li class="latest-news-item product-item"> <!-- LATEST NEWS ITEM TEMPLATE -->
        <div class="latest-news-preview-img-block-outer product-image-preview">
                <a @if ($product->categories()->first())
                        href="{{  route('product.show', ['hierarchy' => $product->treeLink($product), 'slug' => $product->slug]) }}"
                   @else
                        href="#"
                   @endif
                   class="latest-news-preview-img-block-inner">

                @if ($product->thumb && file_exists(base_path() .'/public/img/uploads/shop/products/' . $product->id . '/thumbs/' . $product->thumb))
                    <img class="latest-news-preview-img" src="/img/uploads/shop/products/{!! $product->id !!}/thumbs/{!! $product->thumb !!}" alt="{{ $product->name }}">
                @else
                    <img class="latest-news-preview-img" src="/img/post-default.jpg" alt="{{ $product->title }}">
                @endif
            </a>
        </div>
        <a class="latest-news-category-link" href=""></a>
        <h3><a class="latest-news-item-heading" href="">{{ $product->name }}</a></h3>
        <h4><a class="latest-news-item-heading" href="">${{ $product->price }}</a></h4>
    </li>
@endforeach