@extends('front.template')

@section('title', $product->name . ' - Executive Shopper')
@section('keywords'){{ $product->keywords }}@stop
@section('description'){{ $product->description }}@stop

@section('main')
    <section class="main-content"> <!-- BODY -->
        @include('front.shop.navigation')

        <div class="main-content-footer row">
            <div class="small-images col-sm-1 col-xs-3 col-sm-offset-1">
                <ul class="img-preview">
                    <li class="active" data-hook="update-product-image">
                        @if ($product->thumb && file_exists(base_path() .'/public/img/uploads/shop/products/' . $product->id . '/thumbs/' . $product->thumb))
                            <img class="small-photo" src="/img/uploads/shop/products/{!! $product->id !!}/thumbs/{!! $product->thumb !!}" alt="{{ $product->name }}">
                        @else
                            <img class="small-photo" src="/img/post-default.jpg" alt="{{ $product->title }}">
                        @endif
                    </li>

                    @foreach($product->images()->get() as $image)
                         <li data-hook="update-product-image">
                             @if (file_exists(base_path() .'/public/img/uploads/shop/products/' . $product->id . '/images/' . $image->name))
                                 <img class="small-photo" src="/img/uploads/shop/products/{!! $product->id !!}/images/{!! $image->name !!}" alt="{{ $product->name }}">
                             @else
                                 <img class="small-photo" src="/img/post-default.jpg" alt="{{ $product->title }}">
                             @endif
                         </li>
                    @endforeach
                </ul>
            </div>

            <div class="middle-content col-sm-3 col-xs-9">
                <div class="photo-slider">
                    <i class="image-full-screen fa fa-arrows-alt" aria-hidden="true" data-toggle-modal="product-image-full-screen"></i>
                    <button class="navigation navigation-left" data-hook="product-image-navigation" data-navigate="prev"></button>
                    <button class="navigation navigation-right" data-hook="product-image-navigation" data-navigate="next"></button>
                    @if ($product->thumb && file_exists(base_path() .'/public/img/uploads/shop/products/' . $product->id . '/thumbs/' . $product->thumb))
                        <img id="main-image" src="/img/uploads/shop/products/{!! $product->id !!}/thumbs/{!! $product->thumb !!}" alt="{{ $product->name }}">
                    @else
                        <img id="main-image" src="/img/post-default.jpg" alt="{{ $product->title }}">
                    @endif
                </div>
            </div>

            <div class="article-info col-sm-7 col-xs-12">
                <form class="col-sm-6 col-xs-8" id="product" action="#" method="post">
                    <h1>{{ $product->name }}</h1>
                    <input type="hidden" name="product_id" value="{{ $product->id }}"/>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="select-box">
                        <p class="uppercase">{{ trans('front/product.color') }}</p>
                        <ul class="colours">
                            @foreach(explode(',', $product->colours) as $key => $color)
                                <li>
                                    <input id="color{{ $key }}" type="radio" name="color" value="{{ $color }}">
                                    <label for="color{{ $key }}" style="background-color: #{{ $color }};"></label>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="select-box">
                        <h3 class="size uppercase">{{ trans('front/product.size') }}</h3>
                        <ul class="size-list">
                            @foreach(explode(',', $product->sizes) as $key => $size)
                                <li>
                                    <input id="size{{ $key }}" type="radio" name="size" value="{{ $size }}">
                                    <label for="size{{ $key }}">{{ $size }}</label>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <a href="#" data-toggle="modal" data-target="#modal-buy-service">
                        <div class="add-to-bag">
                            {{ trans('front/product.buy') }}
                        </div>
                    </a>
                </form>

                <div class="price col-sm-2 col-xs-4">
                    <p class="number-1">${{ $product->price }}</p>
                    <p class="number-2">{{ $product->sku }}</p>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="article-about accordion col-sm-4 col-xs-12 col-sm-offset-2">
                <div class="accordion-item">
                    <h4 data-hook="accordion-toggle" class="accordion-h">{{ trans('front/product.video') }}<i class="fa" aria-hidden="true"></i></h4>

                    <div class="accordion-item-content video-content">
                        {!! $product->video !!}
                    </div>
                </div>
                <div class="accordion-item">
                    <h4 data-hook="accordion-toggle" class="accordion-h">{{ trans('front/product.size_fit') }}<i class="fa" aria-hidden="true"></i></h4>

                    <div class="accordion-item-content">
                        {!! $product->size_fit !!}
                    </div>
                </div>
                <div class="accordion-item closed">
                    <h4 data-hook="accordion-toggle" class="accordion-h">{{ trans('front/product.details') }}<i class="fa" aria-hidden="true"></i></h4>

                    <div class="accordion-item-content">
                        {!! $product->details  !!}
                    </div>
                </div>

                <div class="accordion-item closed">
                    <h4 data-hook="accordion-toggle" class="accordion-h">{{ trans('front/product.material_care') }}<i class="fa" aria-hidden="true"></i></h4>

                    <div class="accordion-item-content">
                        {!! $product->material_cafe !!}
                    </div>
                </div>
                <div class="accordion-item closed">
                    <h4 data-hook="accordion-toggle" class="accordion-h">{{ trans('front/product.delivery') }}<i class="fa" aria-hidden="true"></i></h4>

                    <div class="accordion-item-content">
                        {!! $product->delivery !!}
                    </div>
                </div>
            </div>
        </div>


        <div class="clearfix"></div>
    </section>

    <div data-modal="product-image-full-screen">
        <div class="modal-block-content">
            <i class="modal__close fa fa-times" aria-hidden="true" data-toggle-modal="product-image-full-screen"></i>
            <div class="middle-content col-sm-4 col-xs-4 col-sm-offset-4">
                <div class="photo-slider">
                    <button class="navigation navigation-left" data-hook="slider-image-navigation" data-navigate="prev"></button>
                    <button class="navigation navigation-right" data-hook="slider-image-navigation" data-navigate="next"></button>
                    @if ($product->thumb !== '' && file_exists(base_path() .'/public/img/uploads/shop/products/' . $product->id . '/thumbs/' . $product->thumb))
                        <img id="main-modal-image" src="/img/uploads/shop/products/{!! $product->id !!}/thumbs/{!! $product->thumb !!}" alt="{{ $product->name }}">
                    @else
                        <img id="main-modal-image" src="/img/post-default.jpg" alt="{{ $product->title }}">
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/boutique.css') }}"/>
    <script src="{{asset('js/boutique.js')}}"></script>

    <script>
        $('#buy-product-yes').on('click', function (event) {
            var product =  $('#product, #buy-product').serialize();
            var user = $('#buy-product').serialize();

            $this = $(this);

            $.ajax({
                url: "{{ route('product.buy') }}",
                type: "POST",
                data: product,
                success: function (data) {
                    console.log(data);
                    $this.siblings('button, form').remove();
                    $this.siblings('p').text('We will contact you soon');
                    $this.siblings('h4').text('Thank you');
                    $this.remove();
                },
                error: function (data) {
                    console.log(data);
                },
            },"json");
        });
    </script>
@stop

@section('footer')

        <div class="modal fade login-main-modal modal-ajax" tabindex="-1" role="dialog" id="modal-buy-service">  <!-- MODAL BUY SERVICE -->
            <div class="modal-dialog modal-md">
                <div class="modal-content modal-login-content">
                    <h4>{{trans('front/product.are-you-sure') }}</h4>
                    <p>{{trans('front/buy-service.your-data') }}</p>
                        {!! Form::open(['class' => 'buy-service-form', 'id' => 'buy-product']) !!}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        {!! Form::hidden('product', $product->name) !!}
                    @if (Auth::guest())
                        {!! Form::label('name', trans('front/buy-service.name'))!!}
                        {!! Form::text('name','',['required' => 'required']) !!}
                        {!! Form::label('email', trans('front/buy-service.email'))!!}
                        {!! Form::email('email', '', ['id' => 'email', 'required' => 'required'])!!}
                        {!! Form::label('phone', trans('front/buy-service.phone'))!!}
                        {!! Form::text('phone', '',['required' => 'required', 'required' => 'required'])!!}
                    @else
                        {!! Form::hidden('name', Auth::user()->firstname . ' ' . Auth::user()->lastname) !!}
                        {!! Form::hidden('email', Auth::user()->email)!!}
                        {!! Form::hidden('phone', Auth::user()->phone)!!}
                    @endif
                    {!! Form::close() !!}
                    <button class="delete-company-modal-buttons delete-company-modal-buttons-yes" type="button" id="buy-product-yes">{{trans('front/buy-service.yes') }}</button>
                    <button class="delete-company-modal-buttons delete-company-modal-buttons-no" type="button" data-dismiss="modal">{{trans('front/buy-service.no') }}</button>
                    <button class="close-modal-button" type="button" data-dismiss="modal"><img src="/img/icons/close-icon.svg" alt="Close modal"></button>
                </div>
            </div>
        </div>
@stop