@extends('front.template')

@section('title', "$vacancy->title" . ' - Executive Shopper')

@section('main')
    <section class="fjob-list-section fjob-single-section clearfix">
        <div class="clearfix">
            <div class="company-description-back">
                <a href="/{{App::getLocale()}}/fashion-job/" class="fjob-single-get-back-button"><div class="back-arrow"></div>{{trans('front/vacancy-show.get-back')}}</a>
            </div>
            <div class="job-single-add-vacancy clearfix">
                @if (Auth::check())
                    @if ($role == 'employer' || $role == 'redac' || $role == 'admin')
                        <a href="/{{App::getLocale()}}/fashion-job/create/" class="job-single-add-job">{{trans('front/vacancy-show.add-vacancy')}}</a>
                    @endif
                @else
                    <a href="/{{App::getLocale()}}/auth/register" class="job-single-add-job">{{trans('front/vacancy-show.add-vacancy')}}</a>
                @endif
            </div>
        </div>
        <div class="company-description clearfix">
            <div class="job-single-company-logo">
                @if ($vacancy->company->logo !== '' && file_exists(base_path() .'/public/img/uploads/companies/' . $vacancy->company->user_id . '/' . $vacancy->company->id . '/' . $vacancy->company->logo))
                    <img src="/img/uploads/companies/{{ $vacancy->company->user_id }}/{{ $vacancy->company->id }}/{{ $vacancy->company->logo }}" alt="{{ $vacancy->company->name }}">
                @else
                    <img src="/img/company_default.png" alt="{{ $vacancy->company->name}}">
                @endif
            </div>
            <h2 class="job-single-company-heading">{{ $vacancy->company->name }}</h2>
            <div class="job-single-company-text">
                <p>{{ $vacancy->company->description }}</p>
            </div>
            <span class="show-contacts">{{ trans('front/vacancy-show.get-contacts') }}</span>
            <ul class="contacts-list">
                <li class="animated fadeInUp">
                    <a href="#">{{ $vacancy->company->site }}</a>
                </li>
                <li class="animated fadeInUp">
                    <a href="mailto:#">{{ $vacancy->company->email }}</a>
                </li>
                <?php $numbers = explode(',', $vacancy->company->number); ?>
                @for ($i = 0; $i < count($numbers); $i++)
                    <li class="animated fadeInUp">
                        <a href="tel:{{ $numbers[$i] }}" class="job-phone">{{ $numbers[$i] }}</a>
                    </li>
                @endfor
            </ul>
        </div>
        <div class="job-single-content">
            <article class="job-single-article">
                <h1 class="job-single-heading">{{ $vacancy->title }}</h1>
                <div class="job-single-under-heading clearfix">
                    <span class="job-tag">{{ trans('front/vacancy-industry.' . $vacancy->industry->name) }}</span>
                    <span class="job-availibility-tag">{{ trans('front/vacancy-show.' . strtolower($vacancy->availability_id)) }}</span>
                    <div class="job-location-tag">
                        <span class="country-tag">{{ $vacancy->country }}</span>
                        <span class="city-tag">{{ $vacancy->city }}</span>
                    </div>
                    <span class="job-single-time">{{ date('d.m.Y', strtotime($vacancy->created_at)) }}</span>
                </div>
                <div class="job-single-text">
                    {!! $vacancy->description !!}
                </div>
                {!! Form::open(['method' => 'post', 'url' => 'send-cv', 'files' => 'true']) !!}
                    {!! Form::hidden('vacancy', $vacancy->title, '') !!}
                    {!! Form::hidden('company', $vacancy->company->name, '') !!}
                    {!! Form::hidden('email', $vacancy->company->email, '') !!}
                    @if (Auth::check())
                        <span class="send-cv-button" id="loadCVbutton">{{ trans('front/vacancy-show.load') }} CV<i class="fa fa-check"></i></span>
                        <input type="submit" value="{{ trans('front/vacancy-show.send') }} CV" class="send-cv-button">
                    @else
                        <a href="/{{App::getLocale()}}/auth/register"><span class="send-cv-button">{{ trans('front/vacancy-show.load') }} CV<i class="fa fa-check"></i></span></a>
                        <a href="/{{App::getLocale()}}/auth/register" class="send-cv-button">{{ trans('front/vacancy-show.send') }} CV</a>
                    @endif
                    <input type="file" class="cvinput" id="loadCV" name="loadCV" required="required">
                {!! Form::close() !!}
            </article>
        </div>
    </section>
@stop