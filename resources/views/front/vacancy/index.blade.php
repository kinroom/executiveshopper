@extends('front.template')

@section('title', trans('front/fashion-job.fashion-job') . ' - Executive Shopper')

@section('main')
    <section class="fjob-list-section clearfix">
        <div class="filter-block">
            <form action="#" class="filter-form" id="filter-form" method="get">
                <h3 class="filter-heading">{{trans('front/fashion-job.filter')}}</h3>
                <span class="close-filter-slider"><i class="fa fa-times"></i></span>
                <div class="filter-list-block">
                    <h4>{{trans('front/fashion-job.industry')}} <img src="/img/icons/filter-collapse-icon.svg" alt="Show filters"></h4>
                    <ul class="filter-list filter-list-industry">
                        @foreach ($industries as $industry)
                            <?php $first_word = explode(' ', $industry->name); $ind = $first_word[0]; ?>
                            <li class="clearfix">
                                {!! Form::checkbox("ind-$ind", "$industry->id", '', ['class' => 'checkbox-filter', 'id' => "ind-$ind"]) !!}
                                {!! Form::label("ind-$ind", trans('front/vacancy-industry.'.$industry->name)) !!}
                                <span class="filter-total-number">{{$industry->count}}</span>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="filter-list-block">
                    <h4>{{trans('front/fashion-job.category')}} <img src="/img/icons/filter-collapse-icon.svg" alt="Show filters"></h4>
                    <ul class="filter-list filter-list-category">
                        @foreach ($categories as $category)
                            <?php $first_word = explode(' ', $category->name); $cat = $first_word[0]; ?>
                            <li class="clearfix">
                                {!! Form::checkbox("filt-$cat", "$category->id", '', ['class' => 'checkbox-filter', 'id' => "filt-$cat"]) !!}
                                {!! Form::label("filt-$cat",trans('front/vacancy-category.'.$category->name)) !!}
                                <span class="filter-total-number">{{$category->count}}</span>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="filter-list-block">
                    <h4>{{trans('front/fashion-job.availability')}} <img src="/img/icons/filter-collapse-icon.svg" alt="Show filters"></h4>
                    <ul class="filter-list filter-list-availability">
                        <li class="clearfix">
                            {!! Form::checkbox('filter-freelance', '0', '', ['class' => 'checkbox-filter', 'id' => 'filter-freelance']) !!}
                            {!! Form::label('filter-freelance', trans('front/fashion-job.freelance')) !!}
                            <span class="filter-total-number">{{$freelance_count}}</span>
                        </li>
                        <li class="clearfix">
                            {!! Form::checkbox('filter-internship', '1', '', ['class' => 'checkbox-filter', 'id' => 'filter-internship']) !!}
                            {!! Form::label('filter-internship', trans('front/fashion-job.internship')) !!}
                            <span class="filter-total-number">{{$internship_count}}</span>
                        </li>
                    </ul>
                </div>
                <div class="filter-list-block">
                    <h4>{{trans('front/fashion-job.location')}} <img src="/img/icons/filter-collapse-icon.svg" alt="Show filters"></h4>
                    <ul class="filter-list filter-list-select">
                        <li class="clearfix">
                           <label for="filter-country">{{trans('front/fashion-job.country')}}</label>
                            <select name="filter-country" id="filter-country">
                                <option>{{trans('front/fashion-job.all')}}</option>
                                @foreach ($countries as $country)
                                    <option value="{{$country->country}}">{{$country->country}}</option>
                                @endforeach
                            </select>
                        </li>
                        <li class="clearfix">
                            <label for="city">{{trans('front/fashion-job.city')}}</label>
                            <select name="filter-city" id="filter-city">
                                <option>{{trans('front/fashion-job.all')}}</option>
                            </select>
                        </li>
                    </ul>
                </div>
                <div class="filter-list-block">
                    <h4>{{trans('front/fashion-job.selected-options')}}</h4>
                    <div class="selected-content">
                        <div class="clearfix">
                            <div class="selected-left">
                                <span>{{trans('front/fashion-job.industry')}}:</span>
                            </div>
                            <div class="selected-right selected-right-industry">
                                <span>{{trans('front/fashion-job.all')}}</span>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="selected-left">
                                <span>{{trans('front/fashion-job.category')}}:</span>
                            </div>
                            <div class="selected-right selected-right-category">
                                <span>{{trans('front/fashion-job.all')}}</span>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="selected-left">
                                <span>{{trans('front/fashion-job.availability')}}:</span>
                            </div>
                            <div class="selected-right selected-right-availability">
                                <span>{{trans('front/fashion-job.all')}}</span>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="selected-left">
                                <span>{{trans('front/fashion-job.country')}}:</span>
                            </div>
                            <div class="selected-right selected-right-country">
                                <span>{{trans('front/fashion-job.all')}}</span>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="selected-left">
                                <span>{{trans('front/fashion-job.city')}}:</span>
                            </div>
                            <div class="selected-right selected-right-city">
                                <span>{{trans('front/fashion-job.all')}}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <span class="clear-form clear-form-button">{{trans('front/fashion-job.clear-filter')}}</span>
            </form>
        </div>
        <div class="vacancies-block">
            <div class="vacancies-block-heading clearfix">
                <h1>{{trans('front/fashion-job.current-vacancies')}}</h1>
                <span class="results-vacancies"><span>{{$count_vacancy}}</span> {{trans('front/fashion-job.results')}}</span>
                {!! Form::open(array('url' => App::getLocale() . '/fashion-job/search', 'method' => 'post', 'class' => 'search-vacancies')) !!}
                @if (isset($search))
                    {!! Form::text('search', "$search" , ['id' => 'vacancy-search', 'placeholder' => trans('front/fashion-job.search-for-vacancies')]) !!}
                @else
                    {!! Form::text('search', '', ['id' => 'vacancy-search', 'placeholder' => trans('front/fashion-job.search-for-vacancies')]) !!}
                @endif
                <button type="submit"><i class="fa fa-search"></i></button>
                {!! Form::close() !!}
            </div>
            <div class="add-job-block clearfix">
                @if (Auth::check())
                    @if ($role == 'employer' || $role == 'redac' || $role == 'admin')
                        <button class="filter-block-button">{{trans('front/fashion-job.filter')}} <i class="fa fa-tasks"></i></button>
                        <a href="/{{App::getLocale()}}/fashion-job/create">{{trans('front/fashion-job.add-vacancy')}}</a>
                    @endif
                @else
                    <button class="filter-block-button">{{trans('front/fashion-job.filter')}} <i class="fa fa-tasks"></i></button>
                    <a href="/{{App::getLocale()}}/auth/register">{{trans('front/fashion-job.add-vacancy')}}</a>
                @endif

            </div>
            <div class="vacancies-list-wrapper">
                <ul class="vacancies-list">
                    @foreach($vacancies as $vacancy)
                        <li class="clearfix">
                            <a href="/{{ App::getLocale() }}/fashion-job/{{ $vacancy->id }}/" class="clearfix">
                                <div class="vacancies-left">
                                    @if ($vacancy->company->logo != '' && file_exists(base_path() .'/public/img/uploads/companies/' . $vacancy->company->user_id . '/' . $vacancy->company->id . '/' . $vacancy->company->logo))
                                        <img src="/img/uploads/companies/{{ $vacancy->company->user_id }}/{{ $vacancy->company->id }}/{{ $vacancy->company->logo }}" alt="{{ $vacancy->company->name }}">
                                    @else
                                        <img src="/img/company_default.png">
                                    @endif
                                </div>
                                <div class="vacancies-right clearfix">
                                    <div class="job-heading">
                                        <h2>{{ $vacancy->title }}</h2>
                                        <span class="job-company-name">{{ $vacancy->company->name }}</span>
                                        <span class="job-date">{!!  date('d.m.Y', strtotime($vacancy->created_at)) !!}</span>
                                    </div>
                                    <div class="job-text">
                                        <p>{{ $vacancy->description }}</p>
                                    </div>
                                    <div class="vacancy-bottom clearfix">
                                        <div class="clearfix">
                                            <div class="vacancy-bottom-left">
                                                <span>{{ trans('front/fashion-job.salary') }}:</span>
                                            </div>
                                            <div class="vacancy-bottom-right">
                                                <span>{{ $vacancy->salary }}</span>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                            <div class="vacancy-bottom-left">
                                                <span>{{ trans('front/fashion-job.availability') }}:</span>
                                            </div>
                                            <div class="vacancy-bottom-right">
                                                <span>{{ $vacancy->availability_id }}</span>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                            <div class="vacancy-bottom-left">
                                                <span>{{  trans('front/fashion-job.location') }}:</span>
                                            </div>
                                            <div class="vacancy-bottom-right">
                                                <span>{{ $vacancy->country }}</span>
                                                <span class="vacancy-bottom-city">{{ $vacancy->city }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>

            @if (isset($more_vacancies) && count($more_vacancies) > 0)
                <button class="more-posts more-vacancies filter-more" id='1'>
                    <span>{{ trans('front/fashion-job.click-to-see-more') }}</span>
                    <i class="fa fa-angle-down show-more-icon"></i>
                </button>
            @elseif (isset($more_search) && count($more_search) > 0)
                <a class="more-posts more-vacancies" href="#" id='1+{{ $search }}' onclick="searchVacancies(this.id)">
                    <span>{{ trans('front/fashion-job.click-to-see-more') }}</span>
                    <i class="fa fa-angle-down show-more-icon"></i>
                </a>
            @endif
        </div>
        <div class="filter-overlay"></div>
    </section>
@stop
