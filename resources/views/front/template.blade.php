<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
<head>
    <meta charset="UTF-8">

    <title>@yield('title')</title>
    <meta name="keywords" content="@yield('keywords')"/>
    <meta name="description" content="@yield('description')"/>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}" type="image/x-icon">

    <meta name="csrf-token" id="token" content="{{ csrf_token() }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css')}}"/>

    <script src="{{ asset('js/jquery-2.1.4.min.js') }}"></script>

    @yield('head')
</head>

<body>
<div class="main-wrapper animated fadeIn">
    <header> <!-- HEADER -->
        <div class="header--search-profile clearfix">
            <div class="mobile-menu">
                <i class="fa fa-bars"></i>
            </div>
            {!! Form::open(['url' => App::getLocale() . '/blog/search', 'method'=>'get','id' => 'search', 'autocomplete' => 'on']) !!}
                <label for="search-box"><i class="fa fa-search"></i></label>
                <input id="search-box" name="search" type="text" placeholder="{{ trans('front/navigation.search') }}">
                <input type="submit">
                <div class="search-underline"></div>
            {!! Form::close() !!}
            <div class="header--profile">
                @if(session('statut') == 'visitor')
                    <img src="{{asset('img/icons/profile-icon.svg')}}" alt="Profile">
                    <a type="button" data-toggle="modal" data-target=".login-modal" href="">{{ trans('front/auth.login') }}</a>
                    <span>/</span>
                    <a href="/{{App::getLocale()}}/auth/register">{{ trans('front/auth.register') }}</a>
                @else
                    @if(session('statut') == 'admin' || session('statut') == 'redac')
                        <li>
                            <a href="{!! route('admin.dashboard') !!}"><img src="{{asset('img/icons/profile-icon.svg')}}" alt="Profile">{{ trans('front/navigation.personal-profile') }}</a>
                        </li>
                    @else
                        <li>
                            <a href="/{{ App::getLocale() }}/profile"><img src="{{asset('img/icons/profile-icon.svg')}}" alt="Profile">{{ trans('front/navigation.personal-profile') }}</a>
                        </li>
                    @endif
                @endif
            </div>
        </div>

        <div class="menu-logo clearfix">
            <a class="header-logo" href="/">
                <img src="{{asset('img/logo.png')}}" alt="Executive Shopper Blog">
            </a>

            {{--*/ $actual_link = $_SERVER['REQUEST_URI'] /*--}}
            <nav class="header-nav">
                <ul class="clearfix">
                    <a class="top-menu-heading">
                        <img class="close-menu" src="/img/icons/close-icon.svg" alt="Close menu">{{ trans('front/navigation.menu') }}
                    </a>
                    <li>
                        <a @if (stripos($actual_link, 'articles')) class="active-menu" @endif href="#">{{ trans('front/navigation.blog') }}</a>
                        <ul class="main-submenu animated slideInDown clearfix">
                            @foreach ($blog_nav as $key => $nav)
                                @if ($key == 0)
                                    <li><a @if (stripos($actual_link, '/all')) class="active-submenu" @endif href='{{ route('category.show', 'all') }}'>{{ trans('front/category.all') }}</a></li>
                                @endif
                                <li><a @if (stripos($actual_link, '/' . $nav->name) && !stripos($actual_link, 'fashion-job')) class="active-submenu" @endif href='{{ route('category.show', $nav->name) }}'>{{ trans('front/category.'.$nav->name) }}</a></li>
                            @endforeach
                        </ul>
                        <i class="fa fa-angle-left"></i>
                    </li>
                    <li>
                        <a @if (stripos($actual_link, 'business')) class="active-menu" @endif href="#">{{ trans('front/navigation.business') }}</a>
                        <ul class="main-submenu animated slideInDown clearfix">
                            <li><a @if (stripos($actual_link, '/personal-shopping'))class="active-submenu" @endif href="{{ route('business.page', 'personal-shopping') }}">{{ trans('front/navigation.shopping') }}</a></li>
                            <li><a @if (stripos($actual_link, '/promotion')) class="active-submenu" @endif href="{{ route('business.page', 'promotion') }}">{{ trans('front/navigation.promotion') }}</a></li>
                            <li><a @if (stripos($actual_link, '/about-us')) class="active-submenu" @endif href="{{ route('business.page', 'about-us') }}">{{ trans('front/navigation.about') }}</a></li>
                            <li><a @if (stripos($actual_link, '/work-with-us')) class="active-submenu" @endif href="{{ route('business.page', 'worj') }}">{{ trans('front/navigation.work') }}</a></li>
                        </ul>
                        <i class="fa fa-angle-left"></i>
                    </li>
                    <li class="no-submenu">
                        <a @if (stripos($actual_link, '/boutique')) class="active-menu" @endif href="{{ route('boutique', 'all') }}">{{ trans('front/navigation.boutique') }}</a>
                    </li>
                    <li class="no-submenu">
                        <a @if (stripos($actual_link, '/fashion-job')) class="active-menu" @endif href="/{{App::getLocale()}}/fashion-job/">{{ trans('front/navigation.fashion-job') }}</a>
                    </li>
                    <li><h3>{{ trans('front/navigation.language') }}</h3></li>
                    {{--*/ $i = 0; /*--}}
                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                        <li rel="alternate" hreflang="{{$localeCode}}"  class="lang @if (App::getLocale() == $localeCode )lang-active @endif  @if ($i == 0) lang-first @endif"><a href="{{LaravelLocalization::getLocalizedURL($localeCode) }}">{{$localeCode}}</a></li>
                        {{--*/ $i += 1; /*--}}
                    @endforeach
                </ul>
            </nav>
        </div>
        <div class="under-menu clearfix">
        </div>
        @yield('header')
    </header>

    @yield('main')

    @if(session()->has('ok'))
        @include('partials/error', ['type' => 'success', 'message' => session('ok')])
    @endif

    @if(isset($info))
        @include('partials/error', ['type' => 'info', 'message' => $info])
    @endif
</div>
<footer> <!-- FOOTER -->
    <div id="trapezoid"></div><div id="trapezoid2"></div>
    <div class="follow-us-wrapper">
        <ul>
            <li>
                <a href="https://www.facebook.com/exeshopper/">
                    <div class="footer-icon">
                        <img src="/img/icons/fb-icon.svg" alt="Facebook">
                    </div>
                    <span>Facebook</span>
                </a>
            </li>
            <li>
                <a href="https://www.youtube.com/channel/UChVKDmfRVpJb-Np7hHLOsQQ">
                    <div class="footer-icon">
                        <i class="fa fa-youtube" style="font-size:48px"></i>
                    </div>
                    <span>Youtube</span>
                </a>
            </li>
            <li>
                <a href="https://www.pinterest.com/exeshopper/">
                    <div class="footer-icon">
                        <img src="/img/icons/pinterest-icon.svg" alt="Pinterest">
                    </div>
                    <span>Pinterest</span>
                </a>
            </li>
            <li>
                <a href="https://www.instagram.com/executive.shopper/">
                    <div class="footer-icon">
                        <img src="/img/icons/insta-icon.svg" alt="Instagram">
                    </div>
                    <span>Instagram</span>
                </a>
            </li>
        </ul>
        <hr class="footer-divider">
        <div class="footer-text clearfix">
            <div class="footer-text-left">
                <a href="/" class="footer-home-link"><img src="/img/logo-footer.png" alt="Executive shopper"></a>
                <div class="footer-text-inline">
                    <span>&copy; 2015, Executive shopper, Inc. All rights reserved</span>
                </div>
                <div class="footer-text-inline">
                    <a href="#">{{ trans('front/footer.about-company') }}</a>
                    <br>
                    <a href="#">{{ trans('front/footer.how-it-works') }}</a>
                </div>
                <div class="footer-text-inline">
                    <a href="#">{{ trans('front/footer.advertisement') }}</a>
                    <br>
                    <a href="#">{{ trans('front/footer.ask-me-anything') }}</a>
                </div>
            </div>
            <div class="footer-text-right clearfix">
                <span>{{ trans('front/footer.email-subscribe') }}</span>
                <form action="/{{App::getLocale()}}/subscribe" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input class="footer-email" type="email" name="email" placeholder="{{ trans('front/footer.enter-your-email') }}">
                    <input class="footer-submit" type="submit" value="{{ trans('front/footer.subscribe') }}">
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade login-modal login-main-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-sm">
            <div class="modal-content modal-login-content">
                <h4>{{trans('front/auth.login')}}</h4>
                {!! Form::open(array('url' => '/auth/login')) !!}
                    <input type="hidden" value="{{App::getLocale()}}" id="locale"/>
                    {!! Form::label('login', trans('front/auth.email')) !!}
                    {!! Form::text('login', '', ['id' => 'login-text', 'autocomplete' => 'off'] ) !!}
                    {!! Form::label('login', trans('front/auth.password')) !!}
                    {!! Form::password('password', ['id' => 'login-password'] ) !!}
                    <div class="login-modal-buttons clearfix">
                        <button class="login-button" type="submit">{{ trans('front/auth.login') }}</button>
                    </div>
                    <h5>{{trans('front/auth.login') }}<br><span>{{trans('front/auth.with-social-networks') }}</span></h5>
                    <div class="modal-social-login">
                        <a href="/{{App::getLocale()}}/vkontakte"><i class="fa fa-vk"></i></a>
                        <a href="/facebook"><i class="fa fa-facebook"></i></a>
                        <a href="/google"><i class="fa fa-google"></i></a>
                        <a href="/twitter"><i class="fa fa-twitter"></i></a>
                    </div>
                {!! Form::close() !!}
                <button class="close-modal-button" type="button" data-dismiss="modal">
                    <img src="{{asset('img/icons/close-icon.svg')}}" alt="Close modal">
                </button>
            </div>
        </div>
    </div>
    @yield('footer')
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/scripts.js')}}"></script>
    <script src="{{asset('js/smoothscroll.js')}}"></script>

    <script src="{{asset('js/ajax.js')}}"></script>
</footer>
    @yield('scripts')
</body>
</html>
