@extends('front.template')

@section('keywords'){{ $post->keywords }}@stop
@section('description'){{ $post->description }}>@stop


@section('title', $post->title . ' - Executive Shopper')

@section('main')
    <div class="latest-news-section clearfix">

        @if ($post->category->name !== 'video')
            <div class="post-main-img">
                @if ($post->thumb != '' && file_exists(base_path() .'/public//img/uploads/thumbs/fourth/' . $post->user_id . '/' . $post->thumb))
                    <img src="/img/uploads/thumbs/fourth/{!! $post->user->id !!}/{!! $post->thumb !!}" alt="{{ $post->title }}">
                @else
                    <img src="/img/post-default.jpg" alt="{{ $post->title }}">
                @endif
            </div>
        @endif

        <div class="blog-article-section">
            <article class="blog-article">
                <a class="blog-article-category-link" href="{{ route('category.show', $post->category->name) }}">{{ trans('front/category.' . $post->category->name) }}</a>
                <h1 class="blog-article-title">{{ $post->title }}</h1>
                <div class="blog-article-author-share">
                    <div class="blog-article-author-share-date-author">
                        <span class="latest-news-date"> {!! $post->created_at !!}</span>
                        <span class="latest-news-author">{{ trans('front/post.by') }} <a>{{ $post->user->firstname }} {{ $post->user->lastname }}</a></span>
                    </div>
                    <div class="blog-share-post">
                        <div class="blog-share-dots">
                            <i class="fa fa-circle"></i>
                            <i class="fa fa-circle"></i>
                            <i class="fa fa-circle"></i>
                        </div>
                        <span>{{ trans('front/post.share') }}</span>
                        <a class="blog-social-icons" href="/{{App::getLocale()}}/share-twitter/{{ $post->category->name }}/{{$post->slug}}/{{$post->title}}" target="blank">
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a class="blog-social-icons" href="/{{App::getLocale()}}/share-pinterest/{{ $post->category->name }}/{{$post->slug}}/{{$post->title}}" target="blank">
                            <i class="fa fa-pinterest"></i>
                        </a>
                        <a class="blog-social-icons" href="/{{App::getLocale()}}/share-facebook/{{ $post->category->name }}/{{$post->slug}}/{{$post->title}}" target="blank">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </div>
                </div>

                @if (($post->video !== ''))
                    <div class="blog-video-wrapper">
                        {{ $post->video }}
                    </div>
                @endif

                <div class="clearfix">
                    {!! $post->content !!}
                </div>
                @if (($post->is_original == 0 && isset($translator)))
                    <div class="under-blog-article">
                        <span class="under-blog-1">{{ trans('front/post.translator') }}</span>
                        <span class="under-blog-2">{{ $translator->firstname }} {{ $translator->lastname }}</span>
                    </div>
                @endif

                @if (($post->photographer != ''))
                    <div class="under-blog-article">
                        <span class="under-blog-1">{{trans('front/post.photographer') }}</span>
                        <span class="under-blog-2">{{ $post->photographer }}</span>
                    </div>
                @endif

                @if (($post->styling != ''))
                    <div class="under-blog-article">
                        <span class="under-blog-1">{{ trans('front/post.styling-makeup') }}</span>
                        <span class="under-blog-2">{{ $post->styling }}</span>
                    </div>
                @endif

                @if (($post->outfit != ''))
                    <div class="under-blog-article">
                        <span class="under-blog-1">{{ trans('front/post.outfit') }}</span>
                        <span class="under-blog-2">{{ $post->outfit }}</span>
                    </div>
                @endif

                @if (($post->location != ''))
                    <div class="under-blog-article">
                        <span class="under-blog-1">{{ trans('front/post.location') }}</span>
                        <span class="under-blog-2">{{ $post->location }}</span>
                    </div>
                @endif

                @if (($post->website != ''))
                    <div class="under-blog-article">
                        <span class="under-blog-1">{{ trans('front/post.website') }}</span>
                        <span class="under-blog-2">{{ $post->website }}</span>
                    </div>
                @endif

                @if (($post->is_commercial))
                    <div class="blog-buy">
                        <h2>{{ trans('front/post.also-buy') }}</h2>
                        <span class="promo-how">{{ trans('front/post.just-click') }}<span>{{ trans('front/post.about-how') }}</span></span>
                        <div class="blog-buy-divider"></div>
                        @if (Auth::guest())
                            <a href="#" class="button-buy-service" data-toggle="modal" data-target="#modal-buy-service-not-registered">{{trans('front/post.buy-service') }}</a>
                        @else
                            <a href="#" class="button-buy-service" data-toggle="modal" data-target="#modal-buy-service">{{trans('front/post.buy-service') }}</a>
                        @endif
                    </div>
                @endif

                <div class="blog-tags">
                    @if($post->tags->count())
                        @if($post->tags->count() > 0)
                            @foreach($post->tags as $tag)
                                <a href="/{{App::getLocale()}}/blog/tag?tag={!! $tag->id !!}" class="blog-tag">{!! $tag->tag !!}</a>
                            @endforeach
                        @endif
                    @endif
                </div>

                <div class="blog-comments">
                    <div id="disqus_thread"></div>
                    <script>
                        (function() { // DON'T EDIT BELOW THIS LINE
                            var d = document, s = d.createElement('script');

                            s.src = '//burbelo.disqus.com/embed.js';

                            s.setAttribute('data-timestamp', +new Date());
                            (d.head || d.body).appendChild(s);
                        })();
                    </script>
                    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
                </div>
            </article>


            <!-- NEXT AND PREVIOUS POST -->
            <section class="next-prev-posts clearfix">
                <div class="next-prev-posts-50 clearfix">
                    @if (!$previous_post->isEmpty())
                        <div class="blog-post-preview-left">
                            @if ($previous_post[0]['thumb'] !== '' && file_exists(base_path() .'/public/img/uploads/thumbs/first/' . $previous_post[0]['user_id'] . '/' . $previous_post[0]['thumb']))
                                <img src="/img/uploads/thumbs/first/{{$previous_post[0]['user_id']}}/{{$previous_post[0]['thumb']}}" alt="{{ $previous_post[0]['title'] }}">
                            @else
                                <img src="/img/post-default-mini.jpg" alt="{{ $previous_post[0]['title'] }}">
                            @endif
                        </div>
                        <div class="next-prev-posts-50-left-text">
                            <a class="next-prev-posts-50-left-text-category" href="{{ route('category.show', $previous_post[0]->category->name) }}">{{trans('front/category.' . $previous_post[0]->category->name) }}</a>
                            <h2><a href="{{ route('articles.post.show', [$previous_post[0]->category->name, $previous_post[0]->slug]) }}">{{ $previous_post[0]['title'] }}</a></h2>
                            <a href="{{ route('articles.post.show', [$previous_post[0]->category->name, $previous_post[0]->slug]) }}" class="previous-news-link"><i class="fa fa-chevron-circle-left"></i>{{trans('front/post.previous-news') }}</a>
                            <div class="next-prev-posts-50-left-divider"></div>
                        </div>
                    @endif
                </div>

                <div class="next-prev-posts-50 clearfix">
                    @if (!$next_post->isEmpty())
                        <div class="blog-post-preview-right">
                            @if ($next_post[0]['thumb'] !== '' && file_exists(base_path() .'/public/img/uploads/thumbs/first/' . $next_post[0]['user_id'] . '/' . $next_post[0]['thumb']))
                                <img src="/img/uploads/thumbs/first/{{$next_post[0]['user_id']}}/{{$next_post[0]['thumb']}}" alt="{{ $next_post[0]['title'] }}">
                            @else
                                <img src="/img/post-default-mini.jpg" alt="{{ $next_post[0]['title'] }}">
                            @endif
                        </div>
                        <div class="next-prev-posts-50-right-text">
                            <a class="next-prev-posts-50-left-text-category" href="{{ route('category.show', $next_post[0]->category->name) }}">{{trans('front/category.' . $next_post[0]->category->name) }}</a>
                            <h2><a href="{{ route('articles.post.show', [$next_post[0]->category->name, $next_post[0]->slug]) }}">{{ $next_post[0]['title'] }}</a></h2>
                            <a href="{{ route('articles.post.show', [$next_post[0]->category->name, $next_post[0]->slug]) }}" class="next-news-link">{{trans('front/post.next-news') }}<i class="fa fa-chevron-circle-right"></i></a>
                        </div>
                    @endif
                </div>
            </section>

            @if (count($more_articles) > 0)
                <!-- MORE ARTICLES BLOCK -->
                <section class="blog-more-articles">
                    <span class="blog-more-articles-heading">{{trans('front/post.more-articles') }}</span>
                    <ul class="clearfix">
                        @foreach($more_articles as $more_article)
                            <li class="latest-news-item"> <!-- LATEST NEWS ITEM TEMPLATE -->
                                <div class="latest-news-preview-img-block-outer">
                                    <a href="{{ route('articles.post.show', [$more_article->category->name, $more_article->slug]) }}" class="latest-news-preview-img-block-inner">
                                        @if ($more_article->thumb !== '' && file_exists(base_path() .'/public/img/uploads/thumbs/first/' . $more_article->user_id . '/' . $more_article->thumb))
                                            <img class="latest-news-preview-img" src="/img/uploads/thumbs/first/{{$more_article->user_id}}/{{ $more_article->thumb }}" alt="{{ $more_article->thumb }}">
                                        @else
                                            <img class="latest-news-preview-img" src="/img/post-default-feautered.jpg" alt="{{ $more_article->thumb }}">
                                        @endif
                                            <div class="latest-news-preview-img-overlay">
                                            <span>{{trans('front/post.read-article') }}</span>
                                            <div class="read-article-divider"></div>
                                        </div>
                                    </a>
                                </div>
                                <a class="latest-news-category-link" href="{{ route('category.show', $more_article->category->name) }}">{{trans('front/category.' . $more_article->category->name) }}</a>
                                <h3><a class="latest-news-item-heading" href="{{ route('articles.post.show', [$more_article->category->name, $more_article->slug]) }}"> {!! $more_article->title !!}<div class="news-underline"></div></a></h3>
                                <span class="latest-news-date"> {!! $more_article->created_at !!}</span>
                            </li>
                        @endforeach
                    </ul>
                </section>
            @endif
        </div>

            @if ((count($latest) == 3) && (count($most_viewed) == 3))
        <!--  RIGHT POSTS TEMPLATE -->
        <section class="blog-article-right">
            <nav class="blog-sidebar clearfix">
                <div class="sidebar-link sidebar-link-active">{{ trans('front/post.latest') }}</div>
                <div class="sidebar-link">{{ trans('front/post.most-viewed') }}</div>
            </nav>
            <div class="sidebar-slider">
                <div class="sidebar-slider-container clearfix">
                    <div class="sidebar-slide">

                        <!-- LATEST NEWS ITEM TEMPLATE -->
                        <div class="latest-news-item slide-latest-news-item"> <!-- LATEST NEWS ITEM TEMPLATE -->
                            <div class="latest-news-preview-img-block-outer">
                                <a href="{{ route('articles.post.show', [$latest[0]->category->name, $latest[0]->slug]) }}" class="latest-news-preview-img-block-inner">
                                    @if ($latest[0]['thumb'] !== '' && file_exists(base_path() .'/public/img/uploads/thumbs/first/' . $latest[0]['user_id'] . '/' . $latest[0]['thumb']))
                                        <img class="latest-news-preview-img" src="/img/uploads/thumbs/first/{{ $latest[0]['user_id'] }}/{{ $latest[0]['thumb'] }}" alt="{{$latest[0]['title']}}">
                                    @else
                                        <img class="latest-news-preview-img" src="/img/post-default-feautered.jpg" alt="{{$latest[0]['title']}}">
                                    @endif
                                    <div class="latest-news-preview-img-overlay">
                                        <span>{{trans('front/post.read-article') }}</span>
                                        <div class="read-article-divider"></div>
                                    </div>
                                </a>
                            </div>
                            <div class="sidebar-latest-wrapper-text">
                                <a class="latest-news-category-link" href="{{ route('category.show', $latest[0]->category->name) }}">{{trans('front/category.' . $latest[0]->category->name) }}</a>
                                <h3><a class="latest-news-item-heading" href="{{ route('articles.post.show', [$latest[0]->category->name, $latest[0]->slug]) }}">{!! $latest[0]['title'] !!}<div class="news-underline"></div></a></h3>
                                <p>{!! $latest[0]['content'] !!}</p>
                                <span class="latest-news-date">{!! $latest[0]['created_at'] !!}</span>
                                <span class="latest-news-author">{{trans('front/post.by') }} <a>{{ $latest[0]->user->firstname . ' ' . $latest[0]->user->lastname }}</a></span>
                                <div class="likes-views-buttons">
                                    <button onclick="Like({{ $latest[0]['id'] }}, 'daily_')">
                                        <i class="fa fa-heart"></i>
                                        <span id="daily_{{ $latest[0]['id'] }}">{{ $latest[0]['likes'] }}</span>
                                    </button>
                                    <span class="views-number">
                                        <i class="fa fa-eye"></i>
                                        {{ $latest[0]['views'] }}
                                    </span>
                                </div>
                            </div>
                        </div>
                        <ul class="sidebar-last-two">
                            <li class="clearfix">
                                <a href="{{ route('articles.post.show', [$latest[1]->category->name, $latest[1]->slug]) }}" class="last-two-img">
                                    @if ($latest[1]['thumb'] !== '' && file_exists(base_path() .'/public/img/uploads/thumbs/first/' . $latest[1]['user_id'] . '/' . $latest[1]['thumb']))
                                        <img class="latest-news-preview-img" src="/img/uploads/thumbs/first/{{ $latest[1]['user_id'] }}/{{ $latest[1]['thumb'] }}" alt="{{$latest[1]['title']}}">
                                    @else
                                        <img class="latest-news-preview-img" src="/img/post-default-feautered.jpg" alt="{{$latest[1]['title']}}">
                                    @endif
                                </a>
                                <div class="last-two-text">
                                    <a href="{{ route('category.show', $latest[1]->category->name) }}" class="last-two-category-link">{{trans('front/category.' . $latest[1]->category->name) }}</a>
                                    <h3><a href="{{ route('articles.post.show', [$latest[1]->category->name, $latest[1]->slug]) }}">{!! $latest[1]['title'] !!}</a></h3>
                                    <span class="latest-news-date">{!! $latest[1]['created_at'] !!}</span>
                                    <span class="latest-news-author">{{trans('front/post.by') }} <a>{{ $latest[1]->user->firstname . ' ' . $latest[1]->user->lastname }}</a></span>
                                    <div class="likes-views-buttons">
                                        <button onclick="Like({{ $latest[1]['id'] }}, 'daily_')">
                                            <i class="fa fa-heart"></i>
                                            <span id="daily_{{ $latest[1]['id'] }}">{{ $latest[1]['likes'] }}</span>
                                        </button>
                                    <span class="views-number">
                                        <i class="fa fa-eye"></i>
                                        {{ $latest[1]['views'] }}
                                    </span>
                                    </div>
                                </div>
                            </li>
                            <li class="clearfix">
                                <a href="{{ route('articles.post.show', [$latest[2]->category->name, $latest[0]->slug]) }}" class="last-two-img">
                                    @if ($latest[2]['thumb'] !== '' && file_exists(base_path() .'/public/img/uploads/thumbs/first/' . $latest[2]['user_id'] . '/' . $latest[2]['thumb']))
                                        <img class="latest-news-preview-img" src="/img/uploads/thumbs/first/{{ $latest[2]['user_id'] }}/{{ $latest[2]['thumb'] }}" alt="{{$latest[2]['title']}}">
                                    @else
                                        <img class="latest-news-preview-img" src="/img/post-default-feautered.jpg" alt="{{$latest[2]['title']}}">
                                    @endif
                                </a>
                                <div class="last-two-text">
                                    <a href="{{ route('category.show', $latest[2]->category->name) }}" class="last-two-category-link">{{trans('front/category.' . $latest[2]->category->name) }}</a>
                                    <h3><a href="{{ route('articles.post.show', [$latest[2]->category->name, $latest[0]->slug]) }}">{!! $latest[2]['title'] !!}</a></h3>
                                    <span class="latest-news-date">{!! $latest[2]['created_at'] !!}</span>
                                    <span class="latest-news-author">{{trans('front/post.by') }} <a>{{$latest[2]->user->firstname . ' ' . $latest[2]->user->lastname }}</a></span>
                                    <div class="likes-views-buttons">
                                        <button onclick="Like({{ $latest[2]['id'] }}, 'daily_')">
                                            <i class="fa fa-heart"></i>
                                            <span id="daily_{{ $latest[2]['id'] }}">{{ $latest[2]['likes'] }}</span>
                                        </button>
                                    <span class="views-number">
                                        <i class="fa fa-eye"></i>
                                        {{ $latest[2]['views'] }}
                                    </span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <!-- MOST VIEWED ITEM TEMPLATE -->
                    <div class="sidebar-slide">
                        <div class="latest-news-item slide-latest-news-item"> <!-- LATEST NEWS ITEM TEMPLATE -->
                            <div class="latest-news-preview-img-block-outer">
                                <a href="{{ route('articles.post.show', [$most_viewed[0]->category->name, $most_viewed[0]->slug]) }}" class="latest-news-preview-img-block-inner">
                                    @if ($most_viewed[0]['thumb'] !== '' && file_exists(base_path() .'/public/img/uploads/thumbs/first/' . $most_viewed[0]['user_id'] . '/' . $most_viewed[0]['thumb']))
                                        <img class="latest-news-preview-img" src="/img/uploads/thumbs/first/{{ $most_viewed[0]['user_id'] }}/{{ $most_viewed[0]['thumb'] }}" alt="{{$most_viewed[0]['title']}}">
                                    @else
                                        <img class="latest-news-preview-img" src="/img/post-default-feautered.jpg" alt="{{ $most_viewed[0]['title'] }}">
                                    @endif
                                    <div class="latest-news-preview-img-overlay">
                                        <span>{{trans('front/post.read-article') }}</span>
                                        <div class="read-article-divider"></div>
                                    </div>
                                </a>
                            </div>
                            <div class="sidebar-latest-wrapper-text">
                                <a class="latest-news-category-link" href="{{ route('category.show', $most_viewed[0]->category->name) }}">{{trans('front/category.' . $most_viewed[0]->category->name) }}</a>
                                <h3><a class="latest-news-item-heading" href="{{ route('articles.post.show', [$most_viewed[0]->category->name, $most_viewed[0]->slug]) }}">{{ $most_viewed[0]['title'] }}<div class="news-underline"></div></a></h3>
                                <p>{{ $most_viewed[0]['content'] }}</p>
                                <span class="latest-news-date">{{ $most_viewed[0]['created_at'] }}</span>
                                <span class="latest-news-author">{{trans('front/post.by') }} <a>{{ $most_viewed[0]->user->firstname . ' ' . $most_viewed[0]->user->lastname }}</a></span>
                                <div class="likes-views-buttons">
                                    <button onclick="Like({{ $most_viewed[0]['id'] }}, 'daily_')">
                                        <i class="fa fa-heart"></i>
                                        <span id="daily_{{ $most_viewed[0]['id'] }}">{{ $most_viewed[0]['likes'] }}</span>
                                    </button>
                                    <span class="views-number">
                                        <i class="fa fa-eye"></i>
                                        {{ $most_viewed[0]['views'] }}
                                    </span>
                                </div>
                            </div>
                        </div>
                        <ul class="sidebar-last-two">
                            <li class="clearfix">
                                <a href="{{ route('articles.post.show', [$most_viewed[1]->category->name, $most_viewed[1]->slug]) }}" class="last-two-img">
                                    @if ($most_viewed[1]['thumb'] !== '' && file_exists(base_path() .'/public/img/uploads/thumbs/first/' . $most_viewed[1]['user_id'] . '/' . $most_viewed[1]['thumb']))
                                        <img class="latest-news-preview-img" src="/img/uploads/thumbs/first/{{ $most_viewed[1]['user_id'] }}/{{ $most_viewed[1]['thumb'] }}" alt="{{$most_viewed[1]['title']}}">
                                    @else
                                        <img class="latest-news-preview-img" src="/img/post-default-feautered.jpg" alt="{{$most_viewed[1]['title']}}">
                                    @endif
                                </a>
                                <div class="last-two-text">
                                    <a href="{{ route('category.show', $most_viewed[1]->category->name) }}" class="last-two-category-link">{{trans('front/category.' . $most_viewed[1]->category->name) }}</a>
                                    <h3><a href="{{ route('articles.post.show', [$most_viewed[1]->category->name, $most_viewed[1]->slug]) }}">{{ $most_viewed[1]['title'] }}</a></h3>
                                    <span class="latest-news-date">{{ $most_viewed[1]['created_at'] }}</span>
                                    <span class="latest-news-author">{{trans('front/post.by') }} <a>{{ $most_viewed[1]->user->firstname . ' ' . $most_viewed[1]->user->lastname }}</a></span>
                                    <div class="likes-views-buttons">
                                        <button onclick="Like({{ $most_viewed[1]['id'] }}, 'daily_')">
                                            <i class="fa fa-heart"></i>
                                            <span id="daily_{{ $most_viewed[1]['id'] }}">{{ $most_viewed[1]['likes'] }}</span>
                                        </button>
                                    <span class="views-number">
                                        <i class="fa fa-eye"></i>
                                        {{ $most_viewed[1]['views'] }}
                                    </span>
                                    </div>
                                </div>
                            </li>
                            <li class="clearfix">
                                <a href="{{ route('articles.post.show', [$most_viewed[2]->category->name, $most_viewed[2]->slug]) }}" class="last-two-img">
                                    @if ($most_viewed[2]['thumb'] !== '' && file_exists(base_path() .'/public/img/uploads/thumbs/first/' . $most_viewed[2]['user_id'] . '/' . $most_viewed[2]['thumb']))
                                        <img class="latest-news-preview-img" src="/img/uploads/thumbs/first/{{ $most_viewed[2]['user_id'] }}/{{ $most_viewed[2]['thumb'] }}" alt="{{$most_viewed[2]['title']}}">
                                    @else
                                        <img class="latest-news-preview-img" src="/img/post-default-feautered.jpg" alt="{{$most_viewed[2]['title']}}">
                                    @endif
                                </a>
                                <div class="last-two-text">
                                    <a href="{{ route('category.show', $most_viewed[2]->category->name) }}" class="last-two-category-link">{{trans('front/category.' . $most_viewed[2]->category->name) }}</a>
                                    <h3><a href="{{ route('articles.post.show', [$most_viewed[2]->category->name, $most_viewed[2]->slug]) }}">{{ $most_viewed[2]['title'] }}</a></h3>
                                    <span class="latest-news-date">{{ $most_viewed[2]['created_at'] }}</span>
                                    <span class="latest-news-author">{{trans('front/post.by') }} <a>{{ $most_viewed[2]->user->firstname . ' ' . $most_viewed[2]->user->lastname }}</a></span>
                                    <div class="likes-views-buttons">
                                        <button onclick="Like({{ $most_viewed[2]['id'] }}, 'daily_')">
                                            <i class="fa fa-heart"></i>
                                            <span id="daily_{{ $most_viewed[2]['id'] }}">{{ $most_viewed[2]['likes'] }}</span>
                                        </button>
                                    <span class="views-number">
                                        <i class="fa fa-eye"></i>
                                        {{ $most_viewed[2]['views'] }}
                                    </span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            @endif
        </section>
    </div>
@stop

@section('footer')

    @if (Auth::guest())
        <div class="modal fade login-main-modal modal-ajax" tabindex="-1" role="dialog" id="modal-buy-service-not-registered">  <!-- MODAL BUY SERVICE FORM-->
            <div class="modal-dialog modal-sm">
                <div class="modal-content modal-login-content">
                    <h4>{{trans('front/buy-service.send-request') }}</h4>
                    <p>{{trans('front/buy-service.description') }}</p>
                    {!! Form::open(['class' => 'buy-service-form', 'id' => 'buy-service']) !!}
                        {!! Form::hidden('post', "$post->title") !!}
                        {!! Form::label('name', trans('front/buy-service.name'))!!}
                        {!! Form::text('name','',['required' => 'required']) !!}
                        {!! Form::label('email', trans('front/buy-service.email'))!!}
                        {!! Form::email('email', '', ['id' => 'email'])!!}
                        {!! Form::label('phone', trans('front/buy-service.phone'))!!}
                        {!! Form::text('phone', '',['required' => 'required'])!!}
                    {!! Form::close() !!}
                    <button class="delete-company-modal-buttons delete-company-modal-buttons-yes" type="button" id="buy-service-send">{{trans('front/buy-service.send-request') }}</button>
                </div>
            </div>
        </div>
    @else
        <div class="modal fade login-main-modal modal-ajax" tabindex="-1" role="dialog" id="modal-buy-service">  <!-- MODAL BUY SERVICE -->
            <div class="modal-dialog modal-md">
                <div class="modal-content modal-login-content">
                    {!! Form::open(['class' => 'buy-service-form', 'id' => 'buy-service']) !!}
                        {!! Form::hidden('post', "$post->title") !!}
                        {!! Form::hidden('name', Auth::user()->firstname . ' ' . Auth::user()->lastname) !!}
                        {!! Form::hidden('email', Auth::user()->email)!!}
                        {!! Form::hidden('phone', Auth::user()->phone)!!}
                    {!! Form::close() !!}
                    <h4>{{trans('front/buy-service.are-you-sure') }}</h4>
                    <p>{{trans('front/buy-service.your-data') }}</p>
                    <button class="delete-company-modal-buttons delete-company-modal-buttons-yes" type="button" id="buy-service-yes">{{trans('front/buy-service.yes') }}</button>
                    <button class="delete-company-modal-buttons delete-company-modal-buttons-no" type="button" data-dismiss="modal">{{trans('front/buy-service.no') }}</button>
                    <button class="close-modal-button" type="button" data-dismiss="modal"><img src="/img/icons/close-icon.svg" alt="Close modal"></button>
                </div>
            </div>
        </div>
    @endif
@stop
