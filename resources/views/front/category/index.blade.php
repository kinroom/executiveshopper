@extends('front.template')

@section('title', trans('front/category.' . $categoryName). ' ' . trans('front/blog.news') . ' - Executive Shopper')

@section('main')
    <section class="latest-news-section clearfix"> <!-- CATEGORY NEWS SECTION -->
        @if ($categoryName !== 'all' && isset($category))
            <h1 class="category-heading">{{ trans('front/category.' . $category->name) }} {{ trans('front/blog.news') }}</h1> <!-- CATEGORY HEADING -->
            <div class="category-annotation"> <!-- CATEGORY TEXT -->
                <p>{{ $category->description }}</p>
            </div>
            <div class="category-nav clearfix"> <!-- CATEGORY NUMBER OF POSTS AND NAV -->
                <div class="category-nav-inner-wrapper">
                    <div class="articles-number">
                        <span class='articles-number-span'><span>{{ $count }}</span> {{ trans('front/blog.articles') }}</span> <!-- NUMBER OF ARTICLES FROM THIS CATEGORY -->
                    </div>

                    {{--*/ $subcat = explode(',', $category->subcategories) /*--}}
                    @if ($category->subcategories !== '')
                        <nav class="filter-category">
                            <span class="filter-by-text">{{ trans('front/blog.filter_by') }}</span>
                            <ul class="select-tags"> <!-- CATEGORY NAV -->
                                <li>
                                    <span> @if (!isset($subCategory)) {{ trans('front/blog.all') }} @else {{ trans('front/subcategory.'.  trim($subCategory)) }} @endif </span>
                                    <i class="fa fa-angle-down filter-icon"></i>
                                    <ul class="tags-list">
                                        @for($i = 0; $i < count($subcat); $i++)
                                            @if ($i == 0 && isset($subCategory))
                                                <li><a href='{{ route('category.show', $category->name) }}'>{{ trans('front/blog.all') }}</a></li>
                                            @endif
                                                <li><a href='{{ route('subcategory.show', [$category->name, trim($subcat[$i])]) }}'>{{trans('front/subcategory.'.trim($subcat[$i]))}}</a></li>
                                        @endfor
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    @endif
                </div>
            </div>
        @else
            <h1 class="category-heading">{{trans('front/category.all') }} {{trans('front/blog.news') }}</h1> <!-- CATEGORY HEADING -->
            <div class="category-annotation"> <!-- CATEGORY TEXT -->
                <p></p>
            </div>
            <div class="category-nav clearfix"> <!-- CATEGORY NUMBER OF POSTS AND NAV -->
                <div class="category-nav-inner-wrapper">
                    <div class="articles-number">
                        <span class='articles-number-span'><span>{{ $count }}</span> {{trans('front/category.articles') }}</span> <!-- NUMBER OF ARTICLES FROM THIS CATEGORY -->
                    </div>
                </div>
            </div>
        @endif
        <ul class="category-news-list clearfix" id="articles"> <!-- NEWS LIST -->
            @foreach($articles as $key => $article)
                @if ($key == 6 && isset($advertisement) && $advertisement !== '')
                    <div class="adv-blog-block clearfix"> <!-- ADVERTISEMENET BLOCK  -->
                        <div class="adv-blog">
                            <span>{!! $advertisement !!}</span>
                        </div>
                    </div>
                @endif
                <li class="latest-news-item"> <!-- LATEST NEWS ITEM TEMPLATE -->
                    <div class="latest-news-preview-img-block-outer">
                        <a href="{{ route('articles.post.show', ['category' => $article->category->name, 'title' => $article->slug] ) }}" class="latest-news-preview-img-block-inner">
                            @if($article->thumb !== '' && file_exists(base_path() .'/public/img/uploads/thumbs/first/' . $article->user_id . '/' . $article->thumb))
                                <img class="latest-news-preview-img" src="/img/uploads/thumbs/first/{!! $article->user_id !!}/{!! $article->thumb !!}" alt="{{$article->title}}">
                            @else
                                <img class="latest-news-preview-img" src="/img/post-default-feautered.jpg" alt="{{$article->title}}">
                            @endif
                            <div class="latest-news-preview-img-overlay">
                                <span>{{ trans('front/blog.read-article') }}</span>
                                <div class="read-article-divider"></div>
                            </div>
                        </a>
                    </div>
                    <a class="latest-news-category-link" href="{{ route('category.show', $article->category->name) }}">{!! trans('front/category.' .  $article->category->name)!!}</a>
                    <h3><a class="latest-news-item-heading" href="{{ route('articles.post.show', [$article->category->name, $article->slug]) }}">{{ $article->title }}<div class="news-underline"></div></a></h3>
                    <span class="latest-news-date">{!!  date('d/m/y', strtotime($article->created_at)) !!}</span>
                    <span class="latest-news-author">{{ trans('front/home.by') }} <a>{{ $article->user->firstname . ' ' . $article->user->lastname}}</a></span>
                    <div class="likes-views-buttons">
                        <button onclick="Like({{ $article->id }}, 'post_')">
                            <i class="fa fa-heart"></i>
                            <span id="post_{{ $article->id }}">{{$article->likes}}</span>
                        </button>
                            <span class="views-number">
                                <i class="fa fa-eye"></i>
                                <span>{{$article->views}}</span>
                            </span>
                    </div>
                </li>
            @endforeach
        </ul>

        <?php
            $url = explode('/', Request::path());

            if (strlen($url[0]) == 2) {
               unset($url[0]);
            }

            $categories = implode('.', $url);
        ?>

        @if ($count > 12)
            @if (isset($tag))
                <button class="more-posts" id='1_{{$categories}}_notsearch_{{$tag}}' onclick="loadPosts(this.id)"> <!-- SHOW MORE BUTTON -->
            @elseif (isset($search))
                <button class="more-posts" id='1_{{$categories}}_{{$search}}_nottag' onclick="loadPosts(this.id)"> <!-- SHOW MORE BUTTON -->
            @else
                <button class="more-posts" id='1_{{$categories}}_notsearch_nottag' onclick="loadPosts(this.id)"> <!-- SHOW MORE BUTTON -->
            @endif
                <span>{{ trans('front/blog.show-more') }}</span>
                <i class="fa fa-angle-down show-more-icon"></i>
            </button>
        @endif

    </section>
@stop