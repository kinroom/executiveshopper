@extends('front.template')

@section('title', "Promotion" . ' - Executive Shopper')

@section('main')
    <div class="static-page-wrapper static-page-promotion-wrapper">
        <section class="fjob-list-section static-page-section clearfix">
            <article>
                <h1 class="static-page-heading">Promore your brand<br>with us</h1>
                <p>This blog is a personal blog written and edited by me. For questions about this blog, please contact Olga Burbelo at www.olgaburbelo.com or Bahnhofstrasse 58, 8001 Zurich, Switzerland.</p>
                <p>This blog accepts forms of cash advertising, sponsorship, paid insertions or other forms of compensation.</p>
                <p>This blog abides by word of mouth marketing standards. We believe in honesty of relationship, opinion and identity. The compensation received may influence the advertising content, topics or posts made in this blog. That content, advertising space or post will be clearly identified as paid or sponsored content.</p>
                <p>The owner(s) of this blog is compensated to provide opinion on products, services, websites and various other topics. Even though the owner(s) of this blog receives compensation for our posts or advertisements, we always give our honest opinions, findings, beliefs, or experiences on those topics or products. The views and opinions expressed on this blog are purely the bloggers’ own. Any product claim, statistic, quote or other representation about a product or service should be verified with the manufacturer, provider or party in question.</p>
            </article>
        </section>
    </div>
@stop
