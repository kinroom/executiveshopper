@extends('front.template')

@section('title', 'About us' . ' - Executive Shopper')

@section('main')
    <div class="static-page-wrapper static-page-about-us-wrapper">
        <section class="fjob-list-section static-page-section clearfix">
            <article>
                <h1 class="static-page-heading">Olga Burbelo<br><span>Creative Director &amp; Personal Shopper</span></h1>
                <p>As founder and Creative Director of Executive Shopper, Olga brings over 8 years of experience in Beauty and Fashion to her prospective clients.</p>
                <p>Olga started her career in fashion as a runway and print model in Italy and Switzerland and was runner up in such events as the Miss Russian of Switzerland Beauty contest in 2008. In conjunction with her modeling career she has worked in the retail sector with some of the most prestigious names in the business such as Loro Piana, Salvatere Ferragamo, Missoni, Karl Lagerfeld, Brunello Cucinelli and Silvano Lattanzi.</p>
                <p>Born in Odessa (Ukraine) and raised in Italy, she currently lives and works in Zurich Switzerland. Her styling sensibilities are truly international, resulting from time spent working, studying, and living in many different countries around the globe.</p>
                <p>Her international training and work experience in the high fashion business has given her the unique experience required to assist her clients in enhancing their personal characteristics so that they can achieve a dynamic sense of self confidence through their appearance.</p>
            </article>
        </section>
    </div>
@stop
