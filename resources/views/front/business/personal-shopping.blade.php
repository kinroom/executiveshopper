@extends('front.template')

@section('title', "Personal shopping" . ' - Executive Shopper')

@section('main')
    <div class="static-page-wrapper">
        <section class="fjob-list-section static-page-section clearfix">
            <article>
                <h1 class="static-page-heading">Your personal <br>shopping assistant</h1>
                <p>EXECUTIVE SHOPPER is about providing professional advice to enhance the overall look of an individual.</p>
                <p>In Personal Image Consulting, an individual’s personality, lifestyle, wardrobe, budget, level of confidence, body image, values, personal and professional goals are assessed. Image Consultants provide assistance to improve an individual’s appearance, as well as verbal and non-verbal communications.</p>
                <p>Because you tell the world who you are based on your appearance, Image Consultants help you communicate your goals successfully. We will help you choose the right clothes, accessories, styles, fit, and colors to help you feel great and project greater confidence in all situations. We help you improve your personal branding and ‘packaging’ to create greater opportunities and relationships to leave lasting impressions at work, in job interviews, school, social and formal functions, dates and business deals.</p>
                <p>EXECUTIVE SHOPPER are dedicated to building men, women and companies to communicate successfully and look successful. We partner with you to achieve your goals because we understand the impact that the right image conveys in all situations.</p>
            </article>
        </section>
    </div>
@stop
