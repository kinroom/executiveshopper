@extends('front.template')

@section('title', 'Work with us' . ' - Executive Shopper')

@section('main')
    <div class="static-page-wrapper static-page-promotion-wrapper static-page-work-wrapper">
        <section class="fjob-list-section static-page-section clearfix">
            <article>
                <h1 class="static-page-heading">Every details matters</h1>
                <p>Every piece of packaging. Every touch and look. Every “How can I help you?” Everything. And it doesn’t matter just some of the time. It matters all of the time. That’s how we do things at EXECUTIVE SHOPPER. The result is work with the most-loved BRENDS in the world.</p>
                <p>EXECUTIVE SHOPPER is seeking two freelance personal shoppers to provide friendly and affordable personal shopping to female and male professionals in Zurich.</p>
                <p>When there’s an opening at EXECUTIVE SHOPPER, we always look to fill it with the best. If that sounds like you, then send your CV right now : ask@exeshopper.com</p>
            </article>
        </section>
    </div>
@stop
