<?php

use Illuminate\Database\Seeder;
use App\Models\Language;

class LanguageSeeder extends Seeder
{
    public function run() {
        $industries = ['en', 'de', 'it', 'ru'];

        for ($i=0; $i < 4;$i++) {
            Language::create([
                'name' => $industries[$i]
            ]);
        }

    }
}
