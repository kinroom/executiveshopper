<?php

use Illuminate\Database\Seeder;
use App\Models\ProductCategory;

class ProductCategorySeeder extends Seeder
{

    public function run()
    {
        $faker = Faker\Factory::create();

        $parents = [
            'Woman' => [
                'Clothing' => ['Denim', 'Dresses', 'Jackets', 'Outerwear', 'Pants', 'Shorts', 'Skirts', 'Sweaters/knit', 'Tops', 'Wedding', 'Suits', 'Jumpsuits'],
                'Shoes' => ['Winter', 'Summer', 'Autumn/spring', 'Sport'],
                'Bags' => ['Man', 'Woman', 'Wallets', 'Trave'],
                'Jewelry' => ['Man', 'Woman', 'Unisex'],
                'Accessories' => ['Belts', 'Gloves', 'Hats', 'Scarves', 'Small accessories', 'Sunglasses']
            ],
            'Man' => [
                'Clothing' => ['Denim', 'Dresses', 'Jackets', 'Outerwear', 'Pants', 'Shorts', 'Skirts', 'Sweaters/knit', 'Tops', 'Wedding', 'Suits', 'Jumpsuits'],
                'Shoes' => ['Winter', 'Summer', 'Autumn/spring', 'Sport'],
                'Bags' => ['Man', 'Woman', 'Wallets', 'Trave'],
                'Jewelry' => ['Man', 'Woman', 'Unisex'],
                'Accessories' => ['Belts', 'Gloves', 'Hats', 'Scarves', 'Small accessories', 'Sunglasses']
            ],
            'Kids'  => [
                'Categories' => ['Accessories', 'Clothing', 'Home', 'Shoes', 'Toys'],
                'Gender' => ['Girls', 'Unisex', 'Boys']
            ],
            'Beauty' => [
                'Categories' => ['Makeup', 'Skincare', 'Bath&Body', 'Men`s care', 'Hair care', 'Babies&kids', 'Accessories', 'Woman', 'Man', 'Fragrance', 'Man', 'Woman']
            ],
            'Pets' => [
                'Categories' => ['Apparel', 'Carriers', 'Sports', 'Collars', 'Toys', 'Furniture']
            ],
            'Home' => [
                'Categories' => ['Apparel', 'Carriers', 'Sports', 'Toys']
            ]
        ];

        $parentId = 0;
        $childQty = 0;

        foreach ($parents as $name => $parent) {
            ProductCategory::create([
                'name' => $name,
                'slug' => str_slug($name, '-'),
                'parent_id' => null
            ]);

            $parentId++;
            $childsNum = 0;

            foreach ($parent as $childName => $child) {
                ProductCategory::create([
                    'name' => $childName,
                    'slug' => str_slug($childName, '-'),
                    'parent_id' => $parentId
                ]);

                $childId = $parentId + $childQty + 1;

                foreach ($child as $category) {
                    ProductCategory::create([
                        'name' => $category,
                        'slug' => str_slug($category, '-'),
                        'parent_id' => $childId
                    ]);
                }

                $childQty += count($child) + 1;
                $childsNum += count($child);
            }
            $childQty = 0;

            $parentId += count($parent) + $childsNum;
        }

    }
}
