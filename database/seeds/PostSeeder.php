<?php

use Illuminate\Database\Seeder;
use App\Models\Post;

class PostSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker\Factory::create();

        $articles =  ["cdzH8wFDA7w.jpg", "co9Inuln178.jpg", "hYdLImKRkPI.jpg", "ygkrCI3dtGo.jpg", "68hk_fFrYr4.jpg", "aAZt-aKIb04.jpg"];
        $subcategories = ['trend', 'look', 'promotion'];
        $video = ['', 'https://www.youtube.com/watch?v=M4z90wlwYs8'];

        for ($i=0;$i<100;$i++) {
            Post::create([
                'title' => $faker->sentence,
                'slug' => 'post-'. $i . 5,
                'views' => $faker->numberBetween(100,500),
                'likes' => $faker->numberBetween(100,500),
                'category_id' => $faker->numberBetween(1,4),
                'subcategory' => $subcategories[$faker->numberBetween(0,2)],
                'thumb' => $articles[$faker->numberBetween(0,5)],
                'content' => '<p>' .$faker->text . '</p>',
                'video' => $video[$faker->numberBetween(0,1)],
                'photographer' => 'doctor',
                'active' => $faker->numberBetween(0,1),
                'active_seen' => $faker->numberBetween(0,1),
                'is_commercial' => $faker->numberBetween(0,1),
                'original_id' => 0,
                'language_id' =>  $faker->numberBetween(1,4),
                'user_id' => 1
            ]);
        }

    }
}
