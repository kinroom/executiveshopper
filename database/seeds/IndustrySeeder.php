<?php

use Illuminate\Database\Seeder;
use App\Models\Industry;

class IndustrySeeder extends Seeder
{
    public function run() {
        $industries = ['fashion', 'beauty', 'advertising', 'apparel', 'communications', 'consulting', 'retail', 'manufacturing', 'health'];

        for ($i=0; $i < 9;$i++) {
            Industry::create([
                'name' => $industries[$i]
            ]);
        }

    }
}
