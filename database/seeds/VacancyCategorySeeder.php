<?php

use Illuminate\Database\Seeder;
use App\Models\VacancyCategory;

class VacancyCategorySeeder  extends Seeder
{

    public function run() {
        $categories = ['retail', 'marketing', 'design', 'product', 'administration'];

        for ($i=0;$i < 5;$i++) {
            VacancyCategory::create([
                'name' => $categories[$i]
            ]);
        }

    }
}
