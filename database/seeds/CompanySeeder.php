<?php

use Illuminate\Database\Seeder;
use App\Models\Company;

class CompanySeeder extends Seeder
{
    public function run() {
        $faker = Faker\Factory::create();

        for ($i=0;$i < 100;$i++) {
            Company::create([
                'name' => $faker->company,
                'user_id' => $faker->numberBetween(1,50),
                'logo' => '',
                'description' => $faker->text(200),
                'site' => $faker->url,
                'email' => $faker->email,
                'number' => $faker->phoneNumber . ',' . $faker->phoneNumber
            ]);
        }

    }
}
