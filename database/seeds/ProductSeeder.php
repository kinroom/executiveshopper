<?php

use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\ProductCategory;

class ProductSeeder extends Seeder
{

    public function run()
    {
        $images = ["cdzH8wFDA7w.jpg", "co9Inuln178.jpg", "hYdLImKRkPI.jpg", "ygkrCI3dtGo.jpg", "68hk_fFrYr4.jpg", "aAZt-aKIb04.jpg"];
        $colours = "black;blue;red;yellow";
        $sizes = 'XS,S,L,XL';

        $faker = Faker\Factory::create();

        for ($i = 0; $i < 1000; $i++) {
            $title = $faker->sentence;
            Product::create([
                'name' => $title,
                'slug' => str_slug($title . $i, '-'),
                'thumb' => $images[$faker->numberBetween(0,5)],
                'sku' => $faker->numberBetween(0, 100000),
                'price' => $faker->numberBetween(100, 4000),
                'colours' => $colours,
                'sizes' => $sizes,
                'size_fit' => $faker->text,
                'details' => $faker->text,
                'material_cafe' => $faker->text,
                'delivery' => $faker->text,
            ]);
        }

        for ($i = 0; $i < 1000; $i++) {
            DB::table('products_categories')->insert([
                'category_id' => $faker->numberBetween(1, 109),
                'product_id' => $i + 1
            ]);
        }

    }
}
