<?php

use Illuminate\Database\Seeder;
use App\Models\Vacancy;


class VacancySeeder extends Seeder
{
    public function run() {
        $faker = Faker\Factory::create();

        $countries = ['Ukraine', 'Russia', 'Belarus'];
        $cities = ['Kiev', 'Dnepropetrovsk', 'Donetsk'];


        for ($i=0;$i < 36;$i++) {
            Vacancy::create([
                'user_id' => 2,
                'company_id' => $faker->numberBetween(1, 100),
                'title' => $faker->text(60),
                'description' => $faker->text(200),
                'salary' => $faker->randomNumber(),
                'country' => $countries[$faker->numberBetween(0, 2)],
                'city' => $cities[$faker->numberBetween(0, 2)],
                'availability_id' => $faker->boolean(),
                'category_id' => $faker->numberBetween(1, 5),
                'industry_id' => $faker->numberBetween(1, 9)
            ]);
        }

    }
}
