<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Role, App\Models\User, App\Models\Tag, App\Models\Category, App\Models\PostTag, App\Models\Comment;
use App\Services\LoremIpsumGenerator;

class DatabaseSeeder extends Seeder
{
	public function run()
	{
		Model::unguard();

		$lipsum = new LoremIpsumGenerator;


		Role::create([
			'title' => 'Administrator',
			'slug' => 'admin'
		]);

		Role::create([
			'title' => 'Redactor',
			'slug' => 'redac'
		]);

		Role::create([
			'title' => 'Blogger',
			'slug' => 'blogger'
		]);

		Role::create([
			'title' => 'Employer',
			'slug' => 'employer'
		]);

		Role::create([
			'title' => 'User',
			'slug' => 'user'
		]);

		User::create([
			'firstname' => 'Site',
            'lastname' => 'Administrator',
			'email' => 'admin@gmail.com',
			'contact_email' => 'olga.burbelo@gmail.com',
			'password' => bcrypt('odessa7777'),
			'seen' => true,
			'role_id' => 1,
			'confirmed' => true
		]);

		User::create([
            'firstname' => 'Great',
            'lastname' => 'Redactor',
			'email' => 'redac@la.fr',
			'contact_email' => 'redac@la.fr',
			'password' => bcrypt('redac'),
			'seen' => true,
			'role_id' => 2,
			'valid' => true,
			'confirmed' => true
		]);

		User::create([
            'firstname' => 'User',
            'lastname' => 'Walker',
			'email' => 'walker@la.fr',
			'contact_email' => 'redac@la.fr',
			'password' => bcrypt('walker'),
			'role_id' => 3,
			'confirmed' => true
		]);

		User::create([
            'firstname' => 'User',
            'lastname' => 'Slacker',
			'email' => 'slacker@la.fr',
			'contact_email' => 'redac@la.fr',
			'password' => bcrypt('slacker'),
			'role_id' => 3,
			'confirmed' => true
		]);

        $faker = Faker\Factory::create();

        for ($i=0;$i<50;$i++) {
            User::create([
                'firstname' => $faker->firstName,
                'lastname' => $faker->lastName,
                'email' => $faker->email,
				'contact_email' => 'redac@la.fr',
                'password' => bcrypt('slacker'),
                'role_id' => 3,
                'confirmed' => true
            ]);
        }


		Tag::create([
			'tag' => 'Tag1'
		]);

		Tag::create([
			'tag' => 'Tag2'
		]);

		Tag::create([
			'tag' => 'Tag3'
		]);

		Tag::create([
			'tag' => 'Tag4'
		]);

        Category::create([
            'name' => 'fashion',
            'subcategories' => 'trend, look, promotion'
        ]);

        Category::create([
            'name' => 'lifestyle',
            'subcategories' => 'travel, hotel, sport, food, car, home'
        ]);


        Category::create([
            'name' => 'beauty',
            'subcategories' =>  'makeup, hair, body'
        ]);

        Category::create([
            'name' => 'video',
        ]);


        $this->call('PostSeeder');
        $this->call('VacancyCategorySeeder');
        $this->call('IndustrySeeder');
        $this->call('CompanySeeder');
        $this->call('VacancySeeder');
		$this->call('LanguageSeeder');
		$this->call('ProductCategorySeeder');
		$this->call('ProductSeeder');

        for ($i=1;$i<99;$i++) {
            PostTag::create([
                'post_id' => $i,
                'tag_id' => $faker->numberBetween(1,4),
            ]);
        }


		PostTag::create([
			'post_id' => 1,
			'tag_id' => 2
		]);

		PostTag::create([
			'post_id' => 2,
			'tag_id' => 1
		]);

		PostTag::create([
			'post_id' => 2,
			'tag_id' => 2
		]);

		PostTag::create([
			'post_id' => 2,
			'tag_id' => 3
		]);

		PostTag::create([
			'post_id' => 3,
			'tag_id' => 1
		]);

		PostTag::create([
			'post_id' => 3,
			'tag_id' => 2
		]);

		PostTag::create([
			'post_id' => 3,
			'tag_id' => 4
		]);

		$this->call('LaravelShopSeeder');

		Model::reguard();
	}

}
