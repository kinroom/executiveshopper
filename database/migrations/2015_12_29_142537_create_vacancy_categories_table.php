<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacancyCategoriesTable extends Migration
{
    public function up() {
        Schema::create('vacancy_categories', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('vacancy_categories');
    }
}
