<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacancyTable extends Migration
{
    public function up() {
        Schema::create('vacancies', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('company_id');
            $table->string('title');
            $table->text('description');
            $table->string('salary');
            $table->string('country')->default('All');
            $table->string('city')->default('All');;
            $table->string('availability_id')->default(1);
            $table->string('category_id');
            $table->string('industry_id');
            $table->boolean('is_active')->default(0);
            $table->boolean('seen')->default(0);
            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('vacancies');
    }
}
