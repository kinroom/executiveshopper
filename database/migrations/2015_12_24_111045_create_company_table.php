<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration {

    public function up() {

        Schema::create('companies', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('name');
            $table->string('logo')->default('');
            $table->string('site', 255);
            $table->text('description');
            $table->string('email', 70);
            $table->string('number');
            $table->unique(array('user_id', 'name'));
            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('companies');
    }
}
