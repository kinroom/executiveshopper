<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function(Blueprint $table) {
			$table->increments('id');
			$table->string('title', 255);
			$table->string('slug', 255)->unique();
            $table->string('video');
            $table->integer('category_id');
            $table->string('subcategory');
			$table->string('thumb')->default('');
			$table->text('content');
			$table->integer('language_id')->nullable();
            $table->string('photographer')->default('');
            $table->string('styling')->default('');
            $table->string('outfit')->default('');
            $table->string('location')->default('');
            $table->string('website')->default('');
            $table->integer('views')->default(0);
            $table->integer('likes')->default(0);
            $table->boolean('is_commercial')->default(0)->nullable();
			$table->boolean('original_id')->default(0)->nullable();
			$table->boolean('translator_id')->nullable();
            $table->boolean('seen')->default(false);
            $table->boolean('active')->default(false);
            $table->boolean('active_seen')->default(false);
			$table->integer('user_id')->unsigned();
			$table->text('description')->nullable();
			$table->string('keywords')->nullable();
            $table->timestamps();
		});

		Schema::table('posts', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('posts', function(Blueprint $table) {
			$table->dropForeign('posts_user_id_foreign');
		});		

		Schema::drop('posts');
	}

}
