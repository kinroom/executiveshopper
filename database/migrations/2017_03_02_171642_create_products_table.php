<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function(Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->string('slug')->unique();
            $table->string('thumb')->nullable();
            $table->string('colours')->nullable();
            $table->string('sizes')->nullable();
            $table->text('video')->nullable();
            $table->text('size_fit')->nullable();
            $table->text('details')->nullable();
            $table->text('material_cafe')->nullable();
            $table->text('delivery')->nullable();
            $table->text('description')->nullable();
            $table->string('keywords')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
