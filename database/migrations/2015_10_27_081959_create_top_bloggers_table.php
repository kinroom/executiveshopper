<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopBloggersTable extends Migration
{

    public function up() {
        Schema::create('top_bloggers', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('description');
            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('top_bloggers');
    }
}
