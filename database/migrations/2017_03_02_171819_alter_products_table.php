<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function($table)
        {
            $table->string('sku', 10)->after('id');
            $table->decimal('price', 20, 2)->after('sku');
            $table->index('sku');
            $table->index('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Restore type field
        Schema::table('products', function($table)
        {
            $table->dropColumn('sku');
            $table->dropColumn('price');
        });
    }
}
