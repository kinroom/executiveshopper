<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{

    public function up()
    {
        Schema::create('categories', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description')->default('');
            $table->string('subcategories')->default('');
            $table->timestamps();
            $table->unique('name');
        });
    }

    public function down()
    {
        Schema::drop('categories');
    }
}
