<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('service', 30)->default('site');
			$table->string('firstname', 30);
            $table->string('lastname', 30);
            $table->string('avatar')->default('');
			$table->string('email')->unique();
            $table->string('contact_email')->default('');
            $table->boolean('hide_email')->default(0);
            $table->string('birthday')->default('');
            $table->boolean('hide_birthday')->default(0);
            $table->boolean('gender')->default(0);
            $table->string('number')->default('');
            $table->boolean('hide_number')->default(0);
            $table->string('site')->default('');
			$table->string('password', 60);
			$table->integer('role_id')->unsigned();
			$table->boolean('seen')->default(false);
			$table->boolean('valid')->default(false);
			$table->boolean('confirmed')->default(false);
			$table->string('confirmation_code')->nullable();
			$table->timestamps();
			$table->rememberToken();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
