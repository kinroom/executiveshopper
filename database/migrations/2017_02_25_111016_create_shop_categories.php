<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_categories', function(Blueprint $t) {
            $t->increments('id');

            // Nested Set related fields
            $t->integer('parent_id')->nullable();
            $t->integer('lft')->nullable();
            $t->integer('rgt')->nullable();
            $t->integer('depth')->nullable();
            $t->text('description')->nullable();
            $t->string('keywords')->nullable();

            $t->string('name', 255);
            $t->string('slug', 255);
            $t->timestamps();

            // Indexes
            $t->index('parent_id');
            $t->index('lft');
            $t->index('rgt');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shop_categories');
    }
}
