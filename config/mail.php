<?php

return [
    'driver' => env('MAIL_DRIVER', 'smtp'),
    'host' => env('MAIL_HOST', 'smtp.gmail.com'),
    'port' => env('MAIL_PORT', '587'),
    'from' => ['address' => 'valentinemurnik@gmail.com', 'name' => 'Olga Burbelo'],
    'encryption' => env('MAIL_ENCRYPTION', 'tls'),
    'username' => env('MAIL_USERNAME', 'valentinemurnik@gmail.com'),
    'password' => env('MAIL_PASSWORD', 'ilovemygod11'),
    'sendmail' => '/usr/sbin/sendmail -bs',
    'pretend' => false,
];
